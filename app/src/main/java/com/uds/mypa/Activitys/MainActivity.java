package com.uds.mypa.Activitys;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Base64;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.uds.mypa.Home.fragment.HomeFragment;
import com.uds.mypa.Home.fragment.Reminders.RemindersHomeFrag;
import com.uds.mypa.Home.fragment.SensitiveItemsFragment;
import com.uds.mypa.R;
import com.uds.mypa.SensitiveItems.fragment.AadharFragment;
import com.uds.mypa.SensitiveItems.fragment.BankAccountFragment;
import com.uds.mypa.SensitiveItems.fragment.BankingCardFragment;
import com.uds.mypa.SensitiveItems.fragment.CompanyProfileFragment;
import com.uds.mypa.SensitiveItems.fragment.DrivingLicenseFragment;
import com.uds.mypa.SensitiveItems.fragment.ElectricityFragment;
import com.uds.mypa.SensitiveItems.fragment.EmailFragment;
import com.uds.mypa.SensitiveItems.fragment.GasConnectionFragment;
import com.uds.mypa.SensitiveItems.fragment.InsuranceFragment;
import com.uds.mypa.SensitiveItems.fragment.MembershipFragment;
import com.uds.mypa.SensitiveItems.fragment.NewAadharFragment;
import com.uds.mypa.SensitiveItems.fragment.NewBankAccountFragment;
import com.uds.mypa.SensitiveItems.fragment.NewBankingCardFragment;
import com.uds.mypa.SensitiveItems.fragment.NewCompanyProfileFragment;
import com.uds.mypa.SensitiveItems.fragment.NewDrivingLicenseFragment;
import com.uds.mypa.SensitiveItems.fragment.NewElectricityFragment;
import com.uds.mypa.SensitiveItems.fragment.NewEmailFragment;
import com.uds.mypa.SensitiveItems.fragment.NewGasConnectionFragment;
import com.uds.mypa.SensitiveItems.fragment.NewInsuranceFragment;
import com.uds.mypa.SensitiveItems.fragment.NewMembershipFragment;
import com.uds.mypa.SensitiveItems.fragment.NewOnlineAccountsFragment;
import com.uds.mypa.SensitiveItems.fragment.NewPanCardFragment;
import com.uds.mypa.SensitiveItems.fragment.NewPassportFragment;
import com.uds.mypa.SensitiveItems.fragment.NewRemoteLoginFragment;
import com.uds.mypa.SensitiveItems.fragment.NewSocialNetworkingFragment;
import com.uds.mypa.SensitiveItems.fragment.NewSystemLoginFragment;
import com.uds.mypa.SensitiveItems.fragment.NewVoterIdFragment;
import com.uds.mypa.SensitiveItems.fragment.NewWifiFragment;
import com.uds.mypa.SensitiveItems.fragment.OnlineAccountsFragment;
import com.uds.mypa.SensitiveItems.fragment.PanCardFragment;
import com.uds.mypa.SensitiveItems.fragment.PassportFragment;
import com.uds.mypa.SensitiveItems.fragment.RemoteLoginFragment;
import com.uds.mypa.SensitiveItems.fragment.SocialNetworkingFragment;
import com.uds.mypa.SensitiveItems.fragment.SystemLoginFragment;
import com.uds.mypa.SensitiveItems.fragment.VoterCardFragment;
import com.uds.mypa.SensitiveItems.fragment.WifiFragment;
import com.uds.mypa.SensitiveItems.model.Aadhar;
import com.uds.mypa.SensitiveItems.model.BankAccount;
import com.uds.mypa.SensitiveItems.model.BankingCard;
import com.uds.mypa.SensitiveItems.model.CompanyProfile;
import com.uds.mypa.SensitiveItems.model.DrivingLicense;
import com.uds.mypa.SensitiveItems.model.Electricity;
import com.uds.mypa.SensitiveItems.model.Email;
import com.uds.mypa.SensitiveItems.model.GasConnection;
import com.uds.mypa.SensitiveItems.model.Insurance;
import com.uds.mypa.SensitiveItems.model.OnlineAccounts;
import com.uds.mypa.SensitiveItems.model.PanCard;
import com.uds.mypa.SensitiveItems.model.Passport;
import com.uds.mypa.SensitiveItems.model.RemoteLogin;
import com.uds.mypa.SensitiveItems.model.SocialNetworking;
import com.uds.mypa.SensitiveItems.model.SystemLogin;
import com.uds.mypa.SensitiveItems.model.VoterCard;
import com.uds.mypa.SensitiveItems.model.Wifi;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class MainActivity extends AppCompatActivity {
    private BottomSheetBehavior mBottomSheetBehavior;
    private String key = "", id = "";
    private Object object;
    private String date;
    private CoordinatorLayout mRoot;
    public DatePickerDialog datePickerDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUpFragment(0, null);
        View bottomSheet = findViewById(R.id.bottomSheet);
        mBottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        mBottomSheetBehavior.setPeekHeight(0);
        mBottomSheetBehavior.setHideable(true);
        mBottomSheetBehavior.setDraggable(true);
        mRoot = findViewById(R.id.root);
        TextView deleteTextView = findViewById(R.id.delete_textView);
        TextView viewTextView = findViewById(R.id.view_textView);
        deleteTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteItem();
            }
        });
        viewTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewData(0);
            }
        });
        TextView editTextView = findViewById(R.id.edit_textView);
        editTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewData(1);
            }
        });
    }

    public void setUpFragment(int position, Bundle bundle) {
        Fragment fragment = null;
        switch (position) {
            case 0:
                fragment = new HomeFragment();
                fragment.setArguments(bundle);
                break;
            case 1:
                fragment = new SensitiveItemsFragment();
                fragment.setArguments(bundle);
                break;
            case 2:
                fragment = new PassportFragment();
                fragment.setArguments(bundle);
                break;
            case 3:
                fragment = new NewPassportFragment();
                fragment.setArguments(bundle);
                break;
            case 4:
                fragment = new VoterCardFragment();
                fragment.setArguments(bundle);
                break;
            case 5:
                fragment = new NewVoterIdFragment();
                fragment.setArguments(bundle);
                break;
            case 6:
                fragment = new PanCardFragment();
                fragment.setArguments(bundle);
                break;
            case 7:
                fragment = new NewPanCardFragment();
                fragment.setArguments(bundle);
                break;
            case 8:
                fragment = new AadharFragment();
                fragment.setArguments(bundle);
                break;
            case 9:
                fragment = new NewAadharFragment();
                fragment.setArguments(bundle);
                break;
            case 10:
                fragment = new DrivingLicenseFragment();
                fragment.setArguments(bundle);
                break;
            case 11:
                fragment = new NewDrivingLicenseFragment();
                fragment.setArguments(bundle);
                break;
            case 12:
                fragment = new BankingCardFragment();
                fragment.setArguments(bundle);
                break;
            case 13:
                fragment = new NewBankingCardFragment();
                fragment.setArguments(bundle);
                break;
            case 14:
                fragment = new BankAccountFragment();
                fragment.setArguments(bundle);
                break;
            case 15:
                fragment = new NewBankAccountFragment();
                fragment.setArguments(bundle);
                break;
            case 16:
                fragment = new MembershipFragment();
                fragment.setArguments(bundle);
                break;
            case 17:
                fragment = new NewMembershipFragment();
                fragment.setArguments(bundle);
                break;
            case 18:
                fragment = new EmailFragment();
                fragment.setArguments(bundle);
                break;
            case 19:
                fragment = new NewEmailFragment();
                fragment.setArguments(bundle);
                break;
            case 20:
                fragment = new SocialNetworkingFragment();
                fragment.setArguments(bundle);
                break;
            case 21:
                fragment = new NewSocialNetworkingFragment();
                fragment.setArguments(bundle);
                break;
            case 22:
                fragment = new OnlineAccountsFragment();
                fragment.setArguments(bundle);
                break;
            case 23:
                fragment = new NewOnlineAccountsFragment();
                fragment.setArguments(bundle);
                break;
            case 24:
                fragment = new InsuranceFragment();
                fragment.setArguments(bundle);
                break;
            case 25:
                fragment = new NewInsuranceFragment();
                fragment.setArguments(bundle);
                break;
            case 26:
                fragment = new ElectricityFragment();
                fragment.setArguments(bundle);
                break;
            case 27:
                fragment = new NewElectricityFragment();
                fragment.setArguments(bundle);
                break;
            case 28:
                fragment = new GasConnectionFragment();
                fragment.setArguments(bundle);
                break;
            case 29:
                fragment = new NewGasConnectionFragment();
                fragment.setArguments(bundle);
                break;
            case 30:
                fragment = new SystemLoginFragment();
                fragment.setArguments(bundle);
                break;
            case 31:
                fragment = new NewSystemLoginFragment();
                fragment.setArguments(bundle);
                break;
            case 32:
                fragment = new RemoteLoginFragment();
                fragment.setArguments(bundle);
                break;
            case 33:
                fragment = new NewRemoteLoginFragment();
                fragment.setArguments(bundle);
                break;
            case 34:
                fragment = new WifiFragment();
                fragment.setArguments(bundle);
                break;
            case 35:
                fragment = new NewWifiFragment();
                fragment.setArguments(bundle);
                break;
            case 36:
                fragment = new CompanyProfileFragment();
                fragment.setArguments(bundle);
                break;
            case 37:
                fragment = new NewCompanyProfileFragment();
                fragment.setArguments(bundle);
                break;
            case 38:
                fragment = new RemindersHomeFrag();
                fragment.setArguments(bundle);
                break;
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        if (position == 0) {
            fragmentManager.beginTransaction().replace(R.id.container, fragment).commit();
        } else {
            int count = getSupportFragmentManager().getBackStackEntryCount();
            if (fragment != null) {
                fragmentManager.beginTransaction().setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right).addToBackStack(null).replace(R.id.container, fragment, "" + count).commit();
            }

        }
    }

    public void removeAllStack() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.getBackStackEntryCount();
        for (int i = 0; i < fragmentManager.getBackStackEntryCount(); i++)
            fragmentManager.popBackStack();
    }

    public void goBack(View view) {
        if (getFragmentManager().getBackStackEntryCount() == 1) {
            finish();
        } else {
            super.onBackPressed();
        }
    }

    public void goBack() {
        if (getFragmentManager().getBackStackEntryCount() == 1) {
            finish();
        } else {
            super.onBackPressed();
        }
    }

    public void openBottomSheet(String id, String key, Object object) {
        this.key = key;
        this.id = id;
        this.object = object;
        if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        } else
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);

    }

    private void deleteItem() {
        showAlertBox(null, null, 3);
    }

    private void viewData(int choice) {
        if (object instanceof Passport) {
            Bundle args = new Bundle();
            args.putParcelable("passport", (Parcelable) object);
            args.putInt("choose", choice);
            setUpFragment(3, args);
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        } else if (object instanceof VoterCard) {
            Bundle args = new Bundle();
            args.putParcelable("voter", (Parcelable) object);
            args.putInt("choose", choice);
            setUpFragment(5, args);
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        } else if (object instanceof PanCard) {
            Bundle args = new Bundle();
            args.putParcelable("pan", (Parcelable) object);
            setUpFragment(7, args);
            args.putInt("choose", choice);
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        } else if (object instanceof Aadhar) {
            Bundle args = new Bundle();
            args.putParcelable("aadhar", (Parcelable) object);
            setUpFragment(9, args);
            args.putInt("choose", choice);
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        } else if (object instanceof Email) {
            Bundle args = new Bundle();
            args.putParcelable("email", (Parcelable) object);
            args.putInt("choose", choice);
            setUpFragment(19, args);
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        } else if (object instanceof DrivingLicense) {
            Bundle args = new Bundle();
            args.putInt("choose", choice);
            args.putParcelable("driving_license", (Parcelable) object);
            setUpFragment(11, args);
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        } else if (object instanceof RemoteLogin) {
            Bundle args = new Bundle();
            args.putInt("choose", choice);
            args.putParcelable("remote_login", (Parcelable) object);
            setUpFragment(33, args);
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        } else if (object instanceof GasConnection) {
            Bundle args = new Bundle();
            args.putInt("choose", choice);
            args.putParcelable("gas", (Parcelable) object);
            setUpFragment(29, args);
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        } else if (object instanceof OnlineAccounts) {
            Bundle args = new Bundle();
            args.putInt("choose", choice);
            args.putParcelable("online_accounts", (Parcelable) object);
            setUpFragment(23, args);
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        } else if (object instanceof BankingCard) {
            Bundle args = new Bundle();
            args.putInt("choose", choice);
            args.putParcelable("bankingCard", (Parcelable) object);
            setUpFragment(13, args);
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        } else if (object instanceof SocialNetworking) {
            Bundle args = new Bundle();
            args.putInt("choose", choice);
            args.putParcelable("socialNetworking", (Parcelable) object);
            setUpFragment(21, args);
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        } else if (object instanceof SystemLogin) {
            Bundle args = new Bundle();
            args.putInt("choose", choice);
            args.putParcelable("systemLogin", (Parcelable) object);
            setUpFragment(31, args);
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        } else if (object instanceof Electricity) {
            Bundle args = new Bundle();
            args.putInt("choose", choice);
            args.putParcelable("electricity", (Parcelable) object);
            setUpFragment(27, args);
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        } else if (object instanceof Insurance) {
            Bundle args = new Bundle();
            args.putInt("choose", choice);
            args.putParcelable("insurance", (Parcelable) object);
            setUpFragment(25, args);
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        } else if (object instanceof BankAccount) {
            Bundle args = new Bundle();
            args.putInt("choose", choice);
            args.putParcelable("bankAccount", (Parcelable) object);
            setUpFragment(15, args);
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        } else if (object instanceof Wifi) {
            Bundle args = new Bundle();
            args.putInt("choose", choice);
            args.putParcelable("wifi", (Parcelable) object);
            setUpFragment(35, args);
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        } else if (object instanceof CompanyProfile) {
            Bundle args = new Bundle();
            args.putInt("choose", choice);
            args.putParcelable("companyProfile", (Parcelable) object);
            setUpFragment(37, args);
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
    }

    public void toggleBottomSheet() {
        if (mBottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED) {
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        } else
            mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    public void collapseBottomSheet() {
        mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    public ArrayList<String> encryptData(ArrayList<String> users, String id) throws Exception {
        ArrayList<String> list = new ArrayList<>();
        SecretKeySpec key = generateKey(id);
        Cipher c = Cipher.getInstance(getString(R.string.AES));
        c.init(Cipher.ENCRYPT_MODE, key);
        for (int i = 0; i < users.size(); i++) {
            byte[] encVal = c.doFinal(users.get(i).getBytes());
            list.add(Base64.encodeToString(encVal, Base64.DEFAULT));
        }
        return list;
    }


    private SecretKeySpec generateKey(String id) throws Exception {
        final MessageDigest digest = MessageDigest.getInstance("SHA-256");
        byte[] bytes = id.getBytes(StandardCharsets.UTF_8);
        digest.update(bytes, 0, bytes.length);
        byte[] key = digest.digest();
        return new SecretKeySpec(key, "AES");

    }

    public String decryptData(String data, String id) throws Exception {
        SecretKeySpec key = generateKey(id);
        Cipher c = Cipher.getInstance("AES");
        c.init(Cipher.DECRYPT_MODE, key);
        byte[] decodeValue = Base64.decode(data, Base64.DEFAULT);
        byte[] decValue = c.doFinal(decodeValue);
        return new String(decValue);
    }


    public String getDate() {
        return date;
    }

    public void showSnackbar(String message) {
        Snackbar.make(mRoot, message, BaseTransientBottomBar.LENGTH_SHORT).show();
    }

    public boolean compareDate(String d1, String d2, boolean compareToday) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
        Date date = new Date();
        if (compareToday) {
            Date date1 = sdf.parse(d1);
            int comparison = date.compareTo(date1);
            return comparison >= 0;
        } else {
            Date date1 = sdf.parse(d1);
            Date date2 = sdf.parse(d2);
            int comparison = Objects.requireNonNull(date1).compareTo(date2);
            return comparison >= 0;
        }
    }

    public boolean compareBankDate(String d1, String d2, boolean compareToday) throws ParseException {
        SimpleDateFormat sdf = new SimpleDateFormat("MM/yy");
        Date date = new Date();
        if (compareToday) {
            Date date1 = sdf.parse(d1);
            int comparison = date.compareTo(date1);
            return comparison >= 0;
        } else {
            Date date1 = sdf.parse(d1);
            Date date2 = sdf.parse(d2);
            int comparison = Objects.requireNonNull(date1).compareTo(date2);
            return comparison >= 0;
        }
    }


    public void showAlertBox(String title, String message, int alertType) {
        switch (alertType) {
            case 0:
                new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText(title)
                        .setContentText(message)
                        .show();
                break;
            case 1:
                new SweetAlertDialog(this, SweetAlertDialog.ERROR_TYPE)
                        .setTitleText(title)
                        .setContentText(message)
                        .show();
                break;
            case 2:
                new SweetAlertDialog(this, SweetAlertDialog.SUCCESS_TYPE)
                        .setTitleText(title)
                        .setContentText(message)
                        .show();
                break;
            case 3:
                mBottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Are you sure?")
                        .setContentText("Won't be able to recover this record!")
                        .setConfirmText("Yes,delete it!").setCancelText("No,cancel plz!")
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.cancel();
                            }
                        })
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                                assert user != null;
                                DatabaseReference reference = FirebaseDatabase.getInstance().getReference(user.getUid()).child(key).child(id);
                                reference.removeValue();
                                sDialog
                                        .setTitleText("Deleted!")
                                        .setContentText("Your record  has been deleted!")
                                        .setConfirmText("OK")
                                        .setConfirmClickListener(null)
                                        .showCancelButton(false)
                                        .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);

                            }
                        })
                        .show();
                break;

        }
    }

    public boolean isValidEmail(String email) {
        String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\." +
                "[a-zA-Z0-9_+&*-]+)*@" +
                "(?:[a-zA-Z0-9-]+\\.)+[a-z" +
                "A-Z]{2,7}$";

        Pattern pat = Pattern.compile(emailRegex);
        if (email == null)
            return false;
        return pat.matcher(email).matches();
    }

    public boolean isValidPhone(String phone) {
        Pattern p = Pattern.compile("[6-9][0-9]{9}");
        Matcher m = p.matcher(phone);
        return (m.find() && m.group().equals(phone));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
