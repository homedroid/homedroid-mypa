package com.uds.mypa.utils;

public class Utils {
    public static String formatDate(int day,int month,int year)
    {
        return String.format("%02d",day)+"/"+String.format("%02d", month+1)+"/"+year;
    }
    public static String formatBankDate(int month,int year)
    {
        return String.format("%02d", month)+"/"+String.format("%02d",year);
    }



}
