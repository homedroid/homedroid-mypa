package com.uds.mypa.utils;

import com.uds.mypa.R;

public class Images {
    public static int[] reminderIcons = {
            R.drawable.birthday,
            R.drawable.wedding,
            R.drawable.events,
            R.drawable.meeting,
            R.drawable.pills
    };
    public static String[] remindersTypes = {
            "Birthday", "Anniversary", "Events", "Appointment","Medicine"};
}

