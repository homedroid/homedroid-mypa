package com.uds.mypa.Interfaces;

public interface OnItemClickListener {
    void onItemClick(int position);
}
