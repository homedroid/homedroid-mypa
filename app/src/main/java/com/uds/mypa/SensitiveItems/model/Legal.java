package com.uds.mypa.SensitiveItems.model;

public class Legal {
    private String ventureName, numberOfDirectors, CIN, incorporationDate, HeadOfficeAddress, PAN, TAN;

    public Legal() {
    }

    public Legal(String ventureName, String numberOfDirectors, String CIN, String incorporationDate, String headOfficeAddress, String PAN, String TAN) {
        this.ventureName = ventureName;
        this.numberOfDirectors = numberOfDirectors;
        this.CIN = CIN;
        this.incorporationDate = incorporationDate;
        HeadOfficeAddress = headOfficeAddress;
        this.PAN = PAN;
        this.TAN = TAN;
    }

    public String getVentureName() {
        return ventureName;
    }

    public String getNumberOfDirectors() {
        return numberOfDirectors;
    }

    public String getCIN() {
        return CIN;
    }

    public String getIncorporationDate() {
        return incorporationDate;
    }

    public String getHeadOfficeAddress() {
        return HeadOfficeAddress;
    }

    public String getPAN() {
        return PAN;
    }

    public String getTAN() {
        return TAN;
    }
}
