package com.uds.mypa.SensitiveItems.model;

import android.os.Parcel;
import android.os.Parcelable;

public class VoterCard implements Parcelable{
    private String id;
    private String name;
    private String voterIDNumber;
    private String dob;
    private String address;
    private String dateOfIssue;
    private String fathersName;
    public VoterCard() {
    }
    public VoterCard(String id, String name, String voterIDNumber, String dob, String address, String dateOfIssue, String fathersName) {
        this.id = id;
        this.name = name;
        this.voterIDNumber = voterIDNumber;
        this.dob = dob;
        this.address = address;
        this.dateOfIssue = dateOfIssue;
        this.fathersName = fathersName;
    }

    protected VoterCard(Parcel in) {
        id = in.readString();
        name = in.readString();
        voterIDNumber = in.readString();
        dob = in.readString();
        address = in.readString();
        dateOfIssue = in.readString();
        fathersName = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(voterIDNumber);
        dest.writeString(dob);
        dest.writeString(address);
        dest.writeString(dateOfIssue);
        dest.writeString(fathersName);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<VoterCard> CREATOR = new Creator<VoterCard>() {
        @Override
        public VoterCard createFromParcel(Parcel in) {
            return new VoterCard(in);
        }

        @Override
        public VoterCard[] newArray(int size) {
            return new VoterCard[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getVoterIDNumber() {
        return voterIDNumber;
    }

    public String getDob() {
        return dob;
    }

    public String getAddress() {
        return address;
    }

    public String getDateOfIssue() {
        return dateOfIssue;
    }
    public String getFathersName() {
        return fathersName;
    }

}
