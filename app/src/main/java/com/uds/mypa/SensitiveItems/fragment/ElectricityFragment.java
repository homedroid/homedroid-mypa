package com.uds.mypa.SensitiveItems.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.uds.mypa.Activitys.MainActivity;
import com.uds.mypa.Interfaces.OnItemClickListener;
import com.uds.mypa.R;
import com.uds.mypa.SensitiveItems.adapters.ElectricityAdapter;
import com.uds.mypa.SensitiveItems.model.Electricity;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ElectricityFragment extends Fragment implements OnItemClickListener {
    private ArrayList<Electricity> arrayList;
    private MainActivity mainActivity;
    private FirebaseAnalytics mFirebaseAnalytics;
    private FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    private DatabaseReference reference = FirebaseDatabase.getInstance().getReference(user.getUid()).child("Electricity");
    private ArrayList<Electricity> list;
    private RecyclerView recyclerView;
    private ShimmerFrameLayout shimmerFrameLayout;
    private OnItemClickListener listener;
    private ElectricityAdapter adapter;
    private LottieAnimationView lottieAnimationView;

    public ElectricityFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_electricity, container, false);
        mainActivity = (MainActivity) getActivity();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mainActivity);
        arrayList = new ArrayList<>();
        shimmerFrameLayout = view.findViewById(R.id.shimmer);
        listener = this;
        list = new ArrayList<>();
        recyclerView = view.findViewById(R.id.recyclerView);
        lottieAnimationView=view.findViewById(R.id.animation_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(mainActivity));
        FloatingActionButton fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.setUpFragment(27, null);
            }
        });
        recyclerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainActivity.toggleBottomSheet();
            }
        });
        shimmerFrameLayout.startShimmerAnimation();
        Toolbar mToolbar = view.findViewById(R.id.toolbar);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.goBack();
            }
        });
        return view;
    }

    @Override
    public void onItemClick(int position) {
        mainActivity.openBottomSheet(arrayList.get(position).getId(), "Electricity", arrayList.get(position));
    }

    private void showData() {
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                list.clear();
                for (DataSnapshot passSnapshot : dataSnapshot.getChildren()) {
                    Electricity passportModel = passSnapshot.getValue(Electricity.class);
                    list.add(passportModel);
                }
                try {
                    arrayList = getDataList(list);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                adapter = null;
                try {
                    adapter = new ElectricityAdapter(arrayList, mainActivity, listener);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                shimmerFrameLayout.stopShimmerAnimation();
                shimmerFrameLayout.setVisibility(View.GONE);
                recyclerView.setAdapter(adapter);
                if (list.isEmpty()) {
                    lottieAnimationView.playAnimation();
                    lottieAnimationView.setVisibility(View.VISIBLE);
                }
                else {
                    lottieAnimationView.pauseAnimation();
                    lottieAnimationView.setVisibility(View.GONE);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(mainActivity, "Something went wrong, Please try again later.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private ArrayList<Electricity> getDataList(ArrayList<Electricity> arrayList) throws Exception {
        ArrayList<Electricity> list = new ArrayList<>();
        for (int i = 0; i < arrayList.size(); i++) {
            Electricity cuur = arrayList.get(i);
            String id, name, email, number, userName, address, password, phone;
            id = cuur.getId();
            email = cuur.getEmail();
            address = cuur.getAddress();
            name = cuur.getName();
            phone = cuur.getPhone();
            number = cuur.getNumber();
            password = cuur.getPassword();
            userName = cuur.getUserName();
            list.add(new Electricity(id, mainActivity.decryptData(name, id), mainActivity.decryptData(number, id), mainActivity.decryptData(address, id), mainActivity.decryptData(email, id), mainActivity.decryptData(userName, id), mainActivity.decryptData(password, id), mainActivity.decryptData(phone, id)));
        }
        return list;
    }


    @Override
    public void onStart() {
        super.onStart();
        showData();
        mainActivity.collapseBottomSheet();
    }
}
