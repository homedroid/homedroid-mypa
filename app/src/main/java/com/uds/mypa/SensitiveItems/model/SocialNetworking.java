package com.uds.mypa.SensitiveItems.model;

import android.os.Parcel;
import android.os.Parcelable;

public class SocialNetworking implements Parcelable {
    private String id,company,name,userName,password,question,answer,recoveryMail,imageId;

    public SocialNetworking() {
    }

    public SocialNetworking(String id, String company, String name, String userName, String password, String question, String answer, String recoveryMail, String imageId) {
        this.id = id;
        this.company = company;
        this.name = name;
        this.userName = userName;
        this.password = password;
        this.question = question;
        this.answer = answer;
        this.recoveryMail = recoveryMail;
        this.imageId = imageId;
    }

    public String getId() {
        return id;
    }

    public String getCompany() {
        return company;
    }

    public String getName() {
        return name;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public String getQuestion() {
        return question;
    }

    public String getAnswer() {
        return answer;
    }

    public String getRecoveryMail() {
        return recoveryMail;
    }

    public String getImageId() {
        return imageId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}
