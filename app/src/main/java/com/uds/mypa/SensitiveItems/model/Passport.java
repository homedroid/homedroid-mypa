package com.uds.mypa.SensitiveItems.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Passport implements Parcelable {
    private String id, name, passportNumber, placeOfIssue, dateOfIssue, dateOfExpiry;

    public Passport() {
    }

    public Passport(String id, String name, String passportNumber, String placeOfIssue, String dateOfIssue, String dateOfExpiry) {
        this.id = id;
        this.name = name;
        this.passportNumber = passportNumber;
        this.placeOfIssue = placeOfIssue;
        this.dateOfIssue = dateOfIssue;
        this.dateOfExpiry = dateOfExpiry;
    }


    protected Passport(Parcel in) {
        id = in.readString();
        name = in.readString();
        passportNumber = in.readString();
        placeOfIssue = in.readString();
        dateOfIssue = in.readString();
        dateOfExpiry = in.readString();
    }

    public static final Creator<Passport> CREATOR = new Creator<Passport>() {
        @Override
        public Passport createFromParcel(Parcel in) {
            return new Passport(in);
        }

        @Override
        public Passport[] newArray(int size) {
            return new Passport[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPassportNumber() {
        return passportNumber;
    }

    public String getPlaceOfIssue() {
        return placeOfIssue;
    }

    public String getDateOfIssue() {
        return dateOfIssue;
    }

    public String getDateOfExpiry() {
        return dateOfExpiry;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(passportNumber);
        dest.writeString(placeOfIssue);
        dest.writeString(dateOfIssue);
        dest.writeString(dateOfExpiry);
    }
}
