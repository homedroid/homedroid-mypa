package com.uds.mypa.SensitiveItems.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.uds.mypa.Activitys.MainActivity;
import com.uds.mypa.R;
import com.uds.mypa.SensitiveItems.model.Wifi;

import java.util.ArrayList;
import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewWifiFragment extends Fragment {
    TextInputEditText mName, mPassword;
    private TextView mTitle;
    private ImageView mBack;
    private MainActivity mainActivity;
    private FirebaseAnalytics mFirebaseAnalytics;
    private String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
    private DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(uid);
    private Boolean save = true;

    public NewWifiFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
        mainActivity.collapseBottomSheet();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_wifi, container, false);
        myRef.keepSynced(true);
        initialize(view);
        mTitle.setText("Add Wifi Account");

        final FloatingActionButton saveFab, editFab;
        saveFab = view.findViewById(R.id.save_fab);
        editFab = view.findViewById(R.id.edit_fab);
        editFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleViews(true);
                editFab.setVisibility(View.GONE);
            }
        });
        Bundle bundle = getArguments();
        if (bundle != null) {
            Wifi wifi = bundle.getParcelable("wifi");
            mName.setText(wifi.getName());
            mPassword.setText(wifi.getPassword());
            uid = wifi.getId();
            save = false;
            if (bundle.getInt("choose") == 0) {
                toggleViews(false);
                editFab.setVisibility(View.VISIBLE);
            } else {
                toggleViews(true);
                editFab.setVisibility(View.GONE);
            }
        }

        saveFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (save)
                    saveData();
                else
                    updateData();
            }
        });
        return view;
    }

    private void toggleViews(boolean b) {
        mPassword.setEnabled(b);
        mName.setEnabled(b);
        mName.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mPassword.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        if (b)
            mName.requestFocus();
    }

    private void initialize(@NonNull View view) {
        mainActivity = (MainActivity) getActivity();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mainActivity);
        mBack = view.findViewById(R.id.back_arrow);
        mTitle = view.findViewById(R.id.title_TextView);
        mName = view.findViewById(R.id.wifi_name_editText);
        mPassword = view.findViewById(R.id.password_editText);

    }

    private void saveData() {
        if (checkValidation()) {
            String id = myRef.push().getKey();
            ArrayList<String> list = new ArrayList<>();
            String name = mName.getText().toString().trim();
            String password = mPassword.getText().toString().trim();
            list.add(name);
            list.add(password);
            ArrayList<String> encryptData = null;
            try {
                encryptData = mainActivity.encryptData(list, id);
            } catch (Exception e) {
                e.printStackTrace();
            }
            encryptData.add(0, id);
            Wifi wifiModel = new Wifi(id, encryptData.get(1), encryptData.get(2));
            myRef.child("Wifi").child(Objects.requireNonNull(id)).setValue(wifiModel);
            mainActivity.showAlertBox(getString(R.string.txt_Success),getString(R.string.txt_data_saved_success),2);
            mainActivity.goBack();
        }
    }

    private void updateData() {
        if (checkValidation()) {
            ArrayList<String> list = new ArrayList<>();
            String name = mName.getText().toString().trim();
            String password = mPassword.getText().toString().trim();
            list.add(name);
            list.add(password);
            ArrayList<String> encryptData = null;
            try {
                encryptData = mainActivity.encryptData(list, uid);
            } catch (Exception e) {
                e.printStackTrace();
            }
            encryptData.add(0, uid);
            Wifi wifiModel = new Wifi(uid, encryptData.get(1), encryptData.get(2));
            myRef.child("Wifi").child(Objects.requireNonNull(uid)).setValue(wifiModel);
            mainActivity.showAlertBox(getString(R.string.txt_Success),getString(R.string.txt_data_updated_success),2);
            mainActivity.goBack();
        }
    }

    private boolean checkValidation() {
        boolean check = true;
        if (TextUtils.isEmpty(mName.getText().toString())) {
            mName.setError("Cannot be Empty");
            check = false;
        }
        if (TextUtils.isEmpty(mPassword.getText().toString())) {
            mPassword.setError("Cannot be Empty");
            check = false;
        }
        return check;
    }


}
