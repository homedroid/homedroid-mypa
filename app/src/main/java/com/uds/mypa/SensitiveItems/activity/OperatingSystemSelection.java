package com.uds.mypa.SensitiveItems.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.uds.mypa.Interfaces.OnItemClickListener;
import com.uds.mypa.R;
import com.uds.mypa.SensitiveItems.adapters.CommonSelectionAdapter;
import com.uds.mypa.SensitiveItems.model.ListModel;

import java.util.ArrayList;
import java.util.List;

public class OperatingSystemSelection extends AppCompatActivity implements OnItemClickListener, SearchView.OnQueryTextListener {
    private ArrayList<ListModel> arrayList;
    private List<ListModel> commonList;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_operating_system_selection);
        arrayList = new ArrayList<>();
        initOSList();
        CommonSelectionAdapter adapter = new CommonSelectionAdapter(arrayList, getBaseContext(), OperatingSystemSelection.this);
        commonList = new ArrayList<>();
        commonList.clear();
        commonList = arrayList;
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        TextView mTitle = findViewById(R.id.title_TextView);
        mTitle.setText(getString(R.string.txt_select_bank));
        ImageView mBack = findViewById(R.id.back_arrow);
        SearchView mSearchView = findViewById(R.id.searchView);
        mBack.setVisibility(View.GONE);
        recyclerView.setAdapter(adapter);

        mSearchView.setOnQueryTextListener(this);
    }

    private void initOSList() {
        arrayList.add(new ListModel("Android", R.drawable.android));
        arrayList.add(new ListModel("CentOS", R.drawable.centos));
        arrayList.add(new ListModel("Debian", R.drawable.debian));
        arrayList.add(new ListModel("Fedora", R.drawable.fedora));
        arrayList.add(new ListModel("Generic Linux", R.drawable.linux));
        arrayList.add(new ListModel("iOS", R.drawable.ios));
        arrayList.add(new ListModel("Kali", R.drawable.kali));
        arrayList.add(new ListModel("MacOS", R.drawable.macos));
        arrayList.add(new ListModel("Open SUSE", R.drawable.open_suse));
        arrayList.add(new ListModel("Red Hat", R.drawable.redhat));
        arrayList.add(new ListModel("Ubuntu", R.drawable.ubuntu));
        arrayList.add(new ListModel("Windows 7", R.drawable.windows7));
        arrayList.add(new ListModel("Windows 8", R.drawable.windows_8));
        arrayList.add(new ListModel("Windows 10", R.drawable.windows_10));
        arrayList.add(new ListModel("Windows 2000", R.drawable.windows2000));
        arrayList.add(new ListModel("Windows Vista", R.drawable.windows_vista));
        arrayList.add(new ListModel("Windows XP", R.drawable.windowsxp));
        arrayList.add(new ListModel("Windows Sever", R.drawable.windows_8));
        arrayList.add(new ListModel("Other", R.drawable.os));
    }

    @Override
    public void onItemClick(int position) {
        Intent data = new Intent();
        data.putExtra("os_name", commonList.get(position).getName());
        data.putExtra("image_id", commonList.get(position).getImage());
        setResult(300, data);
        finish();
    }

    private void search(String newText) {
        ArrayList<ListModel> searchList = new ArrayList<>();
        for (ListModel obj : arrayList) {
            if (obj.getName().toLowerCase().contains(newText)) {
                searchList.add(obj);
            }
        }
        commonList = searchList;
        CommonSelectionAdapter adapter = new CommonSelectionAdapter(searchList, this, this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        search(newText);
        return false;
    }
}
