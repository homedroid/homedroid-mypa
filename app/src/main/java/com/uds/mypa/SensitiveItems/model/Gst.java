package com.uds.mypa.SensitiveItems.model;

public class Gst {
    private String gstinNumber,gstPortalID,GSTPortalPassword;

    public Gst() {
    }

    public Gst(String gstinNumber, String gstPortalID, String GSTPortalPassword) {
        this.gstinNumber = gstinNumber;
        this.gstPortalID = gstPortalID;
        this.GSTPortalPassword = GSTPortalPassword;
    }

    public String getGstinNumber() {
        return gstinNumber;
    }

    public String getGstPortalID() {
        return gstPortalID;
    }

    public String getGSTPortalPassword() {
        return GSTPortalPassword;
    }
}
