package com.uds.mypa.SensitiveItems.fragment;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.uds.mypa.Activitys.MainActivity;
import com.uds.mypa.R;
import com.uds.mypa.SensitiveItems.model.DrivingLicense;
import com.uds.mypa.utils.Utils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewDrivingLicenseFragment extends Fragment implements DatePickerDialog.OnDateSetListener {
    private TextInputLayout mNameLayout, mNumberLayout, mIssueLayout, mExpiryLayout;
    private TextInputEditText mName, mDlNumber, mDob, mFathersName, mAddress, dateOfIssue, validUpto;
    private TextView mTitle;
    private MainActivity mainActivity;
    private FirebaseAnalytics mFirebaseAnalytics;
    private ImageView mIssueDatePicker, mExpiryDatePicker, mDateOfBirthPicker;
    private String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
    private DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(uid);
    private Boolean save = true;
    private int key;
    private String date;
    private String issueDate = "";

    public NewDrivingLicenseFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
        mainActivity.collapseBottomSheet();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_driving_license, container, false);
        myRef.keepSynced(true);
        initialize(view);
        mTitle.setText("Add Driving License");
        mIssueDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        NewDrivingLicenseFragment.this, mYear, mMonth, mDay);
                datePickerDialog.show();
                key = 0;
            }
        });
        mExpiryDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        NewDrivingLicenseFragment.this, mYear, mMonth, mDay);
                datePickerDialog.show();
                key = 1;
            }
        });
        mDateOfBirthPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        NewDrivingLicenseFragment.this, mYear, mMonth, mDay);
                datePickerDialog.show();
                key = 2;
            }
        });
        final FloatingActionButton saveFab, editFab;
        saveFab = view.findViewById(R.id.save_fab);
        editFab = view.findViewById(R.id.edit_fab);
        editFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleViews(true);
                editFab.setVisibility(View.GONE);
            }
        });
        Bundle bundle = getArguments();
        if (bundle != null) {
            DrivingLicense drivingLicense = bundle.getParcelable("driving_license");
            mName.setText(drivingLicense.getName());
            mDlNumber.setText(drivingLicense.getDlNumber());
            mAddress.setText(drivingLicense.getAddress());
            mFathersName.setText(drivingLicense.getFathersName());
            validUpto.setText(drivingLicense.getValidUpto());
            dateOfIssue.setText(drivingLicense.getIssueDate());
            mDob.setText(drivingLicense.getDob());
            uid = drivingLicense.getId();
            save = false;
            if (bundle.getInt("choose") == 0) {
                toggleViews(false);
                editFab.setVisibility(View.VISIBLE);
            } else {
                toggleViews(true);
                editFab.setVisibility(View.GONE);
            }
        }
        saveFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (save)
                    saveData();
                else
                    updateData();
            }
        });

        return view;
    }

    private void initialize(@NonNull View view) {
        mainActivity = (MainActivity) getActivity();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mainActivity);
        validUpto = view.findViewById(R.id.expiryDate);
        mIssueDatePicker = view.findViewById(R.id.issue_date_picker_actions);
        mExpiryDatePicker = view.findViewById(R.id.expiry_date_picker_actions);
        mDateOfBirthPicker = view.findViewById(R.id.dob_picker_actions);
        mTitle = view.findViewById(R.id.title_TextView);
        mName = view.findViewById(R.id.name_editText);
        mDlNumber = view.findViewById(R.id.license_number_editText);
        mAddress = view.findViewById(R.id.address_editText);
        dateOfIssue = view.findViewById(R.id.issueDate);
        mDob = view.findViewById(R.id.date_of_birth_editText);
        mFathersName = view.findViewById(R.id.fathersName_editText);
        mNameLayout = view.findViewById(R.id.name_layout);
        mNumberLayout = view.findViewById(R.id.number_layout);
        mIssueLayout = view.findViewById(R.id.issue_date_layout);
        mExpiryLayout = view.findViewById(R.id.expiry_date_layout);

    }

    private boolean checkValidation() {
        boolean check = true;
        if (TextUtils.isEmpty(mName.getText().toString())) {
            mNameLayout.setError("Please Enter NAem");
            check = false;
        }
        if (TextUtils.isEmpty(mDlNumber.getText().toString())) {
            mNumberLayout.setError("Please Enter DL Number");
            check = false;
        }
        if (TextUtils.isEmpty(dateOfIssue.getText().toString())) {
            mIssueLayout.setError("Please Enter Issue Date");
            check = false;
        }
        if (TextUtils.isEmpty(validUpto.getText().toString())) {
            mExpiryLayout.setError("Please Enter Expiry Date");
            check = false;
        }

        return check;
    }

    private void toggleViews(boolean value) {
        mFathersName.setEnabled(value);
        mName.setEnabled(value);
        mDob.setEnabled(value);
        mDlNumber.setEnabled(value);
        mAddress.setEnabled(value);
        mIssueDatePicker.setEnabled(value);
        mExpiryDatePicker.setEnabled(value);
        validUpto.setEnabled(value);
        mDateOfBirthPicker.setEnabled(value);
        dateOfIssue.setEnabled(value);
        mFathersName.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mName.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mDob.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mDlNumber.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mAddress.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        validUpto.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        dateOfIssue.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        if (value)
            mName.requestFocus();
    }

    private void saveData() {
        if (checkValidation()) {
            String id = myRef.push().getKey();
            ArrayList<String> list = new ArrayList<>();
            String name = mName.getText().toString();
            String dlNumber = mDlNumber.getText().toString().trim();
            String fathersName = mFathersName.getText().toString().trim();
            String address = mAddress.getText().toString().trim();
            String issueDate = dateOfIssue.getText().toString().trim();
            String dob = mDob.getText().toString().trim();
            String valid = validUpto.getText().toString().trim();
            list.add(name);
            list.add(dlNumber);
            list.add(issueDate);
            list.add(valid);
            list.add(dob);
            list.add(fathersName);
            list.add(address);
            ArrayList<String> encryptData = null;
            try {
                encryptData = mainActivity.encryptData(list, id);
            } catch (Exception e) {
                e.printStackTrace();
            }
            encryptData.add(0, id);
            DrivingLicense drivingLicenseModel = new DrivingLicense(id, encryptData.get(1), encryptData.get(2), encryptData.get(3), encryptData.get(4), encryptData.get(5), encryptData.get(6), encryptData.get(7));
            myRef.child("Driving License").child(id).setValue(drivingLicenseModel);
            mainActivity.showAlertBox(getString(R.string.txt_Success), getString(R.string.txt_data_saved_success), 2);
            mainActivity.goBack();
        }
    }


    private void updateData() {
        if (checkValidation()) {
            ArrayList<String> list = new ArrayList<>();
            String name = mName.getText().toString();
            String dlNumber = mDlNumber.getText().toString().trim();
            String fathersName = mFathersName.getText().toString().trim();
            String address = mAddress.getText().toString().trim();
            String issueDate = dateOfIssue.getText().toString().trim();
            String dob = mDob.getText().toString().trim();
            String valid = validUpto.getText().toString().trim();
            list.add(name);
            list.add(dlNumber);
            list.add(issueDate);
            list.add(valid);
            list.add(dob);
            list.add(fathersName);
            list.add(address);
            ArrayList<String> encryptData = null;
            try {
                encryptData = mainActivity.encryptData(list, uid);
            } catch (Exception e) {
                e.printStackTrace();
            }
            encryptData.add(0, uid);
            DrivingLicense drivingLicenseModel = new DrivingLicense(uid, encryptData.get(1), encryptData.get(2), encryptData.get(3), encryptData.get(4), encryptData.get(5), encryptData.get(6), encryptData.get(7));
            myRef.child("Driving License").child(uid).setValue(drivingLicenseModel);
            mainActivity.showAlertBox(getString(R.string.txt_Success), getString(R.string.txt_data_updated_success), 2);
            mainActivity.goBack();
        }
    }


    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        if (key == 0) {

            date = Utils.formatDate(dayOfMonth, month, year);
            try {
                if (mainActivity.compareDate(date, null, true)) {
                    dateOfIssue.setText(date);
                    issueDate = date;
                } else {
                    dateOfIssue.setText("");
                    mainActivity.showAlertBox("Warning!", "Issue Date Cannot be greater than today date", 0);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else if (key == 2) {
            try {
                if (mainActivity.compareDate(date, null, true)) {
                    mDob.setText(Utils.formatDate(dayOfMonth, month, year));
                } else {
                    mDob.setText("");
                    mainActivity.showAlertBox("Warning!", "Date of Birth Cannot be greater than today date", 0);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
            try {
                if (mainActivity.compareDate(Utils.formatDate(dayOfMonth, month, year), date, false)) {
                    validUpto.setText(Utils.formatDate(dayOfMonth, month, year));
                } else {
                    validUpto.setText("");
                    mainActivity.showAlertBox("Warning!", "Expiry Date Cannot be before Issue date", 0);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

    }
}
