package com.uds.mypa.SensitiveItems.model;

import android.os.Parcel;
import android.os.Parcelable;

public class RemoteLogin implements Parcelable {
    private String id,appName,loginId,password;

    public RemoteLogin(String id, String appName, String loginId, String password) {
        this.id = id;
        this.appName = appName;
        this.loginId = loginId;
        this.password = password;
    }

    public RemoteLogin() {
    }

    protected RemoteLogin(Parcel in) {
        id = in.readString();
        appName = in.readString();
        loginId = in.readString();
        password = in.readString();
    }

    public static final Creator<RemoteLogin> CREATOR = new Creator<RemoteLogin>() {
        @Override
        public RemoteLogin createFromParcel(Parcel in) {
            return new RemoteLogin(in);
        }

        @Override
        public RemoteLogin[] newArray(int size) {
            return new RemoteLogin[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getAppName() {
        return appName;
    }

    public String getLoginId() {
        return loginId;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(appName);
        dest.writeString(loginId);
        dest.writeString(password);
    }
}
