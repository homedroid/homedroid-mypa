package com.uds.mypa.SensitiveItems.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Electricity implements Parcelable {
    private String id, name, number, address, email, userName, password, phone;

    public Electricity(String id, String name, String number, String address, String email, String userName, String password, String phone) {
        this.id = id;
        this.name = name;
        this.number = number;
        this.address = address;
        this.email = email;
        this.userName = userName;
        this.password = password;
        this.phone = phone;
    }

    public Electricity() {
    }

    protected Electricity(Parcel in) {
        id = in.readString();
        name = in.readString();
        number = in.readString();
        address = in.readString();
        email = in.readString();
        userName = in.readString();
        password = in.readString();
        phone = in.readString();
    }

    public static final Creator<Electricity> CREATOR = new Creator<Electricity>() {
        @Override
        public Electricity createFromParcel(Parcel in) {
            return new Electricity(in);
        }

        @Override
        public Electricity[] newArray(int size) {
            return new Electricity[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

    public String getAddress() {
        return address;
    }

    public String getEmail() {
        return email;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public String getPhone() {
        return phone;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(number);
        dest.writeString(address);
        dest.writeString(email);
        dest.writeString(userName);
        dest.writeString(password);
        dest.writeString(phone);
    }
}
