package com.uds.mypa.SensitiveItems.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.uds.mypa.Interfaces.OnItemClickListener;
import com.uds.mypa.R;
import com.uds.mypa.SensitiveItems.adapters.CommonSelectionAdapter;
import com.uds.mypa.SensitiveItems.model.ListModel;

import java.util.ArrayList;
import java.util.List;

public class BankSelectionActivity extends AppCompatActivity implements OnItemClickListener, SearchView.OnQueryTextListener {
    private ArrayList<ListModel> arrayList;
    private List<ListModel> commonList;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_selection);
        arrayList = new ArrayList<>();
        initBankList();
        CommonSelectionAdapter adapter = new CommonSelectionAdapter(arrayList, getBaseContext(), BankSelectionActivity.this);
        commonList = new ArrayList<>();
        commonList.clear();
        commonList=arrayList;
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        TextView mTitle = findViewById(R.id.title_TextView);
        mTitle.setText(getString(R.string.txt_select_bank));
        ImageView mBack = findViewById(R.id.back_arrow);
        SearchView mSearchView = findViewById(R.id.searchView);
        mBack.setVisibility(View.GONE);
        recyclerView.setAdapter(adapter);

        mSearchView.setOnQueryTextListener(this);
    }

    private void initBankList() {
        arrayList.add(new ListModel("Abhyudaya Cooperative Bank Ltd", R.drawable.ic_bank));
        arrayList.add(new ListModel("Adarsh Co-operative Bank Ltd", R.drawable.ic_bank));
        arrayList.add(new ListModel("Aditya Birla Idea Payments Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Airtel Payments Bank", R.drawable.airtel));
        arrayList.add(new ListModel("Allahabad Bank", R.drawable.allahabad));
        arrayList.add(new ListModel("Allahabad UP Gramin Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Andhra Bank", R.drawable.andhra));
        arrayList.add(new ListModel("Andhra Pradesh Grameena Vikas Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Andhra pragathi Grameena Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("A P Mahesh Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Apna Sahakari Bank", R.drawable.apnasahakari));
        arrayList.add(new ListModel("Assam Gramin Vikash Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Axis Bank", R.drawable.axis));
        arrayList.add(new ListModel("Bandhan Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Bank of Baroda", R.drawable.bankofbaroda));
        arrayList.add(new ListModel("Bank of India", R.drawable.boi));
        arrayList.add(new ListModel("Bank of Maharashtra", R.drawable.ic_bank));
        arrayList.add(new ListModel("Baroda Gujarat Gramin Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Baroda Rajasthan Kshetriya Gramin Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Baroda UP Gramin Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Bassein Catholic Co-operative Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("BHILWARA URBAN CO OPERATIVE BANK LTD", R.drawable.ic_bank));
        arrayList.add(new ListModel("Bihar Gramin Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Canara Bank", R.drawable.canara));
        arrayList.add(new ListModel("Catholic Syrian Bank", R.drawable.catholicsyrian));
        arrayList.add(new ListModel("Central  Bank of India", R.drawable.centralbankofindia));
        arrayList.add(new ListModel("Chaitanya Godavari Grameena Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Chattisgarh R G Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Citiic_bank", R.drawable.citi));
        arrayList.add(new ListModel("City Union Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Coastal Local Area  Ltd", R.drawable.ic_bank));
        arrayList.add(new ListModel("Corporation Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Cosmos Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("DBS Bank", R.drawable.dbs));
        arrayList.add(new ListModel("DCB", R.drawable.ic_bank));
        arrayList.add(new ListModel("Dena Bank", R.drawable.dena));
        arrayList.add(new ListModel("Dena Gujarat Gramin Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Deutsche Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Dhanlaxmi Bank Ltd", R.drawable.ic_bank));
        arrayList.add(new ListModel("Dombivali Nagri Sahakari Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Equitas Bank", R.drawable.equitas));
        arrayList.add(new ListModel("ESAF SMALL FINANCE BANK LTD", R.drawable.ic_bank));
        arrayList.add(new ListModel("Federal Bank", R.drawable.federal));
        arrayList.add(new ListModel("Fino Payments Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("GP Parsik Bank", R.drawable.gpparsik));
        arrayList.add(new ListModel("HDFC Bank", R.drawable.hdfc));
        arrayList.add(new ListModel("Himachal Pradesh Gramin Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("HSBC", R.drawable.hsbc));
        arrayList.add(new ListModel("Hutatma Sahakari Bank Ltd", R.drawable.ic_bank));
        arrayList.add(new ListModel("ICICI Bank", R.drawable.icici));
        arrayList.add(new ListModel("IDBI Bank", R.drawable.idbi));
        arrayList.add(new ListModel("IDFC Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Indian Bank", R.drawable.indian));
        arrayList.add(new ListModel("Indian overseas Bank", R.drawable.overseas));
        arrayList.add(new ListModel("India Post Payment Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Indusland Bank", R.drawable.indusland));
        arrayList.add(new ListModel("J&K Grameen Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Jalgaon janata Sahakari Bank Ltd Jalgaon", R.drawable.ic_bank));
        arrayList.add(new ListModel("Jammu & Kashmir Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Jana Small Finance", R.drawable.ic_bank));
        arrayList.add(new ListModel("Janta Sahakari Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Jio Payments Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("KAIJSB - Ichalkaranji", R.drawable.ic_bank));
        arrayList.add(new ListModel("Kalyan Janta Sahakari Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Karnataka Bank", R.drawable.karnataka));
        arrayList.add(new ListModel("Karnataka Vikas Grameena Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Karur Vysya Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Kashi Gomti Samyut Grameen Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("KAVERI GRAMEENA BANK", R.drawable.ic_bank));
        arrayList.add(new ListModel("Kerala Gramin Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Kotak Mahindra Bank", R.drawable.kotak));
        arrayList.add(new ListModel("Lakshmi Vilas Bank", R.drawable.lakshmi));
        arrayList.add(new ListModel("Langpi dehangi Rural Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Madhya bihar Gramin Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Maharashtra Gramin Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Maharashtra State Co-op Bank Ltd", R.drawable.ic_bank));
        arrayList.add(new ListModel("Malwa Gramin Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Manipur rural modelArrayList", R.drawable.ic_bank));
        arrayList.add(new ListModel("Maratha Cooperative Bank Ltd", R.drawable.ic_bank));
        arrayList.add(new ListModel("Meghalaya Rural Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Mehsana Urban Bank", R.drawable.mehsana));
        arrayList.add(new ListModel("Mehsana Urban Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Mizoram Rural Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Nainital Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("NIKGSB Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Oriental modelArrayList of Commerce", R.drawable.obc));
        arrayList.add(new ListModel("PASCHIM BANGA GRAMIN BANK", R.drawable.ic_bank));
        arrayList.add(new ListModel("Paytm Payments Bank", R.drawable.paytm));
        arrayList.add(new ListModel("PMC Bank", R.drawable.pmc));
        arrayList.add(new ListModel("Pragathi Krishna Gramin Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Prathama Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Punjab and Sind Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Punjab Gramin Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Punjab National  Bank", R.drawable.pnb));
        arrayList.add(new ListModel("Purvanchal Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Rajasthan Marudhara Gramin Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Rajkot Nagrik Sahakari Bank", R.drawable.rajkot));
        arrayList.add(new ListModel("RBL", R.drawable.rbl));
        arrayList.add(new ListModel("Samruddhi Co-operative Bank Ltd., Nagpur", R.drawable.ic_bank));
        arrayList.add(new ListModel("Saraswat Bank", R.drawable.saraswat));
        arrayList.add(new ListModel("Sarva Haryana Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("SARVA UP GTRAMIN BANK", R.drawable.ic_bank));
        arrayList.add(new ListModel("Saurashtra Gramin Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("SHREE KADI NAGRIK SAHAKARI BANK LTD", R.drawable.ic_bank));
        arrayList.add(new ListModel("South Indian Bank", R.drawable.southindian));
        arrayList.add(new ListModel("Standard Chartered", R.drawable.chartered));
        arrayList.add(new ListModel("State Bank of India", R.drawable.sbi));
        arrayList.add(new ListModel("SUCO SOUHARDA SAHAKARI BANK LTD", R.drawable.ic_bank));
        arrayList.add(new ListModel("Surat People Cooperative Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Suryoday Small Finance Bank Ltd", R.drawable.ic_bank));
        arrayList.add(new ListModel("SVC Co-operative Bank Ltd", R.drawable.ic_bank));
        arrayList.add(new ListModel("Syndicate Bank", R.drawable.syndicate));
        arrayList.add(new ListModel("Tamilnad Mercantile Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Telangana Grameena Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Telangana State Co Op Apex Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Thane Bharat Sahakari Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("The Ahmedabad District Cooperative Bank Ltd", R.drawable.ic_bank));
        arrayList.add(new ListModel("The Ahmedabad Mercantile Cooperative Bank Ltd", R.drawable.ic_bank));
        arrayList.add(new ListModel("The Andhra Pradesh State Co-operative Bank Ltd", R.drawable.ic_bank));
        arrayList.add(new ListModel("The Gujarat State Cooperative Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("The Hasti Co-operative Bank Ltd", R.drawable.ic_bank));
        arrayList.add(new ListModel("The Mahanagar Co.Op. Bank Ltd", R.drawable.ic_bank));
        arrayList.add(new ListModel("The Malad Sahakari modelArrayList Ltd", R.drawable.ic_bank));
        arrayList.add(new ListModel("The Municipal Co-op Bank ltd", R.drawable.ic_bank));
        arrayList.add(new ListModel("The Muslim Cooperative Bank Ltd", R.drawable.ic_bank));
        arrayList.add(new ListModel("The Sutex Co.op. Bank Ltd", R.drawable.ic_bank));
        arrayList.add(new ListModel("THE UDAIPUR MAHILA SAMRIDHI URBAN COOPERATIVE ", R.drawable.ic_bank));
        arrayList.add(new ListModel("THE UDAIPUR MAHILA URBAN CO OPERATIVE", R.drawable.ic_bank));
        arrayList.add(new ListModel("THE URBAN CO OP BANK LTD DHARANGAON", R.drawable.ic_bank));
        arrayList.add(new ListModel("The varachha Co-Op BAnk Ltd", R.drawable.ic_bank));
        arrayList.add(new ListModel("THE VIJAY COOPERATIVE BANK LTD", R.drawable.ic_bank));
        arrayList.add(new ListModel("The Vishweshwar Sahakari Bank Ltd, Pune", R.drawable.ic_bank));
        arrayList.add(new ListModel("TJSB Bank", R.drawable.tjsb));
        arrayList.add(new ListModel("Tripura Gramin Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("UCO Bank", R.drawable.uco));
        arrayList.add(new ListModel("Ujjivan Small Finance Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Union Bank of India", R.drawable.union));
        arrayList.add(new ListModel("United Bank of India", R.drawable.united));
        arrayList.add(new ListModel("Uttrakhand Gramin Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Vanachal Gramin Bank", R.drawable.ic_bank));
        arrayList.add(new ListModel("Vasai vikas Sahakari Bank", R.drawable.vasaivikas));
        arrayList.add(new ListModel("Vijaya Bank", R.drawable.vijayabank));
        arrayList.add(new ListModel("Yes Bank", R.drawable.yesbank));
    }


    @Override
    public void onItemClick(int position) {
        Intent data = new Intent();
        data.putExtra("bank_name", commonList.get(position).getName());
        data.putExtra("image_id", commonList.get(position).getImage());
        setResult(100, data);
        finish();
    }

    private void search(String newText) {
        ArrayList<ListModel> searchList = new ArrayList<>();
        for (ListModel obj : arrayList) {
            if (obj.getName().toLowerCase().contains(newText)) {
                searchList.add(obj);
            }
        }
        commonList=searchList;
        CommonSelectionAdapter adapter = new CommonSelectionAdapter(searchList, this, this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        search(newText);
        return false;
    }
}
