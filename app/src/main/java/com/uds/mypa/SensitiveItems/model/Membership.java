package com.uds.mypa.SensitiveItems.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Membership implements Parcelable {
    private String id,organization,name,number,expiry;

    public Membership(String id, String organization, String name, String number, String expiry) {
        this.id = id;
        this.organization = organization;
        this.name = name;
        this.number = number;
        this.expiry = expiry;
    }

    public Membership() {
    }

    protected Membership(Parcel in) {
        id = in.readString();
        organization = in.readString();
        name = in.readString();
        number = in.readString();
        expiry = in.readString();
    }

    public static final Creator<Membership> CREATOR = new Creator<Membership>() {
        @Override
        public Membership createFromParcel(Parcel in) {
            return new Membership(in);
        }

        @Override
        public Membership[] newArray(int size) {
            return new Membership[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getOrganization() {
        return organization;
    }

    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

    public String getExpiry() {
        return expiry;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(organization);
        dest.writeString(name);
        dest.writeString(number);
        dest.writeString(expiry);
    }
}
