package com.uds.mypa.SensitiveItems.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.uds.mypa.Interfaces.OnItemClickListener;
import com.uds.mypa.R;
import com.uds.mypa.SensitiveItems.adapters.CommonSelectionAdapter;
import com.uds.mypa.SensitiveItems.model.ListModel;

import java.util.ArrayList;
import java.util.List;

public class CompanySelection extends AppCompatActivity implements OnItemClickListener, SearchView.OnQueryTextListener {
    private ArrayList<ListModel> arrayList;
    private List<ListModel> commonList;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_selection);
        arrayList = new ArrayList<>();
        initCompanyList();
        CommonSelectionAdapter adapter = new CommonSelectionAdapter(arrayList, getBaseContext(), CompanySelection.this);
        commonList = new ArrayList<>();
        commonList.clear();
        commonList = arrayList;
        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        TextView mTitle = findViewById(R.id.title_TextView);
        mTitle.setText(getString(R.string.txt_choose_company_name));
        ImageView mBack = findViewById(R.id.back_arrow);
        SearchView mSearchView = findViewById(R.id.searchView);
        mBack.setVisibility(View.GONE);
        recyclerView.setAdapter(adapter);

        mSearchView.setOnQueryTextListener(this);
    }

    private void initCompanyList() {
        arrayList.add(new ListModel("Instagram", R.drawable.instagram));
        arrayList.add(new ListModel("Twitter", R.drawable.twitter));
        arrayList.add(new ListModel("Snapchat", R.drawable.snapchat));
        arrayList.add(new ListModel("YouTube", R.drawable.youtube));
        arrayList.add(new ListModel("Twitch", R.drawable.twitch));
        arrayList.add(new ListModel("WhatsApp", R.drawable.whatsapp));
        arrayList.add(new ListModel("Line", R.drawable.line));
        arrayList.add(new ListModel("Kik", R.drawable.kik));
        arrayList.add(new ListModel("Pintrest", R.drawable.pinterest));
        arrayList.add(new ListModel("Tumbler", R.drawable.tumblr));
        arrayList.add(new ListModel("Ask.fm", R.drawable.askfm));
        arrayList.add(new ListModel("SoundCloud", R.drawable.soundcloud));
        arrayList.add(new ListModel("Spotify", R.drawable.spotify));
        arrayList.add(new ListModel("LinkedIn", R.drawable.linkedin));
        arrayList.add(new ListModel("Skype", R.drawable.skype));
        arrayList.add(new ListModel("QQ", R.drawable.qq));
        arrayList.add(new ListModel("KakaoTalk", R.drawable.katalk));
        arrayList.add(new ListModel("VK", R.drawable.vk));
        arrayList.add(new ListModel("OK", R.drawable.ok));
        arrayList.add(new ListModel("BBM", R.drawable.bbm));
        arrayList.add(new ListModel("GitHub", R.drawable.github_logo));
        arrayList.add(new ListModel("Other", R.drawable.other_social));


    }

    private void search(String newText) {
        ArrayList<ListModel> searchList = new ArrayList<>();
        for (ListModel obj : arrayList) {
            if (obj.getName().toLowerCase().contains(newText)) {
                searchList.add(obj);
            }
        }
        commonList = searchList;
        CommonSelectionAdapter adapter = new CommonSelectionAdapter(searchList, this, this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        search(newText);
        return false;
    }

    @Override
    public void onItemClick(int position) {
        Intent data = new Intent();
        data.putExtra("company_name", commonList.get(position).getName());
        data.putExtra("image_id", commonList.get(position).getImage());
        setResult(200, data);
        finish();
    }
}
