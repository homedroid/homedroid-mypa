package com.uds.mypa.SensitiveItems.fragment;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.uds.mypa.Activitys.MainActivity;
import com.uds.mypa.R;
import com.uds.mypa.SensitiveItems.model.AmazonBusinessAccount;
import com.uds.mypa.SensitiveItems.model.Aws;
import com.uds.mypa.SensitiveItems.model.CompanyProfile;
import com.uds.mypa.SensitiveItems.model.CurrentAccounts;
import com.uds.mypa.SensitiveItems.model.DomainAccount;
import com.uds.mypa.SensitiveItems.model.Email;
import com.uds.mypa.SensitiveItems.model.EmailAccount;
import com.uds.mypa.SensitiveItems.model.Gst;
import com.uds.mypa.SensitiveItems.model.Legal;
import com.uds.mypa.SensitiveItems.model.Passport;
import com.uds.mypa.SensitiveItems.model.RailwayAccount;
import com.uds.mypa.SensitiveItems.model.WorkplaceByFacebook;
import com.uds.mypa.utils.Utils;

import net.cachapa.expandablelayout.ExpandableLayout;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewCompanyProfileFragment extends Fragment implements View.OnClickListener, DatePickerDialog.OnDateSetListener {
    private TextInputLayout mVentureNameLayout, mDirectorNumLayout, mEmailLayout, mRailPhoneLayout, mRailEmailLayout;
    private MaterialCardView legalCard, currentAccountCard, gstCard, emailCard, domainCard, railwayCard, workplaceCard, amazonBusinessCard, awsCard;
    private ExpandableLayout legalExpLayout, currentAccountExpLayout, gstExpLayout, emailExpLayout, domainExpLayout, railwayExpLayout, workplaceExpLayout, amazonBusinessExpLayout, awsExpLayout;
    private TextView mTitle;
    private TextInputEditText mVenture, mNumDirectors, mCin, mIncorporationDate, mheadOfficeAddress, mPan, mTan;
    private TextInputEditText mBankName, mAccountNumber, mIfsc, mCompanyId, mEstatementPass1, mEstatementPass2, mNetBankingId, mNetBankingPassword, mPin, mVpa;
    private TextInputEditText mGstNumber, mGstPortalId, mGstPortalPassword;
    private TextInputEditText mEmail, mEmailPassword;
    private TextInputEditText mRailId, mRailPassword, mRailAccountName, mDob, mRailEmail, mRailPhone;
    private TextInputEditText mDomainId, mDomainPassword;
    private TextInputEditText mWorkplaceId, mWorkplacePassword;
    private TextInputEditText mAmazonBusinessId, mAmazonBusinessPassword;
    private TextInputEditText mAwsId, mAwsPassword, mAwsServerId, mAwsServerPassword;
    private ImageView mArrow1, mArrow2, mArrow3, mArrow4, mArrow5, mArrow6, mArrow7, mArrow8, mArrow9;
    private MainActivity mainActivity;
    private FirebaseAnalytics mFirebaseAnalytics;
    private ImageView mIncorporationDatePicker, mDobPicker;
    private String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
    private DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(uid);
    private Boolean save = true;
    int key;

    public NewCompanyProfileFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_company_profile, container, false);
        myRef.keepSynced(true);
        initialize(view);
        mTitle.setText("Add Company Profile");
        legalCard.setOnClickListener(this);
        currentAccountCard.setOnClickListener(this);
        gstCard.setOnClickListener(this);
        emailCard.setOnClickListener(this);
        domainCard.setOnClickListener(this);
        railwayCard.setOnClickListener(this);
        workplaceCard.setOnClickListener(this);
        amazonBusinessCard.setOnClickListener(this);
        awsCard.setOnClickListener(this);
        final FloatingActionButton saveFab, editFab;
        saveFab = view.findViewById(R.id.save_fab);
        editFab = view.findViewById(R.id.edit_fab);
        editFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleViews(true);
                editFab.setVisibility(View.GONE);
            }
        });
        Bundle bundle = getArguments();
        if (bundle != null) {
            CompanyProfile companyProfile = bundle.getParcelable("companyProfile");
            setData(companyProfile);
            save = false;
            if (bundle.getInt("choose") == 0) {
                toggleViews(false);
                editFab.setVisibility(View.VISIBLE);
            } else {
                toggleViews(true);
                editFab.setVisibility(View.GONE);
            }

        }
        saveFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (save)
                    saveData();
                else
                    updateData();
            }
        });
        mIncorporationDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        NewCompanyProfileFragment.this, mYear, mMonth, mDay);
                datePickerDialog.show();
                key = 0;
            }
        });
        mDobPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        NewCompanyProfileFragment.this, mYear, mMonth, mDay);
                datePickerDialog.show();
                key = 1;
            }
        });
        return view;
    }

    private void setData(CompanyProfile companyProfile) {
        uid = companyProfile.getId();
        Legal legal = companyProfile.getLegal();
        CurrentAccounts currentAccounts = companyProfile.getCurrentAccounts();
        Gst gst = companyProfile.getGst();
        EmailAccount emailAccount = companyProfile.getEmailAccount();
        DomainAccount domainAccount = companyProfile.getDomainAccount();
        RailwayAccount railwayAccount = companyProfile.getRailwayAccount();
        WorkplaceByFacebook workplace = companyProfile.getWorkplaceByFacebook();
        AmazonBusinessAccount amazonBusinessAccount = companyProfile.getAmazonBusinessAccount();
        Aws aws = companyProfile.getAws();

        mVenture.setText(legal.getVentureName());
        mNumDirectors.setText(legal.getNumberOfDirectors());
        mCin.setText(legal.getCIN());
        mIncorporationDate.setText(legal.getIncorporationDate());
        mheadOfficeAddress.setText(legal.getHeadOfficeAddress());
        mPan.setText(legal.getPAN());
        mTan.setText(legal.getTAN());

        mBankName.setText(currentAccounts.getBankName());
        mAccountNumber.setText(currentAccounts.getAccountNumber());
        mIfsc.setText(currentAccounts.getIfscCode());
        mCompanyId.setText(currentAccounts.getCompanyID());
        mEstatementPass1.setText(currentAccounts.geteStatementPassword1());
        mEstatementPass2.setText(currentAccounts.geteStatementPassword2());
        mNetBankingId.setText(currentAccounts.getNetBankingID());
        mNetBankingPassword.setText(currentAccounts.getNetBankingPassword());
        mPin.setText(currentAccounts.getmPin());
        mVpa.setText(currentAccounts.getVpa());

        mGstNumber.setText(gst.getGstinNumber());
        mGstPortalId.setText(gst.getGstPortalID());
        mGstPortalPassword.setText(gst.getGSTPortalPassword());

        mEmail.setText(emailAccount.getEmail());
        mEmailPassword.setText(emailAccount.getPassword());

        mDomainId.setText(domainAccount.getId());
        mDomainPassword.setText(domainAccount.getPassword());

        mRailId.setText(railwayAccount.getId());
        mRailPassword.setText(railwayAccount.getPassword());
        mRailAccountName.setText(railwayAccount.getName());
        mDob.setText(railwayAccount.getDateOfBirth());
        mRailPhone.setText(railwayAccount.getPhone());
        mRailEmail.setText(railwayAccount.getEmail());

        mWorkplaceId.setText(workplace.getId());
        mWorkplacePassword.setText(workplace.getPassword());

        mAmazonBusinessId.setText(amazonBusinessAccount.getId());
        mAmazonBusinessPassword.setText(amazonBusinessAccount.getPassword());

        mAwsId.setText(aws.getId());
        mAwsPassword.setText(aws.getPassword());
        mAwsServerId.setText(aws.getParseServerID());
        mAwsServerPassword.setText(aws.getParseServerPassword());

    }


    private void initialize(View view) {
        mainActivity = (MainActivity) getActivity();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mainActivity);
        mTitle = view.findViewById(R.id.title_TextView);
        legalCard = view.findViewById(R.id.legal_card);
        legalExpLayout = view.findViewById(R.id.legal_layout);
        currentAccountCard = view.findViewById(R.id.current_account_card);
        currentAccountExpLayout = view.findViewById(R.id.current_account_layout);
        gstCard = view.findViewById(R.id.gst_card);
        gstExpLayout = view.findViewById(R.id.gst_layout);
        emailCard = view.findViewById(R.id.email_card);
        emailExpLayout = view.findViewById(R.id.email_account_layout);
        domainCard = view.findViewById(R.id.domain_card);
        domainExpLayout = view.findViewById(R.id.domain_layout);
        railwayCard = view.findViewById(R.id.railway_card);
        railwayExpLayout = view.findViewById(R.id.railway_layout);
        workplaceCard = view.findViewById(R.id.workplace_card);
        workplaceExpLayout = view.findViewById(R.id.workplace_layout);
        amazonBusinessCard = view.findViewById(R.id.amazon_business_card);
        amazonBusinessExpLayout = view.findViewById(R.id.amazon_business_layout);
        mVentureNameLayout = view.findViewById(R.id.venture_name_layout);
        mDirectorNumLayout = view.findViewById(R.id.directors_layout);
        mEmailLayout = view.findViewById(R.id.email_layout);
        mRailEmailLayout = view.findViewById(R.id.rail_email_layout);
        mRailPhoneLayout = view.findViewById(R.id.rail_phone_layout);
        awsCard = view.findViewById(R.id.aws_card);
        awsExpLayout = view.findViewById(R.id.aws_layout);
        mArrow1 = view.findViewById(R.id.legal_arrow);
        mArrow2 = view.findViewById(R.id.current_arrow);
        mArrow3 = view.findViewById(R.id.gst_arrow);
        mArrow4 = view.findViewById(R.id.email_arrow);
        mArrow5 = view.findViewById(R.id.domain_arrow);
        mArrow6 = view.findViewById(R.id.railway_arrow);
        mArrow7 = view.findViewById(R.id.workplace_arrow);
        mArrow8 = view.findViewById(R.id.amazon_arrow);
        mArrow9 = view.findViewById(R.id.aws_arrow);
        //Legal
        mVenture = view.findViewById(R.id.venture_name_editText);
        mNumDirectors = view.findViewById(R.id.number_of_directors_editText);
        mCin = view.findViewById(R.id.cin_editText);
        mIncorporationDate = view.findViewById(R.id.incorporation_date_editText);
        mheadOfficeAddress = view.findViewById(R.id.office_address_editText);
        mPan = view.findViewById(R.id.pan_editText);
        mTan = view.findViewById(R.id.tan_editText);
        mIncorporationDatePicker = view.findViewById(R.id.incorporation_picker_actions);
        //currentAccounts
        mBankName = view.findViewById(R.id.bankname_editText);
        mAccountNumber = view.findViewById(R.id.account_number_editText);
        mIfsc = view.findViewById(R.id.ifsc_editText);
        mCompanyId = view.findViewById(R.id.company_id_editText);
        mEstatementPass1 = view.findViewById(R.id.e_statement_password1_editText);
        mEstatementPass2 = view.findViewById(R.id.e_statement_password2_editText);
        mNetBankingId = view.findViewById(R.id.netbanking_id_editText);
        mNetBankingPassword = view.findViewById(R.id.netbanking_password_editText);
        mPin = view.findViewById(R.id.mpin_editText);
        mVpa = view.findViewById(R.id.vpa_editText);
        //gst
        mGstNumber = view.findViewById(R.id.gst_number_editText);
        mGstPortalId = view.findViewById(R.id.gst_portal_id_editText);
        mGstPortalPassword = view.findViewById(R.id.gst_portal_password_editText);
        //email account
        mEmail = view.findViewById(R.id.email_editText);
        mEmailPassword = view.findViewById(R.id.email_password_editText);
        //Domain Account
        mDomainId = view.findViewById(R.id.domain_id_editText);
        mDomainPassword = view.findViewById(R.id.domain_password_editText);
        //Railway Account
        mRailId = view.findViewById(R.id.rail_id_editText);
        mRailPassword = view.findViewById(R.id.rail_password_editText);
        mRailAccountName = view.findViewById(R.id.rail_name_editText);
        mDob = view.findViewById(R.id.rail_date_of_birth);
        mRailEmail = view.findViewById(R.id.rail_email_editText);
        mRailPhone = view.findViewById(R.id.rail_phone_editText);
        mDobPicker = view.findViewById(R.id.dob_picker_actions);
        //workplace by facebook
        mWorkplaceId = view.findViewById(R.id.workplace_id_editText);
        mWorkplacePassword = view.findViewById(R.id.workplace_password_editText);
        //Amazon Business Account
        mAmazonBusinessId = view.findViewById(R.id.amazon_id_editText);
        mAmazonBusinessPassword = view.findViewById(R.id.amazon_password_editText);
        //aws
        mAwsId = view.findViewById(R.id.aws_id_editText);
        mAwsPassword = view.findViewById(R.id.aws_password_editText);
        mAwsServerId = view.findViewById(R.id.aws_server_editText);
        mAwsServerPassword = view.findViewById(R.id.aws_server_password_editText);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.legal_card:
                legalExpLayout.toggle();
                currentAccountExpLayout.collapse();
                gstExpLayout.collapse();
                emailExpLayout.collapse();
                domainExpLayout.collapse();
                mArrow1.setImageResource(legalExpLayout.isExpanded() ? R.drawable.ic_arrow_up_white : R.drawable.ic_arrow_down_white);
                break;
            case R.id.current_account_card:
                currentAccountExpLayout.toggle();
                legalExpLayout.collapse();
                gstExpLayout.collapse();
                emailExpLayout.collapse();
                domainExpLayout.collapse();
                railwayExpLayout.collapse();
                workplaceExpLayout.collapse();
                amazonBusinessExpLayout.collapse();
                awsExpLayout.collapse();
                mArrow2.setImageResource(currentAccountExpLayout.isExpanded() ? R.drawable.ic_arrow_up_white : R.drawable.ic_arrow_down_white);
                break;
            case R.id.gst_card:
                gstExpLayout.toggle();
                legalExpLayout.collapse();
                currentAccountExpLayout.collapse();
                emailExpLayout.collapse();
                domainExpLayout.collapse();
                railwayExpLayout.collapse();
                workplaceExpLayout.collapse();
                amazonBusinessExpLayout.collapse();
                awsExpLayout.collapse();
                mArrow3.setImageResource(gstExpLayout.isExpanded() ? R.drawable.ic_arrow_up_white : R.drawable.ic_arrow_down_white);
                break;
            case R.id.email_card:
                emailExpLayout.toggle();
                legalExpLayout.collapse();
                gstExpLayout.collapse();
                currentAccountExpLayout.collapse();
                domainExpLayout.collapse();
                railwayExpLayout.collapse();
                workplaceExpLayout.collapse();
                amazonBusinessExpLayout.collapse();
                awsExpLayout.collapse();
                mArrow4.setImageResource(emailExpLayout.isExpanded() ? R.drawable.ic_arrow_up_white : R.drawable.ic_arrow_down_white);
                break;
            case R.id.domain_card:
                domainExpLayout.toggle();
                legalExpLayout.collapse();
                gstExpLayout.collapse();
                emailExpLayout.collapse();
                currentAccountExpLayout.collapse();
                railwayExpLayout.collapse();
                workplaceExpLayout.collapse();
                amazonBusinessExpLayout.collapse();
                awsExpLayout.collapse();
                mArrow5.setImageResource(domainExpLayout.isExpanded() ? R.drawable.ic_arrow_up_white : R.drawable.ic_arrow_down_white);
                break;
            case R.id.railway_card:
                railwayExpLayout.toggle();
                legalExpLayout.collapse();
                gstExpLayout.collapse();
                emailExpLayout.collapse();
                domainExpLayout.collapse();
                currentAccountExpLayout.collapse();
                workplaceExpLayout.collapse();
                amazonBusinessExpLayout.collapse();
                awsExpLayout.collapse();
                mArrow6.setImageResource(railwayExpLayout.isExpanded() ? R.drawable.ic_arrow_up_white : R.drawable.ic_arrow_down_white);
                break;
            case R.id.workplace_card:
                workplaceExpLayout.toggle();
                legalExpLayout.collapse();
                gstExpLayout.collapse();
                emailExpLayout.collapse();
                domainExpLayout.collapse();
                currentAccountExpLayout.collapse();
                railwayExpLayout.collapse();
                amazonBusinessExpLayout.collapse();
                awsExpLayout.collapse();
                mArrow7.setImageResource(workplaceExpLayout.isExpanded() ? R.drawable.ic_arrow_up_white : R.drawable.ic_arrow_down_white);
                break;
            case R.id.amazon_business_card:
                amazonBusinessExpLayout.toggle();
                legalExpLayout.collapse();
                gstExpLayout.collapse();
                emailExpLayout.collapse();
                domainExpLayout.collapse();
                currentAccountExpLayout.collapse();
                workplaceExpLayout.collapse();
                railwayExpLayout.collapse();
                awsExpLayout.collapse();
                mArrow8.setImageResource(amazonBusinessExpLayout.isExpanded() ? R.drawable.ic_arrow_up_white : R.drawable.ic_arrow_down_white);
                break;
            case R.id.aws_card:
                awsExpLayout.toggle();
                legalExpLayout.collapse();
                gstExpLayout.collapse();
                emailExpLayout.collapse();
                domainExpLayout.collapse();
                currentAccountExpLayout.collapse();
                workplaceExpLayout.collapse();
                amazonBusinessExpLayout.collapse();
                railwayExpLayout.collapse();
                mArrow9.setImageResource(awsExpLayout.isExpanded() ? R.drawable.ic_arrow_up_white : R.drawable.ic_arrow_down_white);
                break;
        }
    }

    private void updateData() {
        if (checkValidation()) {
            CompanyProfile companyProfile=commonSave(uid);
            myRef.child("Company Profile").child(uid).setValue(companyProfile).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    mainActivity.showAlertBox(getString(R.string.txt_error), getString(R.string.txt_error_data_save), 1);
                    Toast.makeText(mainActivity, e.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
                }
            });
            mainActivity.showAlertBox(getString(R.string.txt_Success), getString(R.string.txt_data_saved_success), 2);
            mainActivity.goBack();

        }
    }

    @Override
    public void onStart() {
        super.onStart();
        mainActivity.collapseBottomSheet();
    }

    private void saveData() {
        if (checkValidation()) {
            String id = myRef.push().getKey();
            if (id != null) {
                CompanyProfile companyProfile=commonSave(id);
                myRef.child("Company Profile").child(id).setValue(companyProfile).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(mainActivity, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
                mainActivity.showAlertBox(getString(R.string.txt_Success), getString(R.string.txt_data_saved_success), 2);
                mainActivity.goBack();
            } else
                mainActivity.showAlertBox(getString(R.string.txt_error), getString(R.string.txt_error_data_save), 1);
        }
    }

    private boolean checkValidation() {
        boolean check = true;
        if (TextUtils.isEmpty(mVenture.getText().toString())) {
            mVentureNameLayout.setError("Please Enter Venture Name");
            legalExpLayout.expand();
            check = false;
        }
        if (TextUtils.isEmpty(mNumDirectors.getText().toString())) {
            mDirectorNumLayout.setError("Please Enter Number of Directors");
            check = false;
            legalExpLayout.expand();
        }
        if (!TextUtils.isEmpty(mRailPhone.getText().toString()))
            if (!mainActivity.isValidPhone(mRailPhone.getText().toString())) {
                mRailPhoneLayout.setError("Invalid Phone Number");
                check = false;
                railwayExpLayout.expand();
            }
        if (!TextUtils.isEmpty(mRailEmail.getText().toString()))
            if (!mainActivity.isValidEmail(mRailEmail.getText().toString())) {
                mRailEmailLayout.setError("Invalid Email Address");
                check = false;
                railwayExpLayout.expand();
            }
        if (!TextUtils.isEmpty(mEmail.getText().toString()))
            if (!mainActivity.isValidEmail(mEmail.getText().toString())) {
                mEmailLayout.setError("Invalid Email Address");
                check = false;
                emailExpLayout.expand();
            }
        return check;
    }

    private CompanyProfile commonSave(String id) {
        ArrayList<String> encryptLegalList = null, encryptCurrentAccountList = null, encryptGstList = null, encryptEmailList = null,
                encryptDomainList = null, encryptRailList = null, encryptWorkplaceList = null, encryptAmazonBusinessList = null, encryptAwsList = null;
        try {
            encryptLegalList = getLegalData();
            encryptCurrentAccountList = mainActivity.encryptData(getCurrentAccountData(), id);
            encryptGstList = mainActivity.encryptData(getGstData(), id);
            encryptEmailList = mainActivity.encryptData(getEmailAccountData(), id);
            encryptDomainList = mainActivity.encryptData(getDomainAccountData(), id);
            encryptRailList = mainActivity.encryptData(getRailwayAccountData(), id);
            encryptWorkplaceList = mainActivity.encryptData(getFacebookWorkplaceData(), id);
            encryptAmazonBusinessList = mainActivity.encryptData(getAmazonBusinessAccountData(), id);
            encryptAwsList = mainActivity.encryptData(getAwsData(), id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        Legal legal = new Legal(encryptLegalList.get(0), encryptLegalList.get(1), encryptLegalList.get(2), encryptLegalList.get(3), encryptLegalList.get(4), encryptLegalList.get(5), encryptLegalList.get(6));

        CurrentAccounts currentAccounts = new CurrentAccounts(encryptCurrentAccountList.get(0), encryptCurrentAccountList.get(1), encryptCurrentAccountList.get(2), encryptCurrentAccountList.get(3), encryptCurrentAccountList.get(4), encryptCurrentAccountList.get(5), encryptCurrentAccountList.get(6), encryptCurrentAccountList.get(7), encryptCurrentAccountList.get(8), encryptCurrentAccountList.get(9));

        Gst gst = new Gst(encryptGstList.get(0), encryptGstList.get(1), encryptGstList.get(2));

        EmailAccount emailAccount = new EmailAccount(encryptEmailList.get(0), encryptEmailList.get(1));

        DomainAccount domainAccount = new DomainAccount(encryptDomainList.get(0), encryptDomainList.get(1));

        RailwayAccount railwayAccount = new RailwayAccount(encryptRailList.get(0), encryptRailList.get(1), encryptRailList.get(2), encryptRailList.get(3), encryptRailList.get(4), encryptRailList.get(5));

        WorkplaceByFacebook workplace = new WorkplaceByFacebook(encryptWorkplaceList.get(0), encryptWorkplaceList.get(1));

        AmazonBusinessAccount amazonBusinessAccount = new AmazonBusinessAccount(encryptAmazonBusinessList.get(0), encryptAmazonBusinessList.get(1));

        Aws aws = new Aws(encryptAwsList.get(0), encryptAwsList.get(1), encryptAwsList.get(2), encryptAwsList.get(3));

        CompanyProfile companyProfile = new CompanyProfile(id, legal, currentAccounts, gst, emailAccount, domainAccount, railwayAccount, workplace, amazonBusinessAccount, aws);
        return companyProfile;
    }

    private ArrayList<String> getLegalData() {
        ArrayList<String> list = new ArrayList<>();
        String venture, numDirectors, cin, incorporationDate, headOfficeAddress, pan, tan;
        venture = mVenture.getText().toString().trim();
        numDirectors = mNumDirectors.getText().toString().trim();
        cin = mCin.getText().toString().trim();
        incorporationDate = mIncorporationDate.getText().toString().trim();
        headOfficeAddress = mheadOfficeAddress.getText().toString();
        pan = mPan.getText().toString().trim();
        tan = mTan.getText().toString().trim();
        list.add(venture);
        list.add(numDirectors);
        list.add(cin);
        list.add(incorporationDate);
        list.add(headOfficeAddress);
        list.add(pan);
        list.add(tan);
        return list;
    }

    private ArrayList<String> getCurrentAccountData() {
        ArrayList<String> list = new ArrayList<>();
        String bankName, accountNumber, ifsc, companyId, eStatementPass1, eStatementPass2, netBankingId, netBankingPass, mpin, vpa;
        bankName = mBankName.getText().toString().trim();
        accountNumber = mAccountNumber.getText().toString().trim();
        ifsc = mIfsc.getText().toString().trim();
        companyId = mCompanyId.getText().toString().trim();
        eStatementPass1 = mEstatementPass1.getText().toString().trim();
        eStatementPass2 = mEstatementPass2.getText().toString();
        netBankingId = mNetBankingId.getText().toString().trim();
        netBankingPass = mNetBankingPassword.getText().toString().trim();
        mpin = mPin.getText().toString().trim();
        vpa = mVpa.getText().toString().trim();
        list.add(bankName);
        list.add(accountNumber);
        list.add(ifsc);
        list.add(companyId);
        list.add(eStatementPass1);
        list.add(eStatementPass2);
        list.add(netBankingId);
        list.add(netBankingPass);
        list.add(mpin);
        list.add(vpa);
        return list;
    }

    private ArrayList<String> getGstData() {
        ArrayList<String> list = new ArrayList<>();
        String gstNumber, gstPortalId, gstPortalPass;
        gstNumber = mGstNumber.getText().toString().trim();
        gstPortalId = mGstPortalId.getText().toString().trim();
        gstPortalPass = mGstPortalPassword.getText().toString().trim();
        list.add(gstNumber);
        list.add(gstPortalId);
        list.add(gstPortalPass);
        return list;
    }

    private ArrayList<String> getEmailAccountData() {
        ArrayList<String> list = new ArrayList<>();
        String email, password;
        email = mEmail.getText().toString().trim();
        password = mEmailPassword.getText().toString().trim();
        list.add(email);
        list.add(password);
        return list;
    }

    private ArrayList<String> getDomainAccountData() {
        ArrayList<String> list = new ArrayList<>();
        String id, password;
        id = mDomainId.getText().toString().trim();
        password = mDomainPassword.getText().toString().trim();
        list.add(id);
        list.add(password);
        return list;
    }

    private ArrayList<String> getRailwayAccountData() {
        ArrayList<String> list = new ArrayList<>();
        String id, password, name, dob, email, phone;
        id = mVenture.getText().toString().trim();
        password = mNumDirectors.getText().toString().trim();
        name = mCin.getText().toString().trim();
        dob = mIncorporationDate.getText().toString().trim();
        email = mheadOfficeAddress.getText().toString();
        phone = mPan.getText().toString().trim();
        list.add(id);
        list.add(password);
        list.add(name);
        list.add(dob);
        list.add(email);
        list.add(phone);
        return list;
    }

    private ArrayList<String> getFacebookWorkplaceData() {
        ArrayList<String> list = new ArrayList<>();
        String id, password;
        id = mWorkplaceId.getText().toString().trim();
        password = mWorkplacePassword.getText().toString().trim();
        list.add(id);
        list.add(password);
        return list;
    }

    private ArrayList<String> getAmazonBusinessAccountData() {
        ArrayList<String> list = new ArrayList<>();
        String id, password;
        id = mAmazonBusinessId.getText().toString().trim();
        password = mAmazonBusinessPassword.getText().toString().trim();
        list.add(id);
        list.add(password);
        return list;
    }

    private ArrayList<String> getAwsData() {
        ArrayList<String> list = new ArrayList<>();
        String id, password, serverId, serverPass;
        id = mAwsId.getText().toString().trim();
        password = mAwsPassword.getText().toString().trim();
        serverId = mAwsServerId.getText().toString().trim();
        serverPass = mAwsServerPassword.getText().toString().trim();
        list.add(id);
        list.add(password);
        list.add(serverId);
        list.add(serverPass);
        return list;
    }

    private void toggleViews(boolean value) {
        mVenture.setEnabled(value);
        mNumDirectors.setEnabled(value);
        mCin.setEnabled(value);
        mIncorporationDate.setEnabled(value);
        mIncorporationDatePicker.setEnabled(value);
        mheadOfficeAddress.setEnabled(value);
        mPan.setEnabled(value);
        mTan.setEnabled(value);
        mBankName.setEnabled(value);
        mAccountNumber.setEnabled(value);
        mIfsc.setEnabled(value);
        mCompanyId.setEnabled(value);
        mEstatementPass1.setEnabled(value);
        mEstatementPass2.setEnabled(value);
        mNetBankingId.setEnabled(value);
        mNetBankingPassword.setEnabled(value);
        mPin.setEnabled(value);
        mVpa.setEnabled(value);
        mGstNumber.setEnabled(value);
        mGstPortalId.setEnabled(value);
        mGstPortalPassword.setEnabled(value);
        mEmail.setEnabled(value);
        mEmailPassword.setEnabled(value);
        mDomainId.setEnabled(value);
        mDomainPassword.setEnabled(value);
        mRailId.setEnabled(value);
        mRailPassword.setEnabled(value);
        mRailAccountName.setEnabled(value);
        mDob.setEnabled(value);
        mDobPicker.setEnabled(value);
        mRailPhone.setEnabled(value);
        mRailEmail.setEnabled(value);
        mWorkplaceId.setEnabled(value);
        mWorkplacePassword.setEnabled(value);
        mAmazonBusinessId.setEnabled(value);
        mAmazonBusinessPassword.setEnabled(value);
        mAwsId.setEnabled(value);
        mAwsPassword.setEnabled(value);
        mAwsServerId.setEnabled(value);
        mAwsServerPassword.setEnabled(value);
        mVenture.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mNumDirectors.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mCin.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mIncorporationDate.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mheadOfficeAddress.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mPan.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mTan.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mBankName.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mAccountNumber.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mIfsc.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mCompanyId.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mEstatementPass1.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mEstatementPass2.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mNetBankingId.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mNetBankingPassword.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mPin.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mVpa.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mGstNumber.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mGstPortalId.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mGstPortalPassword.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mEmail.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mEmailPassword.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mDomainId.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mDomainPassword.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mRailId.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mRailPassword.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mRailAccountName.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mDob.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mRailPhone.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mRailEmail.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mWorkplaceId.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mWorkplacePassword.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mAmazonBusinessId.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mAmazonBusinessPassword.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mAwsId.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mAwsPassword.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mAwsServerId.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mAwsServerPassword.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String date = Utils.formatDate(dayOfMonth, month, year);
        if (key == 0) {
            mIncorporationDate.setText(date);
        } else if (key == 1) {
            try {
                if (mainActivity.compareDate(date, null, true)) {
                    mDob.setText(date);
                } else {
                    mDob.setText("");
                    mainActivity.showAlertBox("Warning!", "Date of Birth Cannot be after today's date", 0);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }
}
