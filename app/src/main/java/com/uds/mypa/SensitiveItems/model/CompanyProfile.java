package com.uds.mypa.SensitiveItems.model;

import android.os.Parcel;
import android.os.Parcelable;

public class CompanyProfile implements Parcelable {
    private String id;
    private Legal legal;
    private CurrentAccounts currentAccounts;
    private Gst gst;
    private EmailAccount emailAccount;
    private DomainAccount domainAccount;
    private RailwayAccount railwayAccount;
    private WorkplaceByFacebook workplaceByFacebook;
    private AmazonBusinessAccount amazonBusinessAccount;
    private Aws aws;

    public CompanyProfile() {
    }

    public CompanyProfile(String id, Legal legal, CurrentAccounts currentAccounts, Gst gst, EmailAccount emailAccount, DomainAccount domainAccount, RailwayAccount railwayAccount, WorkplaceByFacebook workplaceByFacebook, AmazonBusinessAccount amazonBusinessAccount, Aws aws) {
        this.id = id;
        this.legal = legal;
        this.currentAccounts = currentAccounts;
        this.gst = gst;
        this.emailAccount = emailAccount;
        this.domainAccount = domainAccount;
        this.railwayAccount = railwayAccount;
        this.workplaceByFacebook = workplaceByFacebook;
        this.amazonBusinessAccount = amazonBusinessAccount;
        this.aws = aws;
    }

    protected CompanyProfile(Parcel in) {
        id = in.readString();
    }

    public static final Creator<CompanyProfile> CREATOR = new Creator<CompanyProfile>() {
        @Override
        public CompanyProfile createFromParcel(Parcel in) {
            return new CompanyProfile(in);
        }

        @Override
        public CompanyProfile[] newArray(int size) {
            return new CompanyProfile[size];
        }
    };

    public String getId() {
        return id;
    }

    public Legal getLegal() {
        return legal;
    }

    public CurrentAccounts getCurrentAccounts() {
        return currentAccounts;
    }

    public Gst getGst() {
        return gst;
    }

    public EmailAccount getEmailAccount() {
        return emailAccount;
    }

    public DomainAccount getDomainAccount() {
        return domainAccount;
    }

    public RailwayAccount getRailwayAccount() {
        return railwayAccount;
    }

    public WorkplaceByFacebook getWorkplaceByFacebook() {
        return workplaceByFacebook;
    }

    public AmazonBusinessAccount getAmazonBusinessAccount() {
        return amazonBusinessAccount;
    }

    public Aws getAws() {
        return aws;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
    }
}
