package com.uds.mypa.SensitiveItems.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.uds.mypa.Activitys.MainActivity;
import com.uds.mypa.Interfaces.OnItemClickListener;
import com.uds.mypa.R;
import com.uds.mypa.SensitiveItems.adapters.InsuranceAdapter;
import com.uds.mypa.SensitiveItems.model.Insurance;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class InsuranceFragment extends Fragment implements OnItemClickListener{
    private ArrayList<Insurance> arrayList;
    private MainActivity mainActivity;
    private FirebaseAnalytics mFirebaseAnalytics;
    private FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    private DatabaseReference reference = FirebaseDatabase.getInstance().getReference(user.getUid()).child("Insurance");
    private ArrayList<Insurance> list;
    private RecyclerView recyclerView;
    private ShimmerFrameLayout shimmerFrameLayout;
    private OnItemClickListener listener;
    private InsuranceAdapter adapter;
    private LottieAnimationView lottieAnimationView;

    public InsuranceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_insurance, container, false);
        mainActivity = (MainActivity) getActivity();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mainActivity);
        arrayList = new ArrayList<>();
        shimmerFrameLayout = view.findViewById(R.id.shimmer);
        listener = this;
        list = new ArrayList<>();
        lottieAnimationView=view.findViewById(R.id.animation_view);
        recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(mainActivity));
        FloatingActionButton fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.setUpFragment(25, null);
            }
        });
        recyclerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainActivity.toggleBottomSheet();
            }
        });
        shimmerFrameLayout.startShimmerAnimation();
        Toolbar mToolbar = view.findViewById(R.id.toolbar);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.goBack();
            }
        });
        return view;
    }
    private void showData() {
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                list.clear();
                for (DataSnapshot passSnapshot : dataSnapshot.getChildren()) {
                    Insurance insurance = passSnapshot.getValue(Insurance.class);
                    list.add(insurance);
                }
                try {
                    arrayList = getDataList(list);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                adapter = null;
                try {
                    adapter = new InsuranceAdapter(arrayList, mainActivity, listener);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                shimmerFrameLayout.stopShimmerAnimation();
                shimmerFrameLayout.setVisibility(View.GONE);
                recyclerView.setAdapter(adapter);
                if (list.isEmpty()) {
                    lottieAnimationView.playAnimation();
                    lottieAnimationView.setVisibility(View.VISIBLE);
                }
                else {
                    lottieAnimationView.pauseAnimation();
                    lottieAnimationView.setVisibility(View.GONE);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(mainActivity, "Something went wrong, Please try again later.", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private ArrayList<Insurance> getDataList(ArrayList<Insurance> arrayList) throws Exception{
        ArrayList<Insurance> list = new ArrayList<>();
        for (int i = 0; i < arrayList.size(); i++) {
            Insurance cuur = arrayList.get(i);
            String id, name, company, due, duration, policyName, plan, number, premium, frequency;
            id = cuur.getId();
            name = cuur.getName();
            company = cuur.getCompanyName();
            due = cuur.getDueDate();
            duration = cuur.getDuration();
            policyName = cuur.getPolicyName();
            plan = cuur.getPlan();
            premium = cuur.getPremium();
            frequency = cuur.getFrequency();
            number = cuur.getNumber();
            list.add(new Insurance(id,mainActivity.decryptData(name,id),mainActivity.decryptData(company,id),mainActivity.decryptData(plan,id),mainActivity.decryptData(policyName,id),mainActivity.decryptData(number,id),mainActivity.decryptData(duration,id),mainActivity.decryptData(premium,id),mainActivity.decryptData(frequency,id),mainActivity.decryptData(due,id)));
        }
        return list;
    }



    @Override
    public void onStart() {
        super.onStart();
        showData();
        mainActivity.collapseBottomSheet();
    }

    @Override
    public void onItemClick(int position) {
        mainActivity.openBottomSheet(arrayList.get(position).getId(), "Insurance", arrayList.get(position));

    }
}
