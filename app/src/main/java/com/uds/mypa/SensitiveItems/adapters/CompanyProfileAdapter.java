package com.uds.mypa.SensitiveItems.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.uds.mypa.Interfaces.OnItemClickListener;
import com.uds.mypa.R;
import com.uds.mypa.SensitiveItems.model.CompanyProfile;
import com.uds.mypa.SensitiveItems.model.DrivingLicense;
import com.uds.mypa.SensitiveItems.model.Legal;

import java.util.ArrayList;

public class CompanyProfileAdapter extends RecyclerView.Adapter<CompanyProfileAdapter.ViewHolder> {

    private ArrayList<CompanyProfile> arrayList;
    private Context context;
    private OnItemClickListener onItemClickListener;

    public CompanyProfileAdapter(ArrayList<CompanyProfile> arrayList, Context context, OnItemClickListener onItemClickListener) {
        this.arrayList = arrayList;
        this.context = context;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public CompanyProfileAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.passport_layout, parent, false);
        return new ViewHolder(view, onItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull CompanyProfileAdapter.ViewHolder holder, int position) {
        CompanyProfile companyProfile = arrayList.get(position);
        Legal legal = companyProfile.getLegal();
        holder.name.setText(legal.getVentureName());
        holder.number.setText(legal.getNumberOfDirectors());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name, number;
        OnItemClickListener onItemClickListener;

        public ViewHolder(@NonNull View itemView, OnItemClickListener onItemClickListener) {
            super(itemView);
            name = itemView.findViewById(R.id.name_editText);
            number = itemView.findViewById(R.id.number);
            this.onItemClickListener = onItemClickListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onItemClickListener.onItemClick(getAdapterPosition());
        }
    }
}
