package com.uds.mypa.SensitiveItems.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.uds.mypa.Activitys.MainActivity;
import com.uds.mypa.R;
import com.uds.mypa.SensitiveItems.model.Electricity;

import java.util.ArrayList;

public class NewElectricityFragment extends Fragment {
    private TextInputLayout mNameLayout, mNumberLayout,mPhoneLayout,mEmailLayout;
    private TextInputEditText mAddress, mName, mNumber, mPassword, mUserName,mPhone,mEmail;
    private TextView mTitle;
    private MainActivity mainActivity;
    private FirebaseAnalytics mFirebaseAnalytics;
    private String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
    private DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(uid);
    private Boolean save = true;
    public NewElectricityFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
        mainActivity.collapseBottomSheet();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_electricity, container, false);
        myRef.keepSynced(true);
        initialize(view);
        mTitle.setText("Add Electricity Account");
        final FloatingActionButton saveFab, editFab;
        saveFab = view.findViewById(R.id.save_fab);
        editFab = view.findViewById(R.id.edit_fab);
        editFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleViews(true);
                editFab.setVisibility(View.GONE);
            }
        });
        Bundle bundle = getArguments();
        if (bundle != null) {
            Electricity electricity = bundle.getParcelable("electricity");
            mName.setText(electricity.getName());
            mAddress.setText(electricity.getAddress());
            mNumber.setText(electricity.getNumber());
            mUserName.setText(electricity.getUserName());
            mPhone.setText(electricity.getPhone());
            mEmail.setText(electricity.getEmail());
            mPassword.setText(electricity.getPassword());
            uid = electricity.getId();
            save = false;
            if (bundle.getInt("choose") == 0) {
                toggleViews(false);
                editFab.setVisibility(View.VISIBLE);
            } else {
                toggleViews(true);
                editFab.setVisibility(View.GONE);
            }
        }

        saveFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (save)
                    saveData();
                else
                    updateData();
            }
        });
        return view;
    }

    private void toggleViews(boolean value) {
        mNumber.setEnabled(value);
        mName.setEnabled(value);
        mPassword.setEnabled(value);
        mEmail.setEnabled(value);
        mUserName.setEnabled(value);
        mPhone.setEnabled(value);
        mAddress.setEnabled(value);
        mName.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        mNumber.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        mPassword.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        mEmail.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        mUserName.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        mPhone.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        mAddress.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
       if(value)
           mNumber.requestFocus();
    }

    private void initialize(View view) {
        mainActivity = (MainActivity) getActivity();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mainActivity);
        mTitle = view.findViewById(R.id.title_TextView);
        mName = view.findViewById(R.id.name_editText);
        mNumber = view.findViewById(R.id.electricity_number_editText);
        mAddress = view.findViewById(R.id.address_editText);
        mEmail = view.findViewById(R.id.email_editText);
        mPassword = view.findViewById(R.id.password_editText);
        mPhone = view.findViewById(R.id.phone_editText);
        mUserName = view.findViewById(R.id.user_name_editText);
        mNameLayout = view.findViewById(R.id.name_layout);
        mNumberLayout = view.findViewById(R.id.number_layout);
        mPhoneLayout = view.findViewById(R.id.phone_layout);
        mEmailLayout = view.findViewById(R.id.email_layout);
    }
    private void saveData() {
        if (checkValidation()){
        String id = myRef.push().getKey();
        ArrayList<String> list = new ArrayList<>();
        String name = mName.getText().toString();
        String number = mNumber.getText().toString().trim();
        String userName = mUserName.getText().toString().trim();
        String address = mAddress.getText().toString().trim();
        String email = mEmail.getText().toString().trim();
        String phone = mPhone.getText().toString().trim();
        String password = mPassword.getText().toString().trim();
        list.add(name);
        list.add(number);
        list.add(address);
        list.add(email);
        list.add(userName);
        list.add(password);
        list.add(phone);
        ArrayList<String> encryptData = null;
        try {
            encryptData = mainActivity.encryptData(list, id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        encryptData.add(0, id);
        Electricity electricityModel = new Electricity(id, encryptData.get(1), encryptData.get(2), encryptData.get(3), encryptData.get(4), encryptData.get(5), encryptData.get(6), encryptData.get(7));
        myRef.child("Electricity").child(id).setValue(electricityModel);
        mainActivity.showAlertBox(getString(R.string.txt_Success), getString(R.string.txt_data_saved_success), 2);
        mainActivity.goBack();
    }
    }

    private void updateData() {
        if (checkValidation()) {
            ArrayList<String> list = new ArrayList<>();
            String name = mName.getText().toString();
            String number = mNumber.getText().toString().trim();
            String userName = mUserName.getText().toString().trim();
            String address = mAddress.getText().toString().trim();
            String email = mEmail.getText().toString().trim();
            String phone = mPhone.getText().toString().trim();
            String password = mPassword.getText().toString().trim();
            list.add(name);
            list.add(number);
            list.add(address);
            list.add(email);
            list.add(userName);
            list.add(password);
            list.add(phone);
            ArrayList<String> encryptData = null;
            try {
                encryptData = mainActivity.encryptData(list, uid);
            } catch (Exception e) {
                e.printStackTrace();
            }
            encryptData.add(0, uid);
            Electricity electricityModel = new Electricity(uid, encryptData.get(1), encryptData.get(2), encryptData.get(3), encryptData.get(4), encryptData.get(5), encryptData.get(6), encryptData.get(7));
            myRef.child("Electricity").child(uid).setValue(electricityModel);
            mainActivity.showAlertBox(getString(R.string.txt_Success), getString(R.string.txt_data_updated_success), 2);
            mainActivity.goBack();
        }
    }

    private boolean checkValidation() {
        boolean check = true;
        if (TextUtils.isEmpty(mName.getText().toString())) {
            mNameLayout.setError("Name cannot be blank");
            check = false;
        }
        if (TextUtils.isEmpty(mNumber.getText().toString())) {
            mNumberLayout.setError("Electricity Connection Number cannot be blank");
            check = false;
        }
        if (!TextUtils.isEmpty(mPhone.getText().toString()))
            if (!mainActivity.isValidPhone(mPhone.getText().toString())) {
                mPhoneLayout.setError("Invalid Phone Number");
                check = false;
            }
        if (!TextUtils.isEmpty(mEmail.getText().toString()))
            if (!mainActivity.isValidEmail(mEmail.getText().toString())) {
                mEmailLayout.setError("Invalid Email Address");
                check = false;
            }
        return check;
    }

}
