package com.uds.mypa.SensitiveItems.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.uds.mypa.Activitys.MainActivity;
import com.uds.mypa.R;
import com.uds.mypa.SensitiveItems.model.RemoteLogin;

import java.util.ArrayList;

public class NewRemoteLoginFragment extends Fragment {
    private TextInputEditText mAppName, mPassword, mLoginId;
    private TextInputLayout appNameLayout, mPasswordLayout, mLoginIdLayout;
    private TextView mTitle;
    private MainActivity mainActivity;
    private FirebaseAnalytics mFirebaseAnalytics;
    private String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
    private DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(uid);
    private Boolean save = true;

    public NewRemoteLoginFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
        mainActivity.collapseBottomSheet();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_remote_login, container, false);
        myRef.keepSynced(true);
        initialize(view);
        mTitle.setText("Add Remote Login");
        final FloatingActionButton saveFab, editFab;
        saveFab = view.findViewById(R.id.save_fab);
        editFab = view.findViewById(R.id.edit_fab);
        editFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleViews(true);
                editFab.setVisibility(View.GONE);
            }
        });
        Bundle bundle = getArguments();
        if (bundle != null) {
            RemoteLogin remoteLogin = bundle.getParcelable("remote_login");
            mAppName.setText(remoteLogin.getAppName());
            mLoginId.setText(remoteLogin.getLoginId());
            mPassword.setText(remoteLogin.getPassword());
            uid = remoteLogin.getId();
            save = false;
            if (bundle.getInt("choose") == 0) {
                toggleViews(false);
                editFab.setVisibility(View.VISIBLE);
            } else {
                toggleViews(true);
                editFab.setVisibility(View.GONE);
            }
        }

        saveFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (save)
                    saveData();
                else
                    updateData();
            }
        });
        return view;
    }

    private void toggleViews(boolean b) {
        mPassword.setEnabled(b);
        mAppName.setEnabled(b);
        mLoginId.setEnabled(b);
        mAppName.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mPassword.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mLoginId.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        if (b)
            mAppName.requestFocus();
    }

    private void initialize(@NonNull View view) {
        mainActivity = (MainActivity) getActivity();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mainActivity);
        mTitle = view.findViewById(R.id.title_TextView);
        mAppName = view.findViewById(R.id.app_name_editText);
        mLoginId = view.findViewById(R.id.login_id_editText);
        mLoginIdLayout = view.findViewById(R.id.login_id_layout);
        mPasswordLayout = view.findViewById(R.id.password_layout);
        mPassword = view.findViewById(R.id.password_editText);
        appNameLayout = view.findViewById(R.id.app_name_layout);
    }

    private void saveData() {
        if (checkValidation()) {
            String id = myRef.push().getKey();
            ArrayList<String> list = new ArrayList<>();
            String appName = mAppName.getText().toString().trim();
            String loginId = mLoginId.getText().toString().trim();
            String password = mPassword.getText().toString().trim();
            list.add(appName);
            list.add(loginId);
            list.add(password);
            ArrayList<String> encryptData = null;
            try {
                encryptData = mainActivity.encryptData(list, id);
            } catch (Exception e) {
                e.printStackTrace();
            }
            encryptData.add(0, id);
            RemoteLogin remoteModel = new RemoteLogin(id, encryptData.get(1), encryptData.get(2), encryptData.get(3));
            assert id != null;
            myRef.child("Remote Login").child(id).setValue(remoteModel);
            mainActivity.showAlertBox(getString(R.string.txt_Success),getString(R.string.txt_data_saved_success),2);
            mainActivity.goBack();
        }
    }

    private void updateData() {
        if (checkValidation()) {
            ArrayList<String> list = new ArrayList<>();
            String appName = mAppName.getText().toString().trim();
            String loginId = mLoginId.getText().toString().trim();
            String password = mPassword.getText().toString().trim();
            list.add(appName);
            list.add(loginId);
            list.add(password);
            ArrayList<String> encryptData = null;
            try {
                encryptData = mainActivity.encryptData(list, uid);
            } catch (Exception e) {
                e.printStackTrace();
            }
            encryptData.add(0, uid);
            RemoteLogin remoteModel = new RemoteLogin(uid, encryptData.get(1), encryptData.get(2), encryptData.get(3));
            myRef.child("Remote Login").child(uid).setValue(remoteModel);
            mainActivity.showAlertBox(getString(R.string.txt_Success),getString(R.string.txt_data_updated_success),2);
            mainActivity.goBack();
        }
    }

    private boolean checkValidation() {
        boolean check = true;
        if (TextUtils.isEmpty(mAppName.getText().toString())) {
            appNameLayout.setError("Cannot be Empty");
            check = false;
        }
        if (TextUtils.isEmpty(mPassword.getText().toString())) {
            mPasswordLayout.setError("Cannot be Empty");
            check = false;
        }
        if (TextUtils.isEmpty(mLoginId.getText().toString())) {
            mLoginIdLayout.setError("Cannot be Empty");
            check = false;
        }

        return check;
    }

}
