package com.uds.mypa.SensitiveItems.fragment;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.radiobutton.MaterialRadioButton;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.uds.mypa.Activitys.MainActivity;
import com.uds.mypa.R;
import com.uds.mypa.SensitiveItems.activity.BankSelectionActivity;
import com.uds.mypa.SensitiveItems.adapters.CardSpinnerAdapter;
import com.uds.mypa.SensitiveItems.model.BankingCard;
import com.uds.mypa.utils.MonthYearPickerDialog;
import com.uds.mypa.utils.Utils;

import java.text.ParseException;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewBankingCardFragment extends Fragment implements DatePickerDialog.OnDateSetListener {
    private TextInputLayout mNameLayout, mNumberLayout, mPhoneLayout, mEmailLayout;
    private TextView mBankName, mtitle;
    private MaterialRadioButton mCreditRadio, mDebitRadio;
    private EditText mCardNumber, mName, mIssueDate, mExpiryDate, mCvv, mPin, mAtmPin, mPhone, mEmail;
    private ImageView mIssueDatePicker, mExpiryDatePicker, mBankLogo;
    private String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
    private DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(uid);
    private RadioGroup mCardRadioGroup;
    private MainActivity mainActivity;
    private FirebaseAnalytics mFirebaseAnalytics;
    private String cardType;
    private Boolean save = true;
    private LinearLayout selectBank;
    private int bankLogoId, cardLogoId;
    private int mYear, mMonth, mDay;
    private int key, cardId = -1;
    private Spinner mCardSpinner;
    private String date, cardNameType = "Visa", bankName = "";
    private String[] typeName;
    private int[] cardTypeImages;


    public NewBankingCardFragment() {
        // Required empty public constructor
    }
    @Override
    public void onStart() {
        super.onStart();
        mainActivity.collapseBottomSheet();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_banking_card, container, false);
        initialize(view);
        mtitle.setText("Add Banking Card");
        mCardRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = group.findViewById(checkedId);
                cardType = radioButton.getText().toString();
            }
        });
        final FloatingActionButton saveFab, editFab;
        saveFab = view.findViewById(R.id.save_fab);
        editFab = view.findViewById(R.id.edit_fab);
        editFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleViews(true);
                editFab.setVisibility(View.GONE);
            }
        });
        saveFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (save)
                    saveData();
                else
                    updateData();
            }
        });
        Bundle bundle = getArguments();
        if (bundle != null) {
            BankingCard bankingCard = bundle.getParcelable("bankingCard");
            mName.setText(bankingCard.getName());
            mCardNumber.setText(bankingCard.getNumber());
            mCvv.setText(bankingCard.getCvv());
            mIssueDate.setText(bankingCard.getValidFrom());
            mExpiryDate.setText(bankingCard.getValidTo());
            mPin.setText(bankingCard.getPin());
            mAtmPin.setText(bankingCard.getMpin());
            mPhone.setText(bankingCard.getPhone());
            mEmail.setText(bankingCard.getEmail());
            mBankName.setText(bankingCard.getBankName());
            mBankLogo.setImageResource(Integer.parseInt(bankingCard.getBankLogo()));
            uid = bankingCard.getId();
            cardId = bankingCard.getCardIndex();
            cardType = bankingCard.getCardType();
            if (cardType.equals("Debit"))
                mDebitRadio.setChecked(true);
            else mCreditRadio.setChecked(true);
            save = false;

            if (bundle.getInt("choose") == 0) {
                toggleViews(false);
                editFab.setVisibility(View.VISIBLE);
            } else {
                toggleViews(true);
                editFab.setVisibility(View.GONE);
            }
        }

        selectBank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getContext(), BankSelectionActivity.class), 100);
            }
        });
        mCardNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                int len = charSequence.toString().length();
                if (len != 20) {
                    if (i1 == 0 && (len == 4 || len == 9 || len == 14 || len == 19))
                        mCardNumber.append(" ");
                }
            }


            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        mIssueDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MonthYearPickerDialog pickerDialog = new MonthYearPickerDialog();
                pickerDialog.setListener(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int i2) {
                        date =  Utils.formatBankDate(month,year);
                        try {
                            if (mainActivity.compareBankDate(date, null, true)) {
                                mIssueDate.setText(date);
                            } else {
                                mIssueDate.setText("");
                                mainActivity.showAlertBox("Warning!", "Issue Date Cannot be greater than today date", 0);
                            }
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }
                    }
                });
                pickerDialog.show(getChildFragmentManager(), "MonthYearPickerDialog");
            }
        });
        mExpiryDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MonthYearPickerDialog pickerDialog = new MonthYearPickerDialog();
                pickerDialog.setListener(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int i2) {
                        date =  Utils.formatBankDate(month,year);
                        {
                            try {
                                if (mainActivity.compareBankDate(Utils.formatBankDate( month, year), date, false)) {
                                    mExpiryDate.setText(Utils.formatBankDate( month, year));
                                } else {
                                    mExpiryDate.setText("");
                                    mainActivity.showAlertBox("Warning!", "Expiry Date Cannot be before Issue date", 0);
                                }
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                });
                pickerDialog.show(getChildFragmentManager(), "MonthYearPickerDialog");
            }
        });
        CardSpinnerAdapter cardSpinnerAdapter = new CardSpinnerAdapter(getContext(), typeName, cardTypeImages);
        mCardSpinner.setAdapter(cardSpinnerAdapter);
        mCardSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
                    case 0:
                        cardNameType = "Visa";
                        break;
                    case 1:
                        cardNameType = "Maestro";
                        break;
                    case 2:
                        cardNameType = "Master Card";
                        break;
                    case 3:
                        cardNameType = "Rupay";
                        break;
                }
                cardLogoId = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                cardNameType = "Visa";
                cardLogoId = cardTypeImages[0];
            }
        });
        return view;
    }

    private void initialize(View view) {
        mainActivity = (MainActivity) getActivity();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mainActivity);
        mtitle = view.findViewById(R.id.title_TextView);
        mBankName = view.findViewById(R.id.bank_name_textView);
        mCardNumber = view.findViewById(R.id.card_number_editText);
        mName = view.findViewById(R.id.name_editText);
        mIssueDate = view.findViewById(R.id.issueDate);
        mExpiryDate = view.findViewById(R.id.expiryDate_editText);
        mCvv = view.findViewById(R.id.cvv_editText);
        mPin = view.findViewById(R.id.mpin_editText);
        mAtmPin = view.findViewById(R.id.atm_editText);
        mPhone = view.findViewById(R.id.phone_editText);
        mEmail = view.findViewById(R.id.email_editText);
        mNameLayout = view.findViewById(R.id.name_layout);
        mPhoneLayout = view.findViewById(R.id.phone_layout);
        mEmailLayout = view.findViewById(R.id.email_layout);
        mNumberLayout = view.findViewById(R.id.number_layout);
        mIssueDatePicker = view.findViewById(R.id.issue_date_picker_actions);
        mExpiryDatePicker = view.findViewById(R.id.expiry_date_picker_actions);
        mCardRadioGroup = view.findViewById(R.id.card_category_radiogroup);
        mBankLogo = view.findViewById(R.id.bankLogo);
        mCreditRadio = view.findViewById(R.id.creditRadioButton);
        mDebitRadio = view.findViewById(R.id.debitRadioButton);
        selectBank = view.findViewById(R.id.selectBank);
        mCardSpinner = view.findViewById(R.id.cardTypeSpinner);
        typeName = new String[]{"Visa", "Maestro", "Master Card", "Rupay"};
        cardTypeImages = new int[]{R.drawable.visa2, R.drawable.masestro, R.drawable.master_card, R.drawable.rupay};
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == 100) {
            if (data != null) {
                bankLogoId = data.getIntExtra("image_id", 0);
                bankName = data.getStringExtra("bank_name");
                mBankName.setText(bankName);
                mBankLogo.setImageResource(bankLogoId);
                mBankName.setTextColor(ContextCompat.getColor(getContext(), android.R.color.tab_indicator_text));
            }
        }
    }

    private void saveData() {
        if (checkValidation()) {
            String id = myRef.push().getKey();
            ArrayList<String> list = new ArrayList<>();
            String name = mName.getText().toString().trim();
            String number = mCardNumber.getText().toString().trim();
            String from = mIssueDate.getText().toString().trim();
            String To = mExpiryDate.getText().toString().trim();
            String cvv = mCvv.getText().toString().trim();
            String email = mEmail.getText().toString().trim();
            String mpin = mPin.getText().toString().trim();
            String pin = mPin.getText().toString().trim();
            String phone = mPhone.getText().toString().trim();
            list.add(name);
            list.add(mBankName.getText().toString());
            list.add(cardType);
            list.add(number);
            list.add(from);
            list.add(To);
            list.add(cvv);
            list.add(mpin);
            list.add(pin);
            list.add(phone);
            list.add(email);
            list.add(cardNameType);
            list.add(String.valueOf(bankLogoId));
            cardId = getRandomNumber();
            ArrayList<String> encryptData = null;
            try {
                encryptData = mainActivity.encryptData(list, id);
            } catch (Exception e) {
                e.printStackTrace();
            }
            encryptData.add(0, id);
            BankingCard bankModel = new BankingCard(id, encryptData.get(1), encryptData.get(2), encryptData.get(3), encryptData.get(4),
                    encryptData.get(5), encryptData.get(6), encryptData.get(7), encryptData.get(8), encryptData.get(9), encryptData.get(10),
                    encryptData.get(11), encryptData.get(12), encryptData.get(13), String.valueOf(cardLogoId), cardId);
            myRef.child("Banking Card").child(id).setValue(bankModel);
            mainActivity.showAlertBox(getString(R.string.txt_Success), getString(R.string.txt_data_saved_success), 2);
            mainActivity.goBack();
        }
    }

    private void updateData() {
        if (checkValidation()) {
            ArrayList<String> list = new ArrayList<>();
            String name = mName.getText().toString().trim();
            String number = mCardNumber.getText().toString().trim();
            String from = mIssueDate.getText().toString().trim();
            String To = mExpiryDate.getText().toString().trim();
            String cvv = mCvv.getText().toString().trim();
            String email = mEmail.getText().toString().trim();
            String mpin = mPin.getText().toString().trim();
            String pin = mPin.getText().toString().trim();
            String phone = mPhone.getText().toString().trim();
            String bankLogo = String.valueOf(bankLogoId);
            String cardLogo = String.valueOf(cardLogoId);
            list.add(name);
            list.add(mBankName.getText().toString());
            list.add(cardType);
            list.add(number);
            list.add(from);
            list.add(To);
            list.add(cvv);
            list.add(mpin);
            list.add(pin);
            list.add(phone);
            list.add(email);
            list.add(cardType);
            list.add(bankLogo);
            list.add(cardLogo);
            cardId = getRandomNumber();
            ArrayList<String> encryptData = null;
            try {
                encryptData = mainActivity.encryptData(list, uid);
            } catch (Exception e) {
                e.printStackTrace();
            }
            encryptData.add(0, uid);
            BankingCard bankingCard = new BankingCard(uid, encryptData.get(1), encryptData.get(2), encryptData.get(3), encryptData.get(4), encryptData.get(5), encryptData.get(6), encryptData.get(7), encryptData.get(8), encryptData.get(9), encryptData.get(10), encryptData.get(11), encryptData.get(12), encryptData.get(13), encryptData.get(14), cardId);
            myRef.child("Banking Card").child(uid).setValue(bankingCard);
            mainActivity.showAlertBox(getString(R.string.txt_Success), getString(R.string.txt_data_saved_success), 2);
            mainActivity.goBack();
        }
    }

    private void toggleViews(boolean value) {
        selectBank.setEnabled(value);
        mName.setEnabled(value);
        mExpiryDate.setEnabled(value);
        mCardNumber.setEnabled(value);
        mCardRadioGroup.setEnabled(value);
        mIssueDate.setEnabled(value);
        mCvv.setEnabled(value);
        mPin.setEnabled(value);
        mAtmPin.setEnabled(value);
        mPhone.setEnabled(value);
        mEmail.setEnabled(value);
        mExpiryDatePicker.setEnabled(value);
        mIssueDatePicker.setEnabled(value);
        mCreditRadio.setEnabled(value);
        mDebitRadio.setEnabled(value);
        mName.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mCardNumber.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mIssueDate.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mCvv.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mExpiryDate.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mCvv.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mPhone.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mPin.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mAtmPin.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mEmail.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        if (value)
            mCardNumber.requestFocus();
    }

    private boolean checkValidation() {
        boolean check = true;
        if (TextUtils.isEmpty(mName.getText().toString())) {
            mNameLayout.setError("Card holder name cannot be blank");
            check = false;
        }
        if (TextUtils.isEmpty(mCardNumber.getText().toString())) {
            mNumberLayout.setError("Card number cannot be empty");
            check = false;
        }
        if (TextUtils.isEmpty(bankName)) {
            mainActivity.showAlertBox("Warning", "Please Select Bank", 0);
            check = false;
        }
        if (mCardRadioGroup.getCheckedRadioButtonId() == -1) {
            mainActivity.showAlertBox("Warning", "Choose Card Category", 0);
            check = false;
        }
        if (!TextUtils.isEmpty(mPhone.getText().toString()))
            if (!mainActivity.isValidPhone(mPhone.getText().toString())) {
                mPhoneLayout.setError("Invalid Phone Number");
                check = false;
            }
        if (!TextUtils.isEmpty(mEmail.getText().toString()))
            if (!mainActivity.isValidEmail(mEmail.getText().toString())) {
                mEmailLayout.setError("Invalid Email Address");
                check = false;
            }
        return check;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        if (key == 0) {

            date = Utils.formatDate(dayOfMonth, month, year);
            try {
                if (mainActivity.compareDate(date, null, true)) {
                    mIssueDate.setText(date);
                } else {
                    mIssueDate.setText("");
                    mainActivity.showAlertBox("Warning!", "Valid From Date Cannot be greater than today date", 0);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
            try {
                if (mainActivity.compareDate(Utils.formatDate(dayOfMonth, month, year), date, false)) {
                    mExpiryDate.setText(Utils.formatDate(dayOfMonth, month, year));
                } else {
                    mExpiryDate.setText("");
                    mainActivity.showAlertBox("Warning!", "Expiry Date Cannot be before Issue date", 0);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
    }

    private int getRandomNumber() {

        int r = (int) (Math.random() * (9));
        return r;

    }
}