package com.uds.mypa.SensitiveItems.model;

import android.os.Parcel;
import android.os.Parcelable;

public class BankAccount implements Parcelable {
    private String id,name,number,ifscCode,nominee,relation,password,bankName,imageId,netbankingId,netbankingPassword;

    public BankAccount() {
    }

    public BankAccount(String id, String name, String number, String ifscCode, String nominee, String relation, String password, String bankName, String imageId, String netbankingId, String netbankingPassword) {
        this.id = id;
        this.name = name;
        this.number = number;
        this.ifscCode = ifscCode;
        this.nominee = nominee;
        this.relation = relation;
        this.password = password;
        this.bankName = bankName;
        this.imageId = imageId;
        this.netbankingId = netbankingId;
        this.netbankingPassword = netbankingPassword;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getNumber() {
        return number;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public String getNominee() {
        return nominee;
    }

    public String getRelation() {
        return relation;
    }

    public String getPassword() {
        return password;
    }

    public String getBankName() {
        return bankName;
    }

    public String getImageId() {
        return imageId;
    }

    public String getNetbankingId() {
        return netbankingId;
    }

    public String getNetbankingPassword() {
        return netbankingPassword;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}
