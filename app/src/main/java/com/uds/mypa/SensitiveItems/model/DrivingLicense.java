package com.uds.mypa.SensitiveItems.model;

import android.os.Parcel;
import android.os.Parcelable;

public class DrivingLicense implements Parcelable {
    private String id, name, dlNumber, issueDate, validUpto, dob, fathersName, address;

    public DrivingLicense(String id, String name, String dlNumber, String issueDate, String validUpto, String dob, String fathersName, String address) {
        this.id = id;
        this.name = name;
        this.dlNumber = dlNumber;
        this.issueDate = issueDate;
        this.validUpto = validUpto;
        this.dob = dob;
        this.fathersName = fathersName;
        this.address = address;
    }

    public DrivingLicense() {
    }

    protected DrivingLicense(Parcel in) {
        id = in.readString();
        name = in.readString();
        dlNumber = in.readString();
        issueDate = in.readString();
        validUpto = in.readString();
        dob = in.readString();
        fathersName = in.readString();
        address = in.readString();
    }

    public static final Creator<DrivingLicense> CREATOR = new Creator<DrivingLicense>() {
        @Override
        public DrivingLicense createFromParcel(Parcel in) {
            return new DrivingLicense(in);
        }

        @Override
        public DrivingLicense[] newArray(int size) {
            return new DrivingLicense[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDlNumber() {
        return dlNumber;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public String getValidUpto() {
        return validUpto;
    }

    public String getDob() {
        return dob;
    }

    public String getFathersName() {
        return fathersName;
    }

    public String getAddress() {
        return address;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(dlNumber);
        dest.writeString(issueDate);
        dest.writeString(validUpto);
        dest.writeString(dob);
        dest.writeString(fathersName);
        dest.writeString(address);
    }
}
