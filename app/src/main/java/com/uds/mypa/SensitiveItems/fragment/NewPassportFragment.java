package com.uds.mypa.SensitiveItems.fragment;


import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.uds.mypa.Activitys.MainActivity;
import com.uds.mypa.R;
import com.uds.mypa.SensitiveItems.model.Passport;
import com.uds.mypa.utils.Utils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewPassportFragment extends Fragment implements DatePickerDialog.OnDateSetListener {
    private TextInputLayout mNameLayout, mNumberLayout;
    private TextInputEditText mExpiryDate, mName, mNumber, mPlace, mIssueDate;
    private TextView mTitle;
    private MainActivity mainActivity;
    private FirebaseAnalytics mFirebaseAnalytics;
    private ImageView mIssueDatePicker, mExpiryDatePicker;
    private String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
    private DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(uid);
    private Boolean save = true;
    private int mYear, mMonth, mDay;
    private int key;
    private String date;


    public NewPassportFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
        mainActivity.collapseBottomSheet();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_passport, container, false);
        myRef.keepSynced(true);
        initialize(view);
        mTitle.setText("Add Passport Detail");
        mIssueDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(mainActivity,
                        NewPassportFragment.this, mYear, mMonth, mDay);
                datePickerDialog.show();
                key = 0;
            }
        });
        mExpiryDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(mainActivity,
                        NewPassportFragment.this, mYear, mMonth, mDay);
                datePickerDialog.show();
                key = 1;

            }
        });

        final FloatingActionButton saveFab, editFab;
        saveFab = view.findViewById(R.id.save_fab);
        editFab = view.findViewById(R.id.edit_fab);
        editFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleViews(true);
                editFab.setVisibility(View.GONE);
            }
        });
        Bundle bundle = getArguments();
        if (bundle != null) {
            Passport passport = bundle.getParcelable("passport");
            mName.setText(passport.getName());
            mPlace.setText(passport.getPlaceOfIssue());
            mNumber.setText(passport.getPassportNumber());
            mIssueDate.setText(passport.getDateOfIssue());
            mExpiryDate.setText(passport.getDateOfExpiry());
            uid = passport.getId();
            save = false;
            if (bundle.getInt("choose") == 0) {
                toggleViews(false);
                editFab.setVisibility(View.VISIBLE);
            } else {
                toggleViews(true);
                editFab.setVisibility(View.GONE);
            }
        }

        saveFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (save)
                    saveData();
                else
                    updateData();
            }
        });
        mIssueDate.addTextChangedListener(new TextWatcher() {
            int prevL = 0;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                int length = editable.length();
                if ((prevL < length) && (length == 2 || length == 5)) {
                    editable.append("/");
                }
            }
        });
        mExpiryDate.addTextChangedListener(new TextWatcher() {
            int prevL = 0;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                int length = editable.length();
                if ((prevL < length) && (length == 2 || length == 5)) {
                    editable.append("/");
                }
            }
        });

        return view;
    }

    private void toggleViews(boolean value) {
        mNumber.setEnabled(value);
        mName.setEnabled(value);
        mExpiryDate.setEnabled(value);
        mIssueDate.setEnabled(value);
        mPlace.setEnabled(value);
        mExpiryDatePicker.setEnabled(value);
        mIssueDatePicker.setEnabled(value);
        mName.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        mNumber.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        mIssueDate.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        mPlace.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        mExpiryDate.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        if (value)
            mName.requestFocus();
    }

    private boolean checkValidation() {
        boolean check = true;
        if (TextUtils.isEmpty(mName.getText().toString())) {
            mNameLayout.setError("Name cannot be blank");
            check = false;
        }
        if (TextUtils.isEmpty(mNumber.getText().toString())) {
            mNumberLayout.setError("Password cannot be blank");
            check = false;
        }
        return check;
    }

    private void initialize(@NonNull View view) {
        mainActivity = (MainActivity) getActivity();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mainActivity);
        mExpiryDate = view.findViewById(R.id.expiryDate);
        mIssueDatePicker = view.findViewById(R.id.issue_date_picker_actions);
        mExpiryDatePicker = view.findViewById(R.id.expiry_date_picker_actions);
        mTitle = view.findViewById(R.id.title_TextView);
        mName = view.findViewById(R.id.name_editText);
        mNumber = view.findViewById(R.id.passport_number_editText);
        mPlace = view.findViewById(R.id.place_editText);
        mIssueDate = view.findViewById(R.id.issueDate);
        mNameLayout = view.findViewById(R.id.name_layout);
        mNumberLayout = view.findViewById(R.id.number_layout);
    }

    private void saveData() {
        if (checkValidation()) {
            String id = myRef.push().getKey();
            ArrayList<String> list = new ArrayList<>();
            String name = mName.getText().toString();
            String passport_no = mNumber.getText().toString();
            String placeOfIssue = mPlace.getText().toString();
            String issueDate = mIssueDate.getText().toString();
            String expiryDate = mExpiryDate.getText().toString();
            list.add(name);
            list.add(passport_no);
            list.add(placeOfIssue);
            list.add(issueDate);
            list.add(expiryDate);
            ArrayList<String> encryptData = null;
            try {
                encryptData = mainActivity.encryptData(list, id);
            } catch (Exception e) {
                e.printStackTrace();
            }
            encryptData.add(0, id);
            Passport passportModel = new Passport(id, encryptData.get(1), encryptData.get(2), encryptData.get(3), encryptData.get(4), encryptData.get(5));
            myRef.child("Passport").child(id).setValue(passportModel);
            mainActivity.showAlertBox(getString(R.string.txt_Success), getString(R.string.txt_data_saved_success), 2);
            mainActivity.goBack();
        }
    }

    private void updateData() {
        if (checkValidation()) {
            ArrayList<String> list = new ArrayList<>();
            String name = mName.getText().toString();
            String passport_no = mNumber.getText().toString();
            String placeOfIssue = mPlace.getText().toString();
            String issueDate = mIssueDate.getText().toString();
            String expiryDate = mExpiryDate.getText().toString();
            list.add(name);
            list.add(passport_no);
            list.add(placeOfIssue);
            list.add(issueDate);
            list.add(expiryDate);
            ArrayList<String> encryptData = null;
            try {
                encryptData = mainActivity.encryptData(list, uid);
            } catch (Exception e) {
                e.printStackTrace();
            }
            encryptData.add(0, uid);
            Passport passportModel = new Passport(uid, encryptData.get(1), encryptData.get(2), encryptData.get(3), encryptData.get(4), encryptData.get(5));
            myRef.child("Passport").child(uid).setValue(passportModel);
            mainActivity.showAlertBox(getString(R.string.txt_Success), getString(R.string.txt_data_updated_success), 2);
            mainActivity.goBack();
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        if (key == 0) {

            date = Utils.formatDate(dayOfMonth, month, year);
            try {
                if (mainActivity.compareDate(date, null, true)) {
                    mIssueDate.setText(date);
                } else {
                    mIssueDate.setText("");
                    mainActivity.showAlertBox("Warning!", "Issue Date Cannot be greater than today date", 0);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

        else {
            try {
                if (mainActivity.compareDate(Utils.formatDate(dayOfMonth, month, year), date, false)) {
                    mExpiryDate.setText(Utils.formatDate(dayOfMonth, month, year));
                } else {
                    mExpiryDate.setText("");
                    mainActivity.showAlertBox("Warning!", "Expiry Date Cannot be before Issue date", 0);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

    }
}
