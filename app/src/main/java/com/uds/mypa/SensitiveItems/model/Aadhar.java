package com.uds.mypa.SensitiveItems.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Aadhar implements Parcelable {
    private String id, name, aadharNumber, dob, address,phoneNumber;

    public Aadhar() {
    }

    public Aadhar(String id, String name, String aadharNumber, String dob, String address, String phoneNumber) {
        this.id = id;
        this.name = name;
        this.aadharNumber = aadharNumber;
        this.dob = dob;
        this.address = address;
        this.phoneNumber = phoneNumber;
    }

    private Aadhar(Parcel in) {
        id = in.readString();
        name = in.readString();
        aadharNumber = in.readString();
        dob = in.readString();
        address = in.readString();
        phoneNumber = in.readString();
    }

    public static final Creator<Aadhar> CREATOR = new Creator<Aadhar>() {
        @Override
        public Aadhar createFromParcel(Parcel in) {
            return new Aadhar(in);
        }

        @Override
        public Aadhar[] newArray(int size) {
            return new Aadhar[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAadharNumber() {
        return aadharNumber;
    }

    public String getDob() {
        return dob;
    }

    public String getAddress() {
        return address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(aadharNumber);
        dest.writeString(dob);
        dest.writeString(address);
        dest.writeString(phoneNumber);
    }
}
