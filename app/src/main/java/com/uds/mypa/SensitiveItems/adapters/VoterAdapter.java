package com.uds.mypa.SensitiveItems.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.uds.mypa.Interfaces.OnItemClickListener;
import com.uds.mypa.R;
import com.uds.mypa.SensitiveItems.model.VoterCard;

import java.util.ArrayList;

public class VoterAdapter extends RecyclerView.Adapter<VoterAdapter.ViewHolder>{
    private ArrayList<VoterCard> arrayList;
    private Context context;
    private OnItemClickListener onItemClickListener;

    public VoterAdapter(ArrayList<VoterCard> arrayList, Context context, OnItemClickListener onItemClickListener) {
        this.arrayList = arrayList;
        this.context = context;
        this.onItemClickListener = onItemClickListener;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.passport_layout, parent, false);
        return new ViewHolder(view, onItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        VoterCard passport = arrayList.get(position);
        holder.name.setText(passport.getName());
        holder.number.setText(passport.getVoterIDNumber());
    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name, number;
        OnItemClickListener onItemClickListner;

        public ViewHolder(@NonNull View itemView, OnItemClickListener onItemClickListner) {
            super(itemView);
            name = itemView.findViewById(R.id.name_editText);
            number = itemView.findViewById(R.id.number);
            this.onItemClickListner = onItemClickListner;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onItemClickListner.onItemClick(getAdapterPosition());
        }
    }

}
