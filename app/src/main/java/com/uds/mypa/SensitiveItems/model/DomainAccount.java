package com.uds.mypa.SensitiveItems.model;

public class DomainAccount {
   private String id,password;

    public DomainAccount() {
    }

    public DomainAccount(String id, String password) {
        this.id = id;
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public String getPassword() {
        return password;
    }
}
