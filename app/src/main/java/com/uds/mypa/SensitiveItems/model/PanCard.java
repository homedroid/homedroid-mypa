package com.uds.mypa.SensitiveItems.model;

import android.os.Parcel;
import android.os.Parcelable;

public class PanCard  implements Parcelable {
    private String id, name, panNumber, fathersName, dob;
    public PanCard(String id, String name, String panNumber, String fathersName, String dob) {
        this.id = id;
        this.name = name;
        this.panNumber = panNumber;
        this.fathersName = fathersName;
        this.dob = dob;
    }

    public PanCard() {
    }

    protected PanCard(Parcel in) {
        id = in.readString();
        name = in.readString();
        panNumber = in.readString();
        fathersName = in.readString();
        dob = in.readString();
    }

    public static final Creator<PanCard> CREATOR = new Creator<PanCard>() {
        @Override
        public PanCard createFromParcel(Parcel in) {
            return new PanCard(in);
        }

        @Override
        public PanCard[] newArray(int size) {
            return new PanCard[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPanNumber() {
        return panNumber;
    }

    public String getFathersName() {
        return fathersName;
    }

    public String getDob() {
        return dob;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(panNumber);
        dest.writeString(fathersName);
        dest.writeString(dob);
    }
}
