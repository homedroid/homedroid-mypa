package com.uds.mypa.SensitiveItems.model;

public class Aws {
    private String id,password,parseServerID,parseServerPassword;

    public Aws() {
    }

    public Aws(String id, String password, String parseServerID, String parseServerPassword) {
        this.id = id;
        this.password = password;
        this.parseServerID = parseServerID;
        this.parseServerPassword = parseServerPassword;
    }

    public String getId() {
        return id;
    }

    public String getPassword() {
        return password;
    }

    public String getParseServerID() {
        return parseServerID;
    }

    public String getParseServerPassword() {
        return parseServerPassword;
    }
}
