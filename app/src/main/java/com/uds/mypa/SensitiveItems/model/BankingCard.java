package com.uds.mypa.SensitiveItems.model;

import android.os.Parcel;
import android.os.Parcelable;

public class BankingCard implements Parcelable {
    private String id, name, bankName, cardType, number, validFrom, validTo, cvv, mpin, pin, phone, email, cardNameType, bankLogo, cardTypeLogo;
    private int cardIndex;

    public BankingCard() {
    }

    public BankingCard(String id, String name, String bankName, String cardType, String number, String validFrom, String validTo, String cvv, String mpin, String pin, String phone, String email, String cardNameType, String bankLogo, String cardTypeLogo, int cardIndex) {
        this.id = id;
        this.name = name;
        this.bankName = bankName;
        this.cardType = cardType;
        this.number = number;
        this.validFrom = validFrom;
        this.validTo = validTo;
        this.cvv = cvv;
        this.mpin = mpin;
        this.pin = pin;
        this.phone = phone;
        this.email = email;
        this.cardNameType = cardNameType;
        this.bankLogo = bankLogo;
        this.cardTypeLogo = cardTypeLogo;
        this.cardIndex = cardIndex;
    }

    protected BankingCard(Parcel in) {
        id = in.readString();
        name = in.readString();
        bankName = in.readString();
        cardType = in.readString();
        number = in.readString();
        validFrom = in.readString();
        validTo = in.readString();
        cvv = in.readString();
        mpin = in.readString();
        pin = in.readString();
        phone = in.readString();
        email = in.readString();
        cardNameType = in.readString();
        bankLogo = in.readString();
        cardTypeLogo = in.readString();
        cardIndex = in.readInt();
    }

    public static final Creator<BankingCard> CREATOR = new Creator<BankingCard>() {
        @Override
        public BankingCard createFromParcel(Parcel in) {
            return new BankingCard(in);
        }

        @Override
        public BankingCard[] newArray(int size) {
            return new BankingCard[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getBankName() {
        return bankName;
    }

    public String getCardType() {
        return cardType;
    }

    public String getNumber() {
        return number;
    }

    public String getValidFrom() {
        return validFrom;
    }

    public String getValidTo() {
        return validTo;
    }

    public String getCvv() {
        return cvv;
    }

    public String getMpin() {
        return mpin;
    }

    public String getPin() {
        return pin;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    public String getCardNameType() {
        return cardNameType;
    }

    public String getBankLogo() {
        return bankLogo;
    }

    public int getCardIndex() {
        return cardIndex;
    }

    public String getCardTypeLogo() {
        return cardTypeLogo;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(bankName);
        dest.writeString(cardType);
        dest.writeString(number);
        dest.writeString(validFrom);
        dest.writeString(validTo);
        dest.writeString(cvv);
        dest.writeString(mpin);
        dest.writeString(pin);
        dest.writeString(phone);
        dest.writeString(email);
        dest.writeString(cardNameType);
        dest.writeString(bankLogo);
        dest.writeString(cardTypeLogo);
        dest.writeInt(cardIndex);
    }
}
