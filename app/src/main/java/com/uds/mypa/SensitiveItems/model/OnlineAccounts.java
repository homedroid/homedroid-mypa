package com.uds.mypa.SensitiveItems.model;

import android.os.Parcel;
import android.os.Parcelable;

public class OnlineAccounts implements Parcelable {
    private String id,name,website,userName,phone,password;

    public OnlineAccounts(String id, String name, String website, String userName, String phone, String password) {
        this.id = id;
        this.name = name;
        this.website = website;
        this.userName = userName;
        this.phone = phone;
        this.password = password;
    }

    public OnlineAccounts() {
    }

    protected OnlineAccounts(Parcel in) {
        id = in.readString();
        name = in.readString();
        website = in.readString();
        userName = in.readString();
        phone = in.readString();
        password = in.readString();
    }

    public static final Creator<OnlineAccounts> CREATOR = new Creator<OnlineAccounts>() {
        @Override
        public OnlineAccounts createFromParcel(Parcel in) {
            return new OnlineAccounts(in);
        }

        @Override
        public OnlineAccounts[] newArray(int size) {
            return new OnlineAccounts[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getWebsite() {
        return website;
    }

    public String getUserName() {
        return userName;
    }

    public String getPhone() {
        return phone;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(website);
        dest.writeString(userName);
        dest.writeString(phone);
        dest.writeString(password);
    }
}
