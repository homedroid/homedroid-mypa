package com.uds.mypa.SensitiveItems.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.uds.mypa.Activitys.MainActivity;
import com.uds.mypa.Interfaces.OnItemClickListener;
import com.uds.mypa.R;
import com.uds.mypa.SensitiveItems.adapters.BankingCardAdapter;
import com.uds.mypa.SensitiveItems.model.BankingCard;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class BankingCardFragment extends Fragment implements OnItemClickListener {
    private ArrayList<BankingCard> arrayList;
    private MainActivity mainActivity;
    private FirebaseAnalytics mFirebaseAnalytics;
    private FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    private DatabaseReference reference = FirebaseDatabase.getInstance().getReference(user.getUid()).child("Banking Card");
    private ArrayList<BankingCard> list;
    private RecyclerView recyclerView;
    private OnItemClickListener listener;
    private BankingCardAdapter adapter;
    private ShimmerFrameLayout shimmerFrameLayout;
    private LottieAnimationView lottieAnimationView;
    public BankingCardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_banking_card, container, false);
        mainActivity = (MainActivity) getActivity();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mainActivity);
        arrayList = new ArrayList<>();
        listener = this;

        shimmerFrameLayout = view.findViewById(R.id.shimmer);
        shimmerFrameLayout.startShimmerAnimation();
        list = new ArrayList<>();
        recyclerView = view.findViewById(R.id.recyclerView);
        lottieAnimationView=view.findViewById(R.id.animation_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(mainActivity));
        FloatingActionButton fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.setUpFragment(13, null);
            }
        });
        recyclerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainActivity.toggleBottomSheet();
            }
        });
        shimmerFrameLayout.startShimmerAnimation();
        recyclerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainActivity.toggleBottomSheet();
            }
        });
        Toolbar mToolbar = view.findViewById(R.id.toolbar);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.goBack();
            }
        });
        return view;
    }
    private void showData() {

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                list.clear();
                for (DataSnapshot passSnapshot : dataSnapshot.getChildren()) {
                    BankingCard bankingCard = passSnapshot.getValue(BankingCard.class);
                    list.add(bankingCard);
                }
                try {
                    arrayList = getDataList(list);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                adapter = null;
                try {
                    adapter = new BankingCardAdapter(arrayList, mainActivity, listener);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                shimmerFrameLayout.stopShimmerAnimation();
                shimmerFrameLayout.setVisibility(View.GONE);
                recyclerView.setAdapter(adapter);
                if (list.isEmpty()) {
                    lottieAnimationView.playAnimation();
                    lottieAnimationView.setVisibility(View.VISIBLE);
                }
                else {
                    lottieAnimationView.pauseAnimation();
                    lottieAnimationView.setVisibility(View.GONE);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    private ArrayList<BankingCard> getDataList(ArrayList<BankingCard> arrayList) throws Exception {
        ArrayList<BankingCard> list = new ArrayList<>();
        for (int i = 0; i < arrayList.size(); i++) {
            BankingCard cuur = arrayList.get(i);
            String id, name, bank, cardType, cardName, number, from, thru, cvv, mpin, pin, phone, email, imageId, imageId1;
            int cardIndex;
            id = cuur.getId();
            name = cuur.getName();
            bank = cuur.getBankName();
            cardType = cuur.getCardType();
            cardName = cuur.getCardNameType();
            from = cuur.getValidFrom();
            thru = cuur.getValidTo();
            cvv = cuur.getCvv();
            number = cuur.getNumber();
            mpin = cuur.getMpin();
            pin = cuur.getPin();
            phone = cuur.getPhone();
            email = cuur.getEmail();
            imageId = cuur.getBankLogo();
            imageId1 = cuur.getCardTypeLogo();
            cardIndex=cuur.getCardIndex();
            list.add(new BankingCard(id, mainActivity.decryptData(name, id), mainActivity.decryptData(bank, id), mainActivity.decryptData(cardType, id), mainActivity.decryptData(number, id), mainActivity.decryptData(from, id), mainActivity.decryptData(thru, id), mainActivity.decryptData(cvv, id), mainActivity.decryptData(mpin, id), mainActivity.decryptData(pin, id), mainActivity.decryptData(phone, id), mainActivity.decryptData(email, id), mainActivity.decryptData(cardName, id), mainActivity.decryptData(imageId, id), imageId1,cardIndex));
        }
        return list;
    }

    @Override
    public void onItemClick(int position) {
        mainActivity.openBottomSheet(arrayList.get(position).getId(), "Banking Card", arrayList.get(position));
    }
    @Override
    public void onStart() {
        super.onStart();
        showData();
        mainActivity.collapseBottomSheet();
    }
}
