package com.uds.mypa.SensitiveItems.fragment;


import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.uds.mypa.Activitys.MainActivity;
import com.uds.mypa.R;
import com.uds.mypa.SensitiveItems.model.VoterCard;
import com.uds.mypa.utils.Utils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewVoterIdFragment extends Fragment implements DatePickerDialog.OnDateSetListener {
    private TextInputLayout mNameLayout, mNumberLayout;
    private TextInputEditText mDateOfBirth, mName, mNumber,  mIssueDate, mAddress,mFathersName;
    private TextView mTitle;
    private MainActivity mainActivity;
    private FirebaseAnalytics mFirebaseAnalytics;
    private ImageView mIssueDatePicker, mDateOfBirthPicker;
    private FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    private String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
    private DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(uid);
    private VoterCard voterCard;
    private Boolean save = true;
    private int mYear, mMonth, mDay;
    private int key;
    private String date;
    public NewVoterIdFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
        mainActivity.collapseBottomSheet();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_voter_id, container, false);
        myRef.keepSynced(true);
        initialize(view);
        mainActivity = (MainActivity) getActivity();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mainActivity);
        mTitle = view.findViewById(R.id.title_TextView);
        mTitle.setText("Add Voter ID");
        final FloatingActionButton saveFab, editFab;
        editFab = view.findViewById(R.id.edit_fab);
        editFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleViews(true);
                editFab.setVisibility(View.GONE);
            }
        });
        saveFab=view.findViewById(R.id.save_fab);
        saveFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (save)
                    saveData();
                else
                    updateData();
            }
        });
        Bundle bundle = getArguments();
        if (bundle != null) {
            voterCard = bundle.getParcelable("voter");
            mName.setText(voterCard.getName());
            mAddress.setText(voterCard.getAddress());
            mNumber.setText(voterCard.getVoterIDNumber());
            mIssueDate.setText(voterCard.getDateOfIssue());
            mDateOfBirth.setText(voterCard.getDob());
            uid = voterCard.getId();
            mFathersName.setText(voterCard.getFathersName());
            save = false;
            if (bundle.getInt("choose") == 0) {
                toggleViews(false);
                editFab.setVisibility(View.VISIBLE);
            } else {
                toggleViews(true);
                editFab.setVisibility(View.GONE);
            }
        }
        mDateOfBirthPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        NewVoterIdFragment.this, mYear, mMonth, mDay);
                datePickerDialog.show();
                key = 0;
            }
        });
        mIssueDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                        NewVoterIdFragment.this, mYear, mMonth, mDay);
                datePickerDialog.show();
                key = 1;

            }
        });
        return view;
    }

    private void toggleViews(boolean b) {
        mAddress.setEnabled(b);
        mDateOfBirth.setEnabled(b);
        mDateOfBirthPicker.setEnabled(b);
        mFathersName.setEnabled(b);
        mIssueDate.setEnabled(b);
        mIssueDatePicker.setEnabled(b);
        mName.setEnabled(b);
        mNumber.setEnabled(b);
        mName.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mNumber.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mFathersName.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mDateOfBirth.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mAddress.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mIssueDate.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        if (b)
            mName.requestFocus();
    }


    private void initialize(@NonNull View view) {
        mainActivity = (MainActivity) getActivity();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mainActivity);
        mDateOfBirth = view.findViewById(R.id.date_of_birth_editText);
        mIssueDatePicker = view.findViewById(R.id.issue_date_picker_actions);
        mDateOfBirthPicker = view.findViewById(R.id.dob_picker_actions);
        mTitle = view.findViewById(R.id.title_TextView);
        mAddress=view.findViewById(R.id.address_editText);
        mName = view.findViewById(R.id.name_editText);
        mNumber = view.findViewById(R.id.voterId_editText);
        mFathersName = view.findViewById(R.id.fathersName_editText);
        mIssueDate = view.findViewById(R.id.issueDate);
        mNameLayout=view.findViewById(R.id.name_layout);
        mNumberLayout=view.findViewById(R.id.number_layout);

    }
    private boolean checkValidation() {
        boolean check = true;
        if (TextUtils.isEmpty(mName.getText().toString())) {
            mNameLayout.setError("Please Enter Name");
            check = false;
        }
        if (TextUtils.isEmpty(mNumber.getText().toString())) {
            mNumberLayout.setError("Please Enter Voter ID Number");
            check = false;
        }
        return check;
    }

    private void saveData() {
        ArrayList<String> list = new ArrayList<>();
       if (checkValidation()) {
           String id = myRef.push().getKey();
           String name = mName.getText().toString();
           String voterIdNumber = mNumber.getText().toString().trim();
           String fathersName = mFathersName.getText().toString().trim();
           String address = mAddress.getText().toString().trim();
           String issueDate = mIssueDate.getText().toString().trim();
           String dob = mDateOfBirth.getText().toString().trim();

           list.add(name);
           list.add(voterIdNumber);
           list.add(dob);
           list.add(address);
           list.add(issueDate);
           list.add(fathersName);
           ArrayList<String> encryptData = null;
           try {
               encryptData = mainActivity.encryptData(list, id);
           } catch (Exception e) {
               e.printStackTrace();
           }
           encryptData.add(0, id);
           VoterCard voterModel = new VoterCard(id, encryptData.get(1), encryptData.get(2), encryptData.get(3), encryptData.get(4), encryptData.get(5), encryptData.get(6));
           myRef.child("Voter Card").child(id).setValue(voterModel);
           mainActivity.showAlertBox(getString(R.string.txt_Success),getString(R.string.txt_data_saved_success),2);
           mainActivity.goBack();
       }
    }


    private void updateData() {
        if (checkValidation()) {
            ArrayList<String> list = new ArrayList<>();
            String name = mName.getText().toString();
            String voterIdNumber = mNumber.getText().toString().trim();
            String fathersName = mFathersName.getText().toString().trim();
            String address = mAddress.getText().toString().trim();
            String issueDate = mIssueDate.getText().toString().trim();
            String dob = mDateOfBirth.getText().toString().trim();
            list.add(name);
            list.add(voterIdNumber);
            list.add(dob);
            list.add(address);
            list.add(issueDate);
            list.add(fathersName);
            ArrayList<String> encryptData = null;
            try {
                encryptData = mainActivity.encryptData(list, uid);
            } catch (Exception e) {
                e.printStackTrace();
            }
            encryptData.add(0, uid);
            VoterCard voterModel = new VoterCard(uid, encryptData.get(1), encryptData.get(2), encryptData.get(3), encryptData.get(4), encryptData.get(5), encryptData.get(6));
            myRef.child("Voter Card").child(uid).setValue(voterModel);
            mainActivity.showAlertBox(getString(R.string.txt_Success),getString(R.string.txt_data_updated_success),2);
            mainActivity.goBack();
        }
    }


    @Override
    public void onDateSet(DatePicker datePicker,int year, int month, int dayOfMonth) {
        if (key == 0) {

            date = Utils.formatDate(dayOfMonth, month, year);
            try {
                if (mainActivity.compareDate(date, null, true)) {
                    mDateOfBirth.setText(date);
                }
                else {
                    mDateOfBirth.setText("");
                    mainActivity.showAlertBox("Warning!", "Date of Birth Cannot be greater than today date", 0);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
            try {
                if (mainActivity.compareDate(Utils.formatDate(dayOfMonth, month, year), null, true)) {
                    mIssueDate.setText(Utils.formatDate(dayOfMonth, month, year));
                } else {
                    mIssueDate.setText("");
                    mainActivity.showAlertBox("Warning!", "Issue Date Cannot be before Today date", 0);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }

    }
}
