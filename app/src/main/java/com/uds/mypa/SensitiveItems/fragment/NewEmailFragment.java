package com.uds.mypa.SensitiveItems.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.uds.mypa.Activitys.MainActivity;
import com.uds.mypa.R;
import com.uds.mypa.SensitiveItems.model.Email;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewEmailFragment extends Fragment {
    TextInputLayout mNameLayout, mEmailLayout, mPasswordLayout,mRecoveryEmailLayout,mPhoneLayout;
    TextInputEditText mName, mEmail, mRecoveryEmail, mPassword, mPhone;
    private TextView mTitle;
    private ImageView mBack;
    private MainActivity mainActivity;
    private FirebaseAnalytics mFirebaseAnalytics;
    private String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
    private DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(uid);
    private Boolean save = true;

    public NewEmailFragment() {
        // Required empty public constructor
    }
    @Override
    public void onStart() {
        super.onStart();
        mainActivity.collapseBottomSheet();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_email, container, false);
        myRef.keepSynced(true);
        initialize(view);
        mTitle.setText("Add Email Account");

        final FloatingActionButton saveFab, editFab;
        saveFab = view.findViewById(R.id.save_fab);
        editFab = view.findViewById(R.id.edit_fab);
        editFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleViews(true);
                editFab.setVisibility(View.GONE);
            }
        });
        Bundle bundle = getArguments();
        if (bundle != null) {
            Email wifi = bundle.getParcelable("email");
            mName.setText(wifi.getName());
            mEmail.setText(wifi.getEmail());
            mPassword.setText(wifi.getPassword());
            mRecoveryEmail.setText(wifi.getRecoveryEmail());
            mPhone.setText(wifi.getPhone());
            uid = wifi.getId();
            if (bundle.getInt("choose") == 0) {
                toggleViews(false);
                editFab.setVisibility(View.VISIBLE);
            } else {
                toggleViews(true);
                editFab.setVisibility(View.GONE);
            }
        }
        saveFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (save)
                    saveData();
                else
                    updateData();
            }
        });

        return view;
    }

    private void toggleViews(boolean b) {
        mPassword.setEnabled(b);
        mRecoveryEmail.setEnabled(b);
        mPhone.setEnabled(b);
        mEmail.setEnabled(b);
        mName.setEnabled(b);
        mName.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mEmail.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mPassword.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mRecoveryEmail.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mPhone.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        if (b)
            mName.requestFocus();
    }

    private void initialize(@NonNull View view) {
        mainActivity = (MainActivity) getActivity();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mainActivity);
        mBack = view.findViewById(R.id.back_arrow);
        mTitle = view.findViewById(R.id.title_TextView);
        mName = view.findViewById(R.id.name_editText);
        mEmail = view.findViewById(R.id.email_editText);
        mPhone = view.findViewById(R.id.phone_editText);
        mRecoveryEmail = view.findViewById(R.id.recovery_email_editText);
        mPassword = view.findViewById(R.id.password_editText);
        mNameLayout = view.findViewById(R.id.name_layout);
        mPasswordLayout = view.findViewById(R.id.password_layout);
        mEmailLayout = view.findViewById(R.id.email_layout);
        mRecoveryEmailLayout=view.findViewById(R.id.recover_email_layout);
        mPhoneLayout = view.findViewById(R.id.phone_layout);
        mEmailLayout = view.findViewById(R.id.email_layout);

    }

    private void saveData() {
        if (checkValidation()) {
            String id = myRef.push().getKey();
            ArrayList<String> list = new ArrayList<>();
            String name = mName.getText().toString();
            String email = mEmail.getText().toString().trim();
            String rmail = mRecoveryEmail.getText().toString().trim();
            String password = mPassword.getText().toString().trim();
            String phone = mPhone.getText().toString().trim();
            list.add(name);
            list.add(password);
            list.add(email);
            list.add(rmail);
            list.add(phone);
            ArrayList<String> encryptData = null;
            try {
                encryptData = mainActivity.encryptData(list, id);
            } catch (Exception e) {
                e.printStackTrace();
            }
            encryptData.add(0, id);
            Email email1 = new Email(id, encryptData.get(1), encryptData.get(2), encryptData.get(3), encryptData.get(4), encryptData.get(5));
            myRef.child("Email Account").child(id).setValue(email1);
            mainActivity.showAlertBox(getString(R.string.txt_Success), getString(R.string.txt_data_saved_success), 2);
            mainActivity.goBack();
        }

    }

    private void updateData() {
        if (checkValidation()) {
            ArrayList<String> list = new ArrayList<>();
            String name = mName.getText().toString();
            String email = mEmail.getText().toString().trim();
            String rmail = mRecoveryEmail.getText().toString().trim();
            String password = mPassword.getText().toString().trim();
            String phone = mPhone.getText().toString().trim();
            list.add(name);
            list.add(password);
            list.add(email);
            list.add(rmail);
            list.add(phone);
            ArrayList<String> encryptData = null;
            try {
                encryptData = mainActivity.encryptData(list, uid);
            } catch (Exception e) {
                e.printStackTrace();
            }
            encryptData.add(0, uid);
            Email email1 = new Email(uid, encryptData.get(1), encryptData.get(2), encryptData.get(3), encryptData.get(4), encryptData.get(5));
            myRef.child("Email Account").child(uid).setValue(email1);
            mainActivity.showAlertBox(getString(R.string.txt_Success), getString(R.string.txt_data_updated_success), 2);
            mainActivity.goBack();
        }
    }

    private boolean checkValidation() {
        boolean check = true;
        if (TextUtils.isEmpty(mName.getText().toString())) {
            mNameLayout.setError("Please Enter Name");
            check = false;
        }
        if (TextUtils.isEmpty(mPassword.getText().toString())) {
            mPasswordLayout.setError("Please Enter Password");
            check = false;
        }
        if (TextUtils.isEmpty(mEmail.getText().toString())) {
            mEmailLayout.setError("Please Enter Email");
            check = false;
        }
        if (!mainActivity.isValidEmail(mEmail.getText().toString()))
            check = false;
        if (!TextUtils.isEmpty(mPhone.getText().toString()))
            if (!mainActivity.isValidPhone(mPhone.getText().toString())) {
                mPhoneLayout.setError("Invalid Phone Number");
                check = false;
            }
        if (!TextUtils.isEmpty(mEmail.getText().toString()))
            if (!mainActivity.isValidEmail(mRecoveryEmail.getText().toString())) {
                mRecoveryEmailLayout.setError("Invalid Email Address");
                check = false;
            }
        return check;
    }

}