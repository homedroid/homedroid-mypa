package com.uds.mypa.SensitiveItems.fragment;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.uds.mypa.Activitys.MainActivity;
import com.uds.mypa.R;
import com.uds.mypa.SensitiveItems.model.Insurance;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewInsuranceFragment extends Fragment implements DatePickerDialog.OnDateSetListener {
    private TextInputLayout mCompanyNameLayout, mNameLayout, mPolicyNumberLayout;
    private TextInputEditText mDueDate, mName, mCompanyName, mPlan, mPolicyName, mPolicyNumber, mPolicyDuration, mInstallment;
    private TextView mTitle;
    private MainActivity mainActivity;
    private FirebaseAnalytics mFirebaseAnalytics;
    private ImageView mDueDatePicker;
    private String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
    private DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(uid);
    private Boolean save = true;
    private Spinner mFrequencySpinner;
    private String frequency = "";

    public NewInsuranceFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
        mainActivity.collapseBottomSheet();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_insurance, container, false);
        myRef.keepSynced(true);
        initialize(view);
        mTitle.setText("Add Insurence Detail");
        mDueDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(mainActivity,
                        NewInsuranceFragment.this, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });
        final FloatingActionButton saveFab, editFab;
        saveFab = view.findViewById(R.id.save_fab);
        editFab = view.findViewById(R.id.edit_fab);
        editFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleViews(true);
                editFab.setVisibility(View.GONE);
            }
        });
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(mainActivity,
                R.array.frequencyArray, android.R.layout.simple_spinner_dropdown_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mFrequencySpinner.setAdapter(adapter);
        mFrequencySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                frequency= String.valueOf(parent.getItemAtPosition(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        Bundle bundle = getArguments();
        if (bundle != null) {
            Insurance insurance = bundle.getParcelable("insurance");
            mName.setText(insurance.getName());
            mDueDate.setText(insurance.getDueDate());
            mCompanyName.setText(insurance.getCompanyName());
            mInstallment.setText(insurance.getPremium());
            mPlan.setText(insurance.getPlan());
            mPolicyDuration.setText(insurance.getDuration());
            mPolicyName.setText(insurance.getPolicyName());
            mPolicyNumber.setText(insurance.getNumber());
            frequency = insurance.getFrequency();
            String[] arr = getResources().getStringArray(R.array.frequencyArray);
            for (int j = 0; j < arr.length; j++) {
                if (frequency.equals(arr[j])) {
                    mFrequencySpinner.setSelection(j);
                    break;
                }
            }
            uid = insurance.getId();
            save = false;
            if (bundle.getInt("choose") == 0) {
                toggleViews(false);
                editFab.setVisibility(View.VISIBLE);
            } else {
                toggleViews(true);
                editFab.setVisibility(View.GONE);
            }


        }

        saveFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (save)
                    saveData();
                else
                    updateData();
            }
        });
        mDueDate.addTextChangedListener(new TextWatcher() {
            int prevL = 0;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                int length = editable.length();
                if ((prevL < length) && (length == 2 || length == 5)) {
                    editable.append("/");
                }
            }
        });

        return view;
    }

    private void initialize(View view) {
        mainActivity = (MainActivity) getActivity();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mainActivity);
        mDueDate = view.findViewById(R.id.dueDate);
        mFrequencySpinner = view.findViewById(R.id.frequencySpinner);
        mDueDatePicker = view.findViewById(R.id.due_date_picker_actions);
        mCompanyName = view.findViewById(R.id.company_name_editText);
        mTitle = view.findViewById(R.id.title_TextView);
        mName = view.findViewById(R.id.name_editText);
        mPolicyNumber = view.findViewById(R.id.policy_number_editText);
        mPlan = view.findViewById(R.id.plan_editText);
        mPolicyName = view.findViewById(R.id.policy_name_editText);
        mNameLayout = view.findViewById(R.id.name_layout);
        mPolicyNumberLayout = view.findViewById(R.id.number_layout);
        mPolicyDuration = view.findViewById(R.id.policy_durationr_editText);
        mInstallment = view.findViewById(R.id.instalment_editText);
        mCompanyNameLayout = view.findViewById(R.id.company_name_layout);
        mNameLayout = view.findViewById(R.id.name_layout);
        mPolicyNumberLayout = view.findViewById(R.id.policy_number_layout);
    }


    private void saveData() {
        if (checkValidation()) {
            String id = myRef.push().getKey();
            ArrayList<String> list = new ArrayList<>();
            String name, companyName, plan, policyName, number, duration, premium, dueDate;
            name = mName.getText().toString();
            number = mPolicyNumber.getText().toString().trim();
            companyName = mCompanyName.getText().toString().trim();
            premium = mInstallment.getText().toString().trim();
            plan = mPlan.getText().toString().trim();
            policyName = mPolicyName.getText().toString().trim();
            duration = mPolicyDuration.getText().toString().trim();
            dueDate = mDueDate.getText().toString().trim();
            list.add(name);
            list.add(companyName);
            list.add(plan);
            list.add(policyName);
            list.add(number);
            list.add(duration);
            list.add(premium);
            list.add(frequency);
            list.add(dueDate);
            ArrayList<String> encryptData = null;
            try {
                encryptData = mainActivity.encryptData(list, id);
            } catch (Exception e) {
                e.printStackTrace();
            }
            encryptData.add(0, id);
            Insurance insurance = new Insurance(id, encryptData.get(1), encryptData.get(2), encryptData.get(3), encryptData.get(4), encryptData.get(5), encryptData.get(6), encryptData.get(7), encryptData.get(8), encryptData.get(9));
            myRef.child("Insurance").child(id).setValue(insurance);
            mainActivity.showAlertBox(getString(R.string.txt_Success), getString(R.string.txt_data_saved_success), 2);
            mainActivity.goBack();
        }
    }

    private void updateData() {
        if (checkValidation()) {
            ArrayList<String> list = new ArrayList<>();
            String name, companyName, plan, policyName, number, duration, premium, dueDate;
            name = mName.getText().toString();
            number = mPolicyNumber.getText().toString().trim();
            companyName = mCompanyName.getText().toString().trim();
            premium = mInstallment.getText().toString().trim();
            plan = mPlan.getText().toString().trim();
            policyName = mPolicyName.getText().toString().trim();
            duration = mPolicyDuration.getText().toString().trim();
            dueDate = mDueDate.getText().toString().trim();
            list.add(name);
            list.add(companyName);
            list.add(plan);
            list.add(policyName);
            list.add(number);
            list.add(duration);
            list.add(premium);
            list.add(frequency);
            list.add(dueDate);
            ArrayList<String> encryptData = null;
            try {
                encryptData = mainActivity.encryptData(list, uid);
            } catch (Exception e) {
                e.printStackTrace();
            }
            encryptData.add(0, uid);
            Insurance insurance = new Insurance(uid, encryptData.get(1), encryptData.get(2), encryptData.get(3), encryptData.get(4), encryptData.get(5), encryptData.get(6), encryptData.get(7), encryptData.get(8), encryptData.get(9));
            myRef.child("Insurance").child(uid).setValue(insurance);
            mainActivity.showAlertBox(getString(R.string.txt_Success), getString(R.string.txt_data_updated_success), 2);
            mainActivity.goBack();
        }
    }

    private void toggleViews(boolean value) {
        mPolicyNumber.setEnabled(value);
        mName.setEnabled(value);
        mPolicyName.setEnabled(value);
        mPolicyDuration.setEnabled(value);
        mPlan.setEnabled(value);
        mInstallment.setEnabled(value);
        mCompanyName.setEnabled(value);
        mDueDate.setEnabled(value);
        mDueDatePicker.setEnabled(value);
        mFrequencySpinner.setEnabled(value);
        mName.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        mPolicyName.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        mPolicyDuration.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        mPlan.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        mInstallment.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        mCompanyName.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        mDueDate.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        mPolicyNumber.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        if (value)
            mCompanyName.requestFocus();
    }


    private boolean checkValidation() {
        boolean check = true;
        if (TextUtils.isEmpty(mCompanyName.getText().toString())) {
            mCompanyNameLayout.setError("Please Enter Insurance Company Name");
            check = false;
        }
        if (TextUtils.isEmpty(mPolicyNumber.getText().toString())) {
            mPolicyNumberLayout.setError("Please Enter Policy Number");
            check = false;
        }
        if (TextUtils.isEmpty(mName.getText().toString())) {
            mNameLayout.setError("Please Enter Policy Holder Name");
            check = false;
        }
        return check;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        mDueDate.setText(String.format("%02d", dayOfMonth) + "/" + String.format("%02d", (month + 1)) + "/" + String.format("%02d", year));
    }
}
