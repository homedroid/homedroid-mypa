package com.uds.mypa.SensitiveItems.model;

public class RailwayAccount {
    private String id,password,name,dateOfBirth,email,phone;

    public RailwayAccount() {
    }

    public RailwayAccount(String id, String password, String name, String dateOfBirth, String email, String phone) {
        this.id = id;
        this.password = password;
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.email = email;
        this.phone = phone;
    }

    public String getId() {
        return id;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }
}
