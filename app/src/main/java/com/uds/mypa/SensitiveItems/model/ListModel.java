package com.uds.mypa.SensitiveItems.model;

public class ListModel {
    private int image;
    private String name;

    public ListModel(String name, int image) {
        this.image = image;
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public String getName() {
        return name;
    }
}
