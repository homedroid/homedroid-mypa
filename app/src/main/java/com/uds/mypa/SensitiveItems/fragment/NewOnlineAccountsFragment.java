package com.uds.mypa.SensitiveItems.fragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.uds.mypa.Activitys.MainActivity;
import com.uds.mypa.R;
import com.uds.mypa.SensitiveItems.model.OnlineAccounts;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewOnlineAccountsFragment extends Fragment {
    private TextInputEditText mWebsite, mName, mUserName, mPassword, mPhone;
    private TextView mTitle;
    private TextInputLayout websiteLayout, nameLayout, mUserNameLayout, passwordLayout,mPhoneLayout;
    private MainActivity mainActivity;
    private FirebaseAnalytics mFirebaseAnalytics;
    private String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
    private DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(uid);
    private OnlineAccounts onlineAccounts;
    private Boolean save = true;

    public NewOnlineAccountsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
        mainActivity.collapseBottomSheet();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_online_accounts, container, false);
        myRef.keepSynced(true);
        initialize(view);
        mainActivity = (MainActivity) getActivity();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mainActivity);
        mTitle = view.findViewById(R.id.title_TextView);
        mTitle.setText("Add Online Accounts");
        final FloatingActionButton saveFab, editFab;
        saveFab = view.findViewById(R.id.save_fab);
        editFab = view.findViewById(R.id.edit_fab);
        editFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleViews(true);
                editFab.setVisibility(View.GONE);
            }
        });
        Bundle bundle = getArguments();
        if (bundle != null) {
            onlineAccounts = bundle.getParcelable("online_accounts");
            mName.setText(onlineAccounts.getName());
            mWebsite.setText(onlineAccounts.getWebsite());
            mUserName.setText(onlineAccounts.getUserName());
            mPassword.setText(onlineAccounts.getPassword());
            mPhone.setText(onlineAccounts.getPhone());
            uid = onlineAccounts.getId();
            save = false;
            if (bundle.getInt("choose") == 0) {
                toggleViews(false);
                editFab.setVisibility(View.VISIBLE);
            } else {
                toggleViews(true);
                editFab.setVisibility(View.GONE);
            }
        }

        saveFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (save)
                    saveData();
                else
                    updateData();
            }
        });
        return view;
    }

    private void initialize(@NonNull View view) {
        mainActivity = (MainActivity) getActivity();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mainActivity);
        mTitle = view.findViewById(R.id.title_TextView);
        mPhone = view.findViewById(R.id.phone_editText);
        mName = view.findViewById(R.id.name_editText);
        mWebsite = view.findViewById(R.id.website_name_editText);
        mUserName = view.findViewById(R.id.user_name_editText);
        mPassword = view.findViewById(R.id.password_editText);
        websiteLayout = view.findViewById(R.id.website_layout);
        mUserNameLayout = view.findViewById(R.id.user_name_layout);
        passwordLayout = view.findViewById(R.id.password_layout);
        nameLayout = view.findViewById(R.id.name_layout);
        mPhoneLayout = view.findViewById(R.id.phone_layout);
    }

    private boolean checkValidation() {
        boolean check = true;
        if (TextUtils.isEmpty(mName.getText().toString())) {
            nameLayout.setError("Cannot be Empty");
            check = false;
        }
        if (TextUtils.isEmpty(mWebsite.getText().toString())) {
            websiteLayout.setError("Cannot be Empty");
            check = false;
        }
        if (TextUtils.isEmpty(mPassword.getText().toString())) {
            passwordLayout.setError("Cannot be Empty");
            check = false;
        }
        if (TextUtils.isEmpty(mUserName.getText().toString())) {
            mUserNameLayout.setError("Cannot be Empty");
            check = false;
        }
        if (!TextUtils.isEmpty(mPhone.getText().toString()))
            if (!mainActivity.isValidPhone(mPhone.getText().toString())) {
                mPhoneLayout.setError("Invalid Phone Number");
                check = false;
            }
        return check;
    }

    private void toggleViews(boolean b) {
        mUserName.setEnabled(b);
        mPassword.setEnabled(b);
        mWebsite.setEnabled(b);
        mPhone.setEnabled(b);
        mName.setEnabled(b);
        mName.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mUserName.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mPassword.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mWebsite.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mPhone.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        if (b)
            mWebsite.requestFocus();
    }

    private void saveData() {
        if (checkValidation()) {
            String id = myRef.push().getKey();
            ArrayList<String> list = new ArrayList<>();
            String name = mName.getText().toString();
            String userName = mUserName.getText().toString().trim();
            String website = mWebsite.getText().toString().trim();
            String password = mPassword.getText().toString().trim();
            String phone = mPhone.getText().toString().trim();
            list.add(name);
            list.add(website);
            list.add(userName);
            list.add(password);
            list.add(phone);
            ArrayList<String> encryptData = null;
            try {
                encryptData = mainActivity.encryptData(list, id);
            } catch (Exception e) {
                e.printStackTrace();
            }
            encryptData.add(0, id);
            OnlineAccounts onlineAccountsModel = new OnlineAccounts(id, encryptData.get(1), encryptData.get(2), encryptData.get(3), encryptData.get(4), encryptData.get(5));
            myRef.child("Online Accounts").child(id).setValue(onlineAccountsModel);
            mainActivity.showAlertBox(getString(R.string.txt_Success),getString(R.string.txt_data_saved_success),2);
            mainActivity.goBack();
        }

    }

    private void updateData() {
        if (checkValidation()) {
            ArrayList<String> list = new ArrayList<>();
            String name = mName.getText().toString();
            String userName = mUserName.getText().toString().trim();
            String website = mWebsite.getText().toString().trim();
            String password = mPassword.getText().toString().trim();
            String phone = mPhone.getText().toString().trim();
            list.add(name);
            list.add(website);
            list.add(userName);
            list.add(password);
            list.add(phone);
            ArrayList<String> encryptData = null;
            try {
                encryptData = mainActivity.encryptData(list, uid);
            } catch (Exception e) {
                e.printStackTrace();
            }
            encryptData.add(0, uid);
            OnlineAccounts onlineAccounts = new OnlineAccounts(uid, encryptData.get(1), encryptData.get(2), encryptData.get(3), encryptData.get(4), encryptData.get(5));
            myRef.child("Online Accounts").child(uid).setValue(onlineAccounts);
            mainActivity.showAlertBox(getString(R.string.txt_Success),getString(R.string.txt_data_updated_success),2);
            mainActivity.goBack();
        }
    }

}
