package com.uds.mypa.SensitiveItems.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.uds.mypa.Activitys.MainActivity;
import com.uds.mypa.Interfaces.OnItemClickListener;
import com.uds.mypa.R;
import com.uds.mypa.SensitiveItems.adapters.PassportAdapter;
import com.uds.mypa.SensitiveItems.model.Passport;

import java.util.ArrayList;


public class PassportFragment extends Fragment implements OnItemClickListener {
    private ArrayList<Passport> arrayList;
    private MainActivity mainActivity;
    private FirebaseAnalytics mFirebaseAnalytics;
    private FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    private DatabaseReference reference = FirebaseDatabase.getInstance().getReference(user.getUid()).child("Passport");
    private ArrayList<Passport> list;
    private RecyclerView recyclerView;
    private ShimmerFrameLayout shimmerFrameLayout;
    private OnItemClickListener listener;
    private PassportAdapter adapter;
    private LottieAnimationView lottieAnimationView;

    public PassportFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_passport, container, false);
        mainActivity = (MainActivity) getActivity();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mainActivity);
        arrayList = new ArrayList<>();
        shimmerFrameLayout = view.findViewById(R.id.shimmer);
        listener = this;
        list = new ArrayList<>();
        recyclerView = view.findViewById(R.id.recyclerView);
        lottieAnimationView=view.findViewById(R.id.animation_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(mainActivity));
        FloatingActionButton fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.setUpFragment(3, null);
            }
        });
        recyclerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainActivity.toggleBottomSheet();
            }
        });
        shimmerFrameLayout.startShimmerAnimation();
        Toolbar mToolbar = view.findViewById(R.id.toolbar);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.goBack();
            }
        });
        return view;
    }

    @Override
    public void onItemClick(int position) {
        mainActivity.openBottomSheet(arrayList.get(position).getId(), "Passport", arrayList.get(position));
    }

    private void showData() {
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                list.clear();
                for (DataSnapshot passSnapshot : dataSnapshot.getChildren()) {
                    Passport passportModel = passSnapshot.getValue(Passport.class);
                    list.add(passportModel);
                }
                try {
                    arrayList = getDataList(list);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                adapter = null;
                try {
                    adapter = new PassportAdapter(arrayList, mainActivity, listener);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                shimmerFrameLayout.stopShimmerAnimation();
                shimmerFrameLayout.setVisibility(View.GONE);
                recyclerView.setAdapter(adapter);
                if (list.isEmpty()) {
                    lottieAnimationView.playAnimation();
                    lottieAnimationView.setVisibility(View.VISIBLE);
                }
                else {
                    lottieAnimationView.pauseAnimation();
                    lottieAnimationView.setVisibility(View.GONE);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(mainActivity, "Something went wrong, Please try again later.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private ArrayList<Passport> getDataList(ArrayList<Passport> arrayList) throws Exception {
        ArrayList<Passport> list = new ArrayList<>();
        for (int i = 0; i < arrayList.size(); i++) {
            Passport cuur = arrayList.get(i);
            String id = cuur.getId();
            String name = cuur.getName();
            String number = cuur.getPassportNumber();
            String expiryDate = cuur.getDateOfExpiry();
            String issueDate = cuur.getDateOfIssue();
            String placeOfIssue = cuur.getPlaceOfIssue();
            list.add(new Passport(id, mainActivity.decryptData(name, id), mainActivity.decryptData(number, id), mainActivity.decryptData(placeOfIssue, id), mainActivity.decryptData(issueDate, id), mainActivity.decryptData(expiryDate, id)));
        }
        return list;
    }


    @Override
    public void onStart() {
        super.onStart();
        showData();
        mainActivity.collapseBottomSheet();
    }
}
