package com.uds.mypa.SensitiveItems.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.uds.mypa.Interfaces.OnItemClickListener;
import com.uds.mypa.R;
import com.uds.mypa.SensitiveItems.model.BankingCard;

import java.util.ArrayList;

public class BankingCardAdapter extends RecyclerView.Adapter<BankingCardAdapter.ViewHolder> {
    private ArrayList<BankingCard> arrayList;
    private Context context;
    private OnItemClickListener onItemClickListener;
    private int[] cardId = {R.drawable.card1, R.drawable.card2, R.drawable.card3, R.drawable.card4, R.drawable.card5,
            R.drawable.card6, R.drawable.card7, R.drawable.card8, R.drawable.card9, R.drawable.card10};
    private int[] cardTypeImages = new int[]{R.drawable.visa2, R.drawable.masestro, R.drawable.master_card, R.drawable.rupay};

    public BankingCardAdapter(ArrayList<BankingCard> arrayList, Context context, OnItemClickListener onItemClickListener) {
        this.arrayList = arrayList;
        this.context = context;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.credit_debit_layout, parent, false);
        return new ViewHolder(view, onItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        BankingCard bankingCard = arrayList.get(position);
        holder.name.setText(bankingCard.getName());
        holder.number.setText(bankingCard.getNumber());
        holder.validThru.setText(bankingCard.getValidTo());
        holder.bankName.setText(bankingCard.getBankName());
        holder.bankLogoId.setImageResource(Integer.parseInt(bankingCard.getBankLogo()));
        holder.cardTypeId.setImageResource(cardTypeImages[Integer.parseInt(bankingCard.getCardTypeLogo())]);
        holder.cardView.setBackground(ContextCompat.getDrawable(context, cardId[bankingCard.getCardIndex()]));
    }



    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView name, number, bankName, validThru;
        ImageView bankLogoId, cardTypeId;
        ConstraintLayout cardView;
        OnItemClickListener onItemClickListener;

        public ViewHolder(@NonNull View itemView, OnItemClickListener onItemClickListener) {
            super(itemView);
            name = itemView.findViewById(R.id.name);
            number = itemView.findViewById(R.id.card_number);
            bankName = itemView.findViewById(R.id.bankName);
            bankLogoId = itemView.findViewById(R.id.bankLogo);
            cardTypeId = itemView.findViewById(R.id.cardType);
            validThru = itemView.findViewById(R.id.validThru);
            cardView = itemView.findViewById(R.id.card);
            this.onItemClickListener = onItemClickListener;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onItemClickListener.onItemClick(getAdapterPosition());
        }
    }
}
