package com.uds.mypa.SensitiveItems.fragment;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.uds.mypa.Activitys.MainActivity;
import com.uds.mypa.R;
import com.uds.mypa.SensitiveItems.model.Membership;
import com.uds.mypa.utils.Utils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewMembershipFragment extends Fragment implements DatePickerDialog.OnDateSetListener {
    private TextInputEditText  mName, mNumber,  mExpiryDate, mOrganisation;
    private TextInputLayout mOrganisationLayout,mNameLayout,mNumberLayout;
    private TextView mTitle;
    private MainActivity mainActivity;
    private FirebaseAnalytics mFirebaseAnalytics;
    private ImageView mExpiryDatePicker;
    private String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
    private DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(uid);
    private Boolean save = true;
    private int mYear,mMonth,mDay;

    public NewMembershipFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
        mainActivity.collapseBottomSheet();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view=inflater.inflate(R.layout.fragment_new_membership, container, false);
        myRef.keepSynced(true);
        initialize(view);
        mainActivity = (MainActivity) getActivity();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mainActivity);
        mTitle = view.findViewById(R.id.title_TextView);
        mTitle.setText("Add Membership Detail");
        view.findViewById(R.id.expiry_date_picker_actions).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             //   showDatePickerDialog(v);
            }
        });

        mExpiryDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c=Calendar.getInstance();
                mYear=c.get(Calendar.YEAR);
                mMonth=c.get(Calendar.MONTH);
                mDay=c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog=new DatePickerDialog(getContext(),
                        NewMembershipFragment.this,mYear,mMonth,mDay);
                datePickerDialog.show();
            }
        });
        Bundle bundle = getArguments();
        if (bundle != null) {
            Membership membership = bundle.getParcelable("membership");
            mName.setText(membership.getName());
            mOrganisation.setText(membership.getOrganization());
            mNumber.setText(membership.getNumber());
            mExpiryDate.setText(membership.getExpiry());
            uid = membership.getId();
            save = false;
            if (bundle.getInt("choose") == 0) {
                toggleViews(false);
            } else
                toggleViews(true);
        }
        FloatingActionButton saveFab=view.findViewById(R.id.save_fab);
        saveFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (save)
                    saveData();
                else
                    updateData();
            }
        });
        mExpiryDate.addTextChangedListener(new TextWatcher() {
            int prevl=0;
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int length = s.length();
                if ((prevl<length)&&(length==2||length==5)){
                    s.append("/");
                }

            }
        });
        return view;
    }

    private void toggleViews(boolean b) {
        mOrganisation.setEnabled(b);
        mExpiryDate.setEnabled(b);
        mExpiryDatePicker.setEnabled(b);
        mNumber.setEnabled(b);
        mName.setEnabled(b);
    }


    private void initialize(@NonNull View view) {
        mainActivity = (MainActivity) getActivity();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mainActivity);
        mExpiryDatePicker = view.findViewById(R.id.expiry_date_picker_actions);
        mTitle = view.findViewById(R.id.title_TextView);
        mOrganisation=view.findViewById(R.id.organisation_editText);
        mName = view.findViewById(R.id.name_editText);
        mNumber = view.findViewById(R.id.membership_number_editText);
        mExpiryDate = view.findViewById(R.id.expiryDate);
        mNameLayout=view.findViewById(R.id.name_layout);
        mOrganisationLayout=view.findViewById(R.id.organisation_layout);
        mNumberLayout=view.findViewById(R.id.number_layout);

    }
    private boolean checkValidation() {
        boolean check = true;
        if (TextUtils.isEmpty(mOrganisation.getText().toString())) {
            mOrganisationLayout.setError("Please Enter Organisation Name");
            check = false;
        }
        if (TextUtils.isEmpty(mName.getText().toString())) {
            mNameLayout.setError("Please Enter Name");
            check = false;
        }
        if (TextUtils.isEmpty(mNumber.getText().toString())) {
            mNumberLayout.setError("Please Enter Membership Number");
            check = false;
        }
        return check;
    }
    private void saveData() {
        if (checkValidation()) {
            String id = myRef.push().getKey();
            ArrayList<String> list = new ArrayList<>();
            String name = mName.getText().toString();
            String membershipNumber = mNumber.getText().toString().trim();
            String org = mOrganisation.getText().toString().trim();
            String expiry = mExpiryDate.getText().toString().trim();
            list.add(org);
            list.add(name);
            list.add(membershipNumber);
            list.add(expiry);
            ArrayList<String> encryptData = null;
            try {
                encryptData = mainActivity.encryptData(list, id);
            } catch (Exception e) {
                e.printStackTrace();
            }
            encryptData.add(0, id);
            Membership voterModel = new Membership(id, encryptData.get(1), encryptData.get(2), encryptData.get(3), encryptData.get(4));
            myRef.child("Membership").child(id).setValue(voterModel);
            mainActivity.showAlertBox(getString(R.string.txt_Success),getString(R.string.txt_data_saved_success),2);
            mainActivity.goBack();
        }
    }
    private void updateData() {
        if (checkValidation()) {
            ArrayList<String> list = new ArrayList<>();
            String name = mName.getText().toString();
            String membershipNumber = mNumber.getText().toString().trim();
            String org = mOrganisation.getText().toString().trim();
            String expiry = mExpiryDate.getText().toString().trim();
            list.add(org);
            list.add(name);
            list.add(membershipNumber);
            list.add(expiry);
            ArrayList<String> encryptData = null;
            try {
                encryptData = mainActivity.encryptData(list, uid);
            } catch (Exception e) {
                e.printStackTrace();
            }
            encryptData.add(0, uid);
            Membership voterModel = new Membership(uid, encryptData.get(1), encryptData.get(2), encryptData.get(3), encryptData.get(4));

            myRef.child("Membership").child(uid).setValue(voterModel);
            mainActivity.showAlertBox(getString(R.string.txt_Success),getString(R.string.txt_data_updated_success),2);
            mainActivity.goBack();
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String date = Utils.formatDate(dayOfMonth, month, year);
            try {
                if (!mainActivity.compareDate(date, null, true)) {
                    mExpiryDate.setText(date);
                } else {
                    mExpiryDate.setText("");
                    mainActivity.showAlertBox("Warning!", "Expiry Date Cannot be before today's date", 0);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
    }
}
