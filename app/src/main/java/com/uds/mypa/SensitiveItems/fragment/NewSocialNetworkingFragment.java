package com.uds.mypa.SensitiveItems.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.uds.mypa.Activitys.MainActivity;
import com.uds.mypa.R;
import com.uds.mypa.SensitiveItems.activity.CompanySelection;
import com.uds.mypa.SensitiveItems.model.SocialNetworking;

import java.util.ArrayList;


public class NewSocialNetworkingFragment extends Fragment {
    private TextInputLayout mNameLayout, mUserNameLayout, mPasswordLayout,mEmailLayout;
    private TextInputEditText mName, mUserName, mPassword, mQuestion, mAnswer, mRecoveryEmail;
    private TextView mTitle, mCompanyName;
    private ImageView mCompanyLogo;
    private MainActivity mainActivity;
    private FirebaseAnalytics mFirebaseAnalytics;
    private String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
    private DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(uid);
    private Boolean save = true;
    private LinearLayout mChooseCompany;
    private int companyLogoId;
    private String companyName;

    public NewSocialNetworkingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
        mainActivity.collapseBottomSheet();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_social_networking, container, false);
        myRef.keepSynced(true);
        initialize(view);
        mTitle.setText("Add Social Networking Detail");
        final FloatingActionButton saveFab, editFab;
        saveFab = view.findViewById(R.id.save_fab);
        editFab = view.findViewById(R.id.edit_fab);
        editFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleViews(true);
                editFab.setVisibility(View.GONE);
            }
        });
        Bundle bundle = getArguments();
        if (bundle != null) {
            SocialNetworking socialNetworking = bundle.getParcelable("socialNetworking");
            mName.setText(socialNetworking.getName());
            mUserName.setText(socialNetworking.getUserName());
            mPassword.setText(socialNetworking.getPassword());
            mQuestion.setText(socialNetworking.getQuestion());
            mAnswer.setText(socialNetworking.getAnswer());
            mRecoveryEmail.setText(socialNetworking.getRecoveryMail());
            mCompanyName.setText(socialNetworking.getCompany());
            mCompanyLogo.setImageResource(Integer.parseInt(socialNetworking.getImageId()));
            companyName=socialNetworking.getCompany();
            companyLogoId=Integer.parseInt(socialNetworking.getImageId());
            uid = socialNetworking.getId();
            save = false;
            if (bundle.getInt("choose") == 0) {
                toggleViews(false);
                editFab.setVisibility(View.VISIBLE);
            } else {
                toggleViews(true);
                editFab.setVisibility(View.GONE);
            }
        }

        saveFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (save)
                    saveData();
                else
                    updateData();
            }
        });
        mChooseCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getContext(), CompanySelection.class), 200);
            }
        });
        return view;
    }

    private void toggleViews(boolean value) {
        mUserName.setEnabled(value);
        mName.setEnabled(value);
        mPassword.setEnabled(value);
        mQuestion.setEnabled(value);
        mAnswer.setEnabled(value);
        mRecoveryEmail.setEnabled(value);
        mChooseCompany.setEnabled(value);
        mName.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mPassword.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mQuestion.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mUserName.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mAnswer.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        mRecoveryEmail.setTextColor(ContextCompat.getColor(getContext(), android.R.color.background_dark));
        if (value)
        mName.requestFocus();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == 200) {
            if (data != null) {
                companyLogoId = data.getIntExtra("image_id", 0);
                companyName = data.getStringExtra("company_name");
                mCompanyName.setText(companyName);
                mCompanyLogo.setImageResource(companyLogoId);
                mCompanyName.setTextColor(ContextCompat.getColor(getContext(), android.R.color.tab_indicator_text));
            }
        }
    }

    private void initialize(View view) {
        mainActivity = (MainActivity) getActivity();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mainActivity);
        mRecoveryEmail = view.findViewById(R.id.recovery_email_editText);
        mAnswer = view.findViewById(R.id.answer_editText);
        mQuestion = view.findViewById(R.id.question_editText);
        mTitle = view.findViewById(R.id.title_TextView);
        mName = view.findViewById(R.id.name_editText);
        mPassword = view.findViewById(R.id.password_editText);
        mUserName = view.findViewById(R.id.user_name_editText);
        mUserNameLayout = view.findViewById(R.id.user_name_layout);
        mNameLayout = view.findViewById(R.id.name_layout);
        mPasswordLayout = view.findViewById(R.id.password_layout);
        mChooseCompany = view.findViewById(R.id.chooseCompany);
        mCompanyName = view.findViewById(R.id.company_name);
        mCompanyLogo = view.findViewById(R.id.companyLogo);
        mEmailLayout = view.findViewById(R.id.email_layout);

    }

    private void saveData() {
        if (checkValidation()) {
            String id = myRef.push().getKey();
            ArrayList<String> list = new ArrayList<>();
            String name = mName.getText().toString();
            String ques = mQuestion.getText().toString().trim();
            String ans = mAnswer.getText().toString().trim();
            String password = mPassword.getText().toString().trim();
            String email = mRecoveryEmail.getText().toString().trim();
            String userName = mUserName.getText().toString().trim();
            String imageId = String.valueOf(companyLogoId);

            list.add(companyName);
            list.add(name);
            list.add(userName);
            list.add(password);
            list.add(ques);
            list.add(ans);
            list.add(email);
            list.add(imageId);
            ArrayList<String> encryptData = null;
            try {
                encryptData = mainActivity.encryptData(list, id);
            } catch (Exception e) {
                e.printStackTrace();
            }
            encryptData.add(0, id);
            SocialNetworking drivingLicenseModel = new SocialNetworking(id, encryptData.get(1), encryptData.get(2), encryptData.get(3), encryptData.get(4), encryptData.get(5), encryptData.get(6), encryptData.get(7), encryptData.get(8));
            myRef.child("Social Networking").child(id).setValue(drivingLicenseModel);
            mainActivity.showAlertBox(getString(R.string.txt_Success), getString(R.string.txt_data_saved_success), 2);
            mainActivity.goBack();
        }
    }

    private void updateData() {
        if (checkValidation()) {
            String id = myRef.push().getKey();
            ArrayList<String> list = new ArrayList<>();
            String name = mName.getText().toString();
            String ques = mQuestion.getText().toString().trim();
            String ans = mAnswer.getText().toString().trim();
            String password = mPassword.getText().toString().trim();
            String email = mRecoveryEmail.getText().toString().trim();
            String userName = mUserName.getText().toString().trim();
            String imageId = String.valueOf(companyLogoId);
            list.add(companyName);
            list.add(name);
            list.add(userName);
            list.add(password);
            list.add(ques);
            list.add(ans);
            list.add(email);
            list.add(imageId);
            ArrayList<String> encryptData = null;
            try {
                encryptData = mainActivity.encryptData(list, uid);
            } catch (Exception e) {
                e.printStackTrace();
            }
            encryptData.add(0, uid);
            SocialNetworking drivingLicenseModel = new SocialNetworking(uid, encryptData.get(1), encryptData.get(2), encryptData.get(3), encryptData.get(4), encryptData.get(5), encryptData.get(6), encryptData.get(7), encryptData.get(8));
            myRef.child("Social Networking").child(uid).setValue(drivingLicenseModel);
            mainActivity.showAlertBox(getString(R.string.txt_Success), getString(R.string.txt_data_updated_success), 2);
            mainActivity.goBack();
        }
    }

    private boolean checkValidation() {
        boolean check = true;
        if (TextUtils.isEmpty(mName.getText().toString())) {
            mNameLayout.setError("Cannot be Empty");
            check = false;
        }
        if (TextUtils.isEmpty(mUserName.getText().toString())) {
            mUserNameLayout.setError("Cannot be Empty");
            check = false;
        }
        if (TextUtils.isEmpty(mPassword.getText().toString())) {
            mPasswordLayout.setError("Cannot be Empty");
            check = false;
        }
        if (TextUtils.isEmpty(companyName)) {
            mainActivity.showAlertBox("Warning", "Please Select Company", 0);
            check = false;
        }
        if (!TextUtils.isEmpty(mRecoveryEmail.getText().toString()))
            if (!mainActivity.isValidEmail(mRecoveryEmail.getText().toString())) {
                mEmailLayout.setError("Invalid Email Address");
                check = false;
            }
        return check;
    }
}
