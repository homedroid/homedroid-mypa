package com.uds.mypa.SensitiveItems.model;

public class CurrentAccounts {
    private String bankName,accountNumber,ifscCode,companyID,eStatementPassword1,eStatementPassword2,netBankingID,netBankingPassword,mPin,vpa;

    public CurrentAccounts() {
    }

    public CurrentAccounts(String bankName, String accountNumber, String ifscCode, String companyID, String eStatementPassword1, String eStatementPassword2, String netBankingID, String netBankingPassword, String mPin, String vpa) {
        this.bankName = bankName;
        this.accountNumber = accountNumber;
        this.ifscCode = ifscCode;
        this.companyID = companyID;
        this.eStatementPassword1 = eStatementPassword1;
        this.eStatementPassword2 = eStatementPassword2;
        this.netBankingID = netBankingID;
        this.netBankingPassword = netBankingPassword;
        this.mPin = mPin;
        this.vpa=vpa;
    }

    public String getBankName() {
        return bankName;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getIfscCode() {
        return ifscCode;
    }

    public String getCompanyID() {
        return companyID;
    }

    public String geteStatementPassword1() {
        return eStatementPassword1;
    }

    public String geteStatementPassword2() {
        return eStatementPassword2;
    }

    public String getNetBankingID() {
        return netBankingID;
    }

    public String getNetBankingPassword() {
        return netBankingPassword;
    }

    public String getmPin() {
        return mPin;
    }

    public String getVpa() {
        return vpa;
    }
}
