package com.uds.mypa.SensitiveItems.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.uds.mypa.Activitys.MainActivity;
import com.uds.mypa.R;
import com.uds.mypa.SensitiveItems.activity.OperatingSystemSelection;
import com.uds.mypa.SensitiveItems.model.SystemLogin;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewSystemLoginFragment extends Fragment {
    private TextInputLayout mSystemNameLayout, mUserNameLayout, mPasswordLayout;
    private TextInputEditText mSystemName, mUserName, mPassword, mHint;
    private TextView mTitle, mOsName;
    private ImageView mSystemLogo;
    private MainActivity mainActivity;
    private FirebaseAnalytics mFirebaseAnalytics;
    private String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
    private DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(uid);
    private Boolean save = true;
    private LinearLayout mSelectOs;
    private int osLogoId;
    private String osName;

    public NewSystemLoginFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
        mainActivity.collapseBottomSheet();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_system_login, container, false);
        myRef.keepSynced(true);
        initialize(view);
        mTitle.setText("Add System Login Detail");
        final FloatingActionButton saveFab, editFab;
        saveFab = view.findViewById(R.id.save_fab);
        editFab = view.findViewById(R.id.edit_fab);
        editFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleViews(true);
                editFab.setVisibility(View.GONE);
            }
        });
        Bundle bundle = getArguments();
        if (bundle != null) {
            SystemLogin systemLogin = bundle.getParcelable("systemLogin");
            mSystemName.setText(systemLogin.getSystemName());
            mUserName.setText(systemLogin.getUserName());
            mPassword.setText(systemLogin.getPassword());
            mHint.setText(systemLogin.getHint());
            mOsName.setText(systemLogin.getOsName());
            mSystemLogo.setImageResource(Integer.parseInt(systemLogin.getImageId()));
            osName = systemLogin.getOsName();
            osLogoId = Integer.parseInt(systemLogin.getImageId());
            uid = systemLogin.getId();
            save = false;
            if (bundle.getInt("choose") == 0) {
                toggleViews(false);
                editFab.setVisibility(View.VISIBLE);
            } else {
                toggleViews(true);
                editFab.setVisibility(View.GONE);
            }
        }

        saveFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (save)
                    saveData();
                else
                    updateData();
            }
        });
        mSelectOs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(mainActivity, OperatingSystemSelection.class), 300);
            }
        });
        return view;
    }

    private void initialize(View view) {
        mainActivity = (MainActivity) getActivity();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mainActivity);
        mHint = view.findViewById(R.id.hint_editText);
        mTitle = view.findViewById(R.id.title_TextView);
        mSystemName = view.findViewById(R.id.system_name_editText);
        mPassword = view.findViewById(R.id.password_editText);
        mUserName = view.findViewById(R.id.user_name_editText);
        mUserNameLayout = view.findViewById(R.id.user_name_layout);
        mSystemNameLayout = view.findViewById(R.id.name_layout);
        mPasswordLayout = view.findViewById(R.id.password_layout);
        mSelectOs = view.findViewById(R.id.selectOs);
        mOsName = view.findViewById(R.id.os_name);
        mSystemLogo = view.findViewById(R.id.osLogo);

    }

    private void toggleViews(boolean value) {
        mUserName.setEnabled(value);
        mSystemName.setEnabled(value);
        mPassword.setEnabled(value);
        mHint.setEnabled(value);
        mSelectOs.setEnabled(value);
        mSystemName.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        mPassword.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        mUserName.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        mHint.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        mOsName.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        mSystemName.requestFocus();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == 300) {
            if (data != null) {
                osLogoId = data.getIntExtra("image_id", 0);
                osName = data.getStringExtra("os_name");
                mOsName.setText(osName);
                mSystemLogo.setImageResource(osLogoId);
                mOsName.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.tab_indicator_text));
            }
        }
    }

    private void saveData() {
        if (checkValidation()) {
            String id = myRef.push().getKey();
            ArrayList<String> list = new ArrayList<>();
            String hint = mHint.getText().toString().trim();
            String password = mPassword.getText().toString().trim();
            String userName = mUserName.getText().toString().trim();
            list.add(osName);
            list.add(userName);
            list.add(password);
            list.add(hint);
            list.add(osName);
            list.add(String.valueOf(osLogoId));
            ArrayList<String> encryptData = null;
            try {
                encryptData = mainActivity.encryptData(list, id);
            } catch (Exception e) {
                e.printStackTrace();
            }
            encryptData.add(0, id);
            SystemLogin systemModel = new SystemLogin(id, encryptData.get(1), encryptData.get(2), encryptData.get(3), encryptData.get(4), encryptData.get(5), encryptData.get(6));
            myRef.child("System Login").child(id).setValue(systemModel);
            mainActivity.showAlertBox(getString(R.string.txt_Success), getString(R.string.txt_data_updated_success), 2);
            mainActivity.goBack();
        }
    }

    private void updateData() {
        if (checkValidation()) {
            ArrayList<String> list = new ArrayList<>();
            String hint = mHint.getText().toString().trim();
            String password = mPassword.getText().toString().trim();
            String userName = mUserName.getText().toString().trim();
            list.add(osName);
            list.add(userName);
            list.add(password);
            list.add(hint);
            list.add(osName);
            list.add(String.valueOf(osLogoId));
            ArrayList<String> encryptData = null;
            try {
                encryptData = mainActivity.encryptData(list, uid);
            } catch (Exception e) {
                e.printStackTrace();
            }
            encryptData.add(0, uid);
            SystemLogin systemModel = new SystemLogin(uid, encryptData.get(1), encryptData.get(2), encryptData.get(3), encryptData.get(4), encryptData.get(5), encryptData.get(6));
            myRef.child("System Login").child(uid).setValue(systemModel);
            mainActivity.showAlertBox(getString(R.string.txt_Success), getString(R.string.txt_data_updated_success), 2);
            mainActivity.goBack();
        }
    }

    private boolean checkValidation() {
        boolean check = true;
        if (TextUtils.isEmpty(mSystemName.getText().toString())) {
            mSystemNameLayout.setError("Cannot be Empty");
            check = false;
        }
        if (TextUtils.isEmpty(mUserName.getText().toString())) {
            mUserNameLayout.setError("Cannot be Empty");
            check = false;
        }
        if (TextUtils.isEmpty(mPassword.getText().toString())) {
            mPasswordLayout.setError("Cannot be Empty");
            check = false;
        }
        if (TextUtils.isEmpty(osName)) {
            mainActivity.showAlertBox("Warning", "Please Select Company", 0);
            check = false;
        }
        return check;
    }

}
