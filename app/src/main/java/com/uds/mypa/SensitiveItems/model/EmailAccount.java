package com.uds.mypa.SensitiveItems.model;

public class EmailAccount {
    private String email,password;

    public EmailAccount() {
    }

    public EmailAccount(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
