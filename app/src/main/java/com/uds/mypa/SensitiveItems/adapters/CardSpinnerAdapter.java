package com.uds.mypa.SensitiveItems.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.uds.mypa.R;

public class CardSpinnerAdapter extends BaseAdapter {
    private Context context;
    private String[] typeName;
    private int[] cardTypeImages= new int[]{R.drawable.visa_card, R.drawable.masestro, R.drawable.master_card, R.drawable.rupay};

    public CardSpinnerAdapter(Context context, String[] typeName, int[] cardTypeImages) {
        this.context = context;
        this.typeName = typeName;
    }

    @Override
    public int getCount() {
        return typeName.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = LayoutInflater.from(context).inflate(R.layout.custom_card_spinner_layout, null);
        ImageView icon =  view.findViewById(R.id.imageView);
        TextView names =  view.findViewById(R.id.textView);
        icon.setImageResource(cardTypeImages[i]);
        names.setText(typeName[i]);
        return view;
    }
}
