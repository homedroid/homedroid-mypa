package com.uds.mypa.SensitiveItems.model;

import android.os.Parcel;
import android.os.Parcelable;

public class SystemLogin implements Parcelable {
    private String id,systemName,userName,password,hint,osName,imageId;

    public SystemLogin() {
    }

    public SystemLogin(String id, String systemName, String userName, String password, String hint, String osName, String imageId) {
        this.id = id;
        this.systemName = systemName;
        this.userName = userName;
        this.password = password;
        this.hint = hint;
        this.osName = osName;
        this.imageId = imageId;
    }

    protected SystemLogin(Parcel in) {
        id = in.readString();
        systemName = in.readString();
        userName = in.readString();
        password = in.readString();
        hint = in.readString();
        osName = in.readString();
        imageId = in.readString();
    }

    public static final Creator<SystemLogin> CREATOR = new Creator<SystemLogin>() {
        @Override
        public SystemLogin createFromParcel(Parcel in) {
            return new SystemLogin(in);
        }

        @Override
        public SystemLogin[] newArray(int size) {
            return new SystemLogin[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getSystemName() {
        return systemName;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public String getHint() {
        return hint;
    }

    public String getOsName() {
        return osName;
    }

    public String getImageId() {
        return imageId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(systemName);
        dest.writeString(userName);
        dest.writeString(password);
        dest.writeString(hint);
        dest.writeString(osName);
        dest.writeString(imageId);
    }
}
