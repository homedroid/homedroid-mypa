package com.uds.mypa.SensitiveItems.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Email implements Parcelable {
    private String id,name,password,email,recoveryEmail,phone;

    public Email(String id, String name, String password, String email, String recoveryEmail, String phone) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.email = email;
        this.recoveryEmail = recoveryEmail;
        this.phone = phone;
    }

    public Email() {
    }

    protected Email(Parcel in) {
        id = in.readString();
        name = in.readString();
        password = in.readString();
        email = in.readString();
        recoveryEmail = in.readString();
        phone = in.readString();
    }

    public static final Creator<Email> CREATOR = new Creator<Email>() {
        @Override
        public Email createFromParcel(Parcel in) {
            return new Email(in);
        }

        @Override
        public Email[] newArray(int size) {
            return new Email[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public String getRecoveryEmail() {
        return recoveryEmail;
    }

    public String getPhone() {
        return phone;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(password);
        dest.writeString(email);
        dest.writeString(recoveryEmail);
        dest.writeString(phone);
    }
}
