package com.uds.mypa.SensitiveItems.model;

import android.os.Parcel;
import android.os.Parcelable;

public class Insurance implements Parcelable {
    private String id,name,companyName,plan,policyName,number,duration,premium,frequency,dueDate;

    public Insurance(String id, String name, String companyName, String plan, String policyName, String number, String duration, String premium, String frequency, String dueDate) {
        this.id = id;
        this.name = name;
        this.companyName = companyName;
        this.plan = plan;
        this.policyName = policyName;
        this.number = number;
        this.duration = duration;
        this.premium = premium;
        this.frequency = frequency;
        this.dueDate = dueDate;
    }

    public Insurance() {
    }

    protected Insurance(Parcel in) {
        id = in.readString();
        name = in.readString();
        companyName = in.readString();
        plan = in.readString();
        policyName = in.readString();
        number = in.readString();
        duration = in.readString();
        premium = in.readString();
        frequency = in.readString();
        dueDate = in.readString();
    }

    public static final Creator<Insurance> CREATOR = new Creator<Insurance>() {
        @Override
        public Insurance createFromParcel(Parcel in) {
            return new Insurance(in);
        }

        @Override
        public Insurance[] newArray(int size) {
            return new Insurance[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getPlan() {
        return plan;
    }

    public String getPolicyName() {
        return policyName;
    }

    public String getNumber() {
        return number;
    }

    public String getDuration() {
        return duration;
    }

    public String getPremium() {
        return premium;
    }

    public String getFrequency() {
        return frequency;
    }

    public String getDueDate() {
        return dueDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(companyName);
        dest.writeString(plan);
        dest.writeString(policyName);
        dest.writeString(number);
        dest.writeString(duration);
        dest.writeString(premium);
        dest.writeString(frequency);
        dest.writeString(dueDate);
    }
}
