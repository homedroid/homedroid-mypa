package com.uds.mypa.SensitiveItems.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.uds.mypa.Activitys.MainActivity;
import com.uds.mypa.R;
import com.uds.mypa.SensitiveItems.activity.BankSelectionActivity;
import com.uds.mypa.SensitiveItems.model.BankAccount;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewBankAccountFragment extends Fragment {
    private TextInputLayout mNameLayout, mNumberLayout;
    private TextView mBankName, mTitle;
    private EditText mAccountNumber, mName, mIFSC, mNomineeName, mNomineeRelation, mNetBankingID, mNetBankingPassword, mProfilePassword;
    private ImageView mBankLogo;
    private String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
    private DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(uid);
    private MainActivity mainActivity;
    private FirebaseAnalytics mFirebaseAnalytics;
    private String bankName = "";
    ;
    private Boolean save = true;
    private LinearLayout selectBank;
    private int bankLogoId;

    public NewBankAccountFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
        mainActivity.collapseBottomSheet();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_bank_account, container, false);
        initialize(view);
        mTitle.setText("Add Bank Account Detail");
        final FloatingActionButton saveFab, editFab;
        saveFab = view.findViewById(R.id.save_fab);
        editFab = view.findViewById(R.id.edit_fab);
        editFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleViews(true);
                editFab.setVisibility(View.GONE);
            }
        });
        saveFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (save)
                    saveData();
                else
                    updateData();
            }
        });
        Bundle bundle = getArguments();
        if (bundle != null) {
            BankAccount bankAccount = bundle.getParcelable("bankAccount");
            mName.setText(bankAccount.getName());
            mAccountNumber.setText(bankAccount.getNumber());
            mIFSC.setText(bankAccount.getIfscCode());
            mNomineeName.setText(bankAccount.getNominee());
            mNomineeRelation.setText(bankAccount.getRelation());
            mNetBankingID.setText(bankAccount.getNetbankingId());
            mNetBankingPassword.setText(bankAccount.getNetbankingPassword());
            mBankName.setText(bankAccount.getBankName());
            mBankLogo.setImageResource(Integer.parseInt(bankAccount.getImageId()));
            uid = bankAccount.getId();

            save = false;

            if (bundle.getInt("choose") == 0) {
                toggleViews(false);
                editFab.setVisibility(View.VISIBLE);
            } else {
                toggleViews(true);
                editFab.setVisibility(View.GONE);
            }
        }

        selectBank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(mainActivity, BankSelectionActivity.class), 100);
            }
        });
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (requestCode == 100) {
            if (data != null) {
                bankLogoId = data.getIntExtra("image_id", 0);
                bankName = data.getStringExtra("bank_name");
                mBankName.setText(bankName);
                mBankLogo.setImageResource(bankLogoId);
                mBankName.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.tab_indicator_text));
            }
        }
    }

    private void toggleViews(boolean value) {
        selectBank.setEnabled(value);
        mName.setEnabled(value);
        mAccountNumber.setEnabled(value);
        mIFSC.setEnabled(value);
        mNomineeName.setEnabled(value);
        mNomineeRelation.setEnabled(value);
        mNetBankingID.setEnabled(value);
        mNetBankingPassword.setEnabled(value);
        mProfilePassword.setEnabled(value);
        mName.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        mAccountNumber.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        mIFSC.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        mNomineeName.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        mNomineeRelation.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        mNetBankingID.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        mNetBankingPassword.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        mProfilePassword.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        if (value)
            mAccountNumber.requestFocus();
    }

    private void initialize(View view) {
        mainActivity = (MainActivity) getActivity();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mainActivity);
        mTitle = view.findViewById(R.id.title_TextView);
        mBankName = view.findViewById(R.id.bank_name_textView);
        mAccountNumber = view.findViewById(R.id.account_number_editText);
        mName = view.findViewById(R.id.name_editText);
        mIFSC = view.findViewById(R.id.ifsc_editText);
        mNomineeName = view.findViewById(R.id.nominee_editText);
        mNomineeRelation = view.findViewById(R.id.nominee_relation_editText);
        mNetBankingID = view.findViewById(R.id.netbanking_id_editText);
        mNetBankingPassword = view.findViewById(R.id.netbanking_password_editText);
        mNameLayout = view.findViewById(R.id.name_layout);
        mNumberLayout = view.findViewById(R.id.number_layout);
        mBankLogo = view.findViewById(R.id.bankLogo);
        mProfilePassword = view.findViewById(R.id.profile_password_editText);
        selectBank = view.findViewById(R.id.selectBank);

    }

    private void saveData() {
        if (checkValidation()) {
            String id = myRef.push().getKey();
            ArrayList<String> list = new ArrayList<>();
            String name, number, code, nominee, relation, password;
            name = mName.getText().toString();
            number = mAccountNumber.getText().toString().trim();
            code = mIFSC.getText().toString().trim();
            nominee = mNomineeName.getText().toString().trim();
            relation = mNomineeRelation.getText().toString().trim();
            password = mProfilePassword.getText().toString().trim();
            String imageId = String.valueOf(bankLogoId);
            String netBankingId = mNetBankingID.getText().toString().trim();
            String netBankingPassword = mNetBankingPassword.getText().toString().trim();
            list.add(name);
            list.add(number);
            list.add(code);
            list.add(nominee);
            list.add(relation);
            list.add(password);
            list.add(bankName);
            list.add(imageId);
            list.add(netBankingId);
            list.add(netBankingPassword);
            ArrayList<String> encryptData = null;
            try {
                encryptData = mainActivity.encryptData(list, id);
            } catch (Exception e) {
                e.printStackTrace();
            }
            encryptData.add(0, id);
            BankAccount bankAccountsModel = new BankAccount(id, encryptData.get(1), encryptData.get(2), encryptData.get(3), encryptData.get(4), encryptData.get(5), encryptData.get(6), encryptData.get(7), encryptData.get(8), encryptData.get(9), encryptData.get(10));
            myRef.child("Bank Accounts").child(id).setValue(bankAccountsModel);
            mainActivity.showAlertBox(getString(R.string.txt_Success), getString(R.string.txt_data_saved_success), 2);
            mainActivity.goBack();
        }
    }

    private void updateData() {
        if (checkValidation()) {
            ArrayList<String> list = new ArrayList<>();
            String name, number, code, nominee, relation, password;
            name = mName.getText().toString();
            number = mAccountNumber.getText().toString().trim();
            code = mIFSC.getText().toString().trim();
            nominee = mNomineeName.getText().toString().trim();
            relation = mNomineeRelation.getText().toString().trim();
            password = mProfilePassword.getText().toString().trim();
            String imageId = String.valueOf(bankLogoId);
            String netBankingId = mNetBankingID.getText().toString().trim();
            String netBankingPassword = mNetBankingPassword.getText().toString().trim();
            list.add(name);
            list.add(number);
            list.add(code);
            list.add(nominee);
            list.add(relation);
            list.add(password);
            list.add(bankName);
            list.add(imageId);
            list.add(netBankingId);
            list.add(netBankingPassword);
            ArrayList<String> encryptData = null;
            try {
                encryptData = mainActivity.encryptData(list, uid);
            } catch (Exception e) {
                e.printStackTrace();
            }
            encryptData.add(0, uid);
            BankAccount bankAccountsModel = new BankAccount(uid, encryptData.get(1), encryptData.get(2), encryptData.get(3), encryptData.get(4), encryptData.get(5), encryptData.get(6), encryptData.get(7), encryptData.get(8), encryptData.get(9), encryptData.get(10));
            myRef.child("Bank Accounts").child(uid).setValue(bankAccountsModel);
            mainActivity.showAlertBox(getString(R.string.txt_Success), getString(R.string.txt_data_updated_success), 2);
            mainActivity.goBack();
        }
    }

    private boolean checkValidation() {
        boolean check = true;
        if (TextUtils.isEmpty(mName.getText().toString())) {
            mNameLayout.setError("Please Enter Account Holder Name");
            check = false;
        }
        if (TextUtils.isEmpty(mAccountNumber.getText().toString())) {
            mNumberLayout.setError("Please Enter Account Number");
            check = false;
        }
        if (TextUtils.isEmpty(bankName)) {
            mainActivity.showAlertBox("Warning", "Please Select Bank", 0);
            check = false;
        }
        return check;
    }

}
