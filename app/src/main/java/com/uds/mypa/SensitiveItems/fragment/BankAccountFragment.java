package com.uds.mypa.SensitiveItems.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.uds.mypa.Activitys.MainActivity;
import com.uds.mypa.Interfaces.OnItemClickListener;
import com.uds.mypa.R;
import com.uds.mypa.SensitiveItems.adapters.BankAccountAdapter;
import com.uds.mypa.SensitiveItems.model.BankAccount;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class BankAccountFragment extends Fragment implements OnItemClickListener {
    private ArrayList<BankAccount> arrayList;
    private MainActivity mainActivity;
    private FirebaseAnalytics mFirebaseAnalytics;
    private FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    private DatabaseReference reference = FirebaseDatabase.getInstance().getReference(user.getUid()).child("Bank Accounts");
    private ArrayList<BankAccount> list;
    private RecyclerView recyclerView;
    private ShimmerFrameLayout shimmerFrameLayout;
    private OnItemClickListener listener;
    private BankAccountAdapter adapter;
    private LottieAnimationView lottieAnimationView;
    public BankAccountFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_bank_account, container, false);
        mainActivity = (MainActivity) getActivity();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mainActivity);
        arrayList = new ArrayList<>();
        shimmerFrameLayout = view.findViewById(R.id.shimmer);
        listener = this;
        list = new ArrayList<>();
        recyclerView = view.findViewById(R.id.recyclerView);
        lottieAnimationView=view.findViewById(R.id.animation_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(mainActivity));
        FloatingActionButton fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.setUpFragment(15, null);
            }
        });
        recyclerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainActivity.toggleBottomSheet();
            }
        });
        shimmerFrameLayout.startShimmerAnimation();
        Toolbar mToolbar = view.findViewById(R.id.toolbar);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.goBack();
            }
        });
        return view;
    }
    private void showData() {
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                list.clear();
                for (DataSnapshot passSnapshot : dataSnapshot.getChildren()) {
                    BankAccount bankAccount = passSnapshot.getValue(BankAccount.class);
                    list.add(bankAccount);
                }
                try {
                    arrayList = getDataList(list);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                adapter = null;
                try {
                    adapter = new BankAccountAdapter(arrayList, mainActivity, listener);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                shimmerFrameLayout.stopShimmerAnimation();
                shimmerFrameLayout.setVisibility(View.GONE);
                recyclerView.setAdapter(adapter);
                if (list.isEmpty()) {
                    lottieAnimationView.playAnimation();
                    lottieAnimationView.setVisibility(View.VISIBLE);
                }
                else {
                    lottieAnimationView.pauseAnimation();
                    lottieAnimationView.setVisibility(View.GONE);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(mainActivity, "Something went wrong, Please try again later.", Toast.LENGTH_SHORT).show();
            }
        });
    }
    private ArrayList<BankAccount> getDataList(ArrayList<BankAccount> arrayList) throws Exception{
        ArrayList<BankAccount> list = new ArrayList<>();
        for (int i = 0; i < arrayList.size(); i++) {
            BankAccount cuur = arrayList.get(i);
            final String id, name, number, bankName, nominee, relation, password,imageId,netBankingId,netBankingPassword;
            final String code = cuur.getIfscCode();
            id = cuur.getId();
            name = cuur.getName();
            number = cuur.getNumber();
            bankName = cuur.getBankName();
            nominee = cuur.getNominee();
            relation = cuur.getRelation();
            password = cuur.getPassword();
            imageId=cuur.getImageId();
            netBankingId=cuur.getNetbankingId();
            netBankingPassword=cuur.getNetbankingPassword();
            list.add(new BankAccount(id,mainActivity.decryptData(name,id),mainActivity.decryptData(number,id),mainActivity.decryptData(code,id),mainActivity.decryptData(nominee,id),mainActivity.decryptData(relation,id),mainActivity.decryptData(password,id),mainActivity.decryptData(bankName,id),mainActivity.decryptData(imageId,id),mainActivity.decryptData(netBankingId,id),mainActivity.decryptData(netBankingPassword,id)));
        }
        return list;
    }

    @Override
    public void onItemClick(int position) {
        mainActivity.openBottomSheet(arrayList.get(position).getId(), "Bank Accounts", arrayList.get(position));
    }

    @Override
    public void onStart() {
        super.onStart();
        showData();
        mainActivity.collapseBottomSheet();
    }
}
