package com.uds.mypa.SensitiveItems.model;

public class AmazonBusinessAccount {
    private String id,password;

    public AmazonBusinessAccount() {
    }

    public AmazonBusinessAccount(String id, String password) {
        this.id = id;
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public String getPassword() {
        return password;
    }
}
