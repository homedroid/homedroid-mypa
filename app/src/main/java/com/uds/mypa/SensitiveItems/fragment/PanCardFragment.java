package com.uds.mypa.SensitiveItems.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.uds.mypa.Activitys.MainActivity;
import com.uds.mypa.Interfaces.OnItemClickListener;
import com.uds.mypa.R;
import com.uds.mypa.SensitiveItems.adapters.PanAdapter;
import com.uds.mypa.SensitiveItems.model.PanCard;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class PanCardFragment extends Fragment implements OnItemClickListener{
    private ArrayList<PanCard> arrayList;
    private MainActivity mainActivity;
    private FirebaseAnalytics mFirebaseAnalytics;
    private FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    private DatabaseReference reference = FirebaseDatabase.getInstance().getReference(user.getUid()).child("Pan Card");
    private ArrayList<PanCard> list;
    private RecyclerView recyclerView;
    private OnItemClickListener listener;
    private PanAdapter adapter;
    private ShimmerFrameLayout shimmerFrameLayout;
    private LottieAnimationView lottieAnimationView;
    public PanCardFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view= inflater.inflate(R.layout.fragment_pan_card, container, false);
        mainActivity = (MainActivity) getActivity();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mainActivity);
        arrayList = new ArrayList<>();
        listener = this;
        shimmerFrameLayout = view.findViewById(R.id.shimmer);
        shimmerFrameLayout.startShimmerAnimation();
        list = new ArrayList<>();
        recyclerView = view.findViewById(R.id.recyclerView);
        lottieAnimationView=view.findViewById(R.id.animation_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(mainActivity));
        FloatingActionButton fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.setUpFragment(7, null);
            }
        });
        recyclerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainActivity.toggleBottomSheet();
            }
        });
        Toolbar mToolbar = view.findViewById(R.id.toolbar);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.goBack();
            }
        });
        return view;
    }
    private void showData() {

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                list.clear();
                for (DataSnapshot passSnapshot : dataSnapshot.getChildren()) {
                    PanCard panModel = passSnapshot.getValue(PanCard.class);
                    list.add(panModel);
                }
                try {
                    arrayList=getDataList(list);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                adapter = null;
                try {
                    adapter = new PanAdapter(arrayList, mainActivity,listener);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                shimmerFrameLayout.stopShimmerAnimation();
                shimmerFrameLayout.setVisibility(View.GONE);
                recyclerView.setAdapter(adapter);
                if (list.isEmpty()) {
                    lottieAnimationView.playAnimation();
                    lottieAnimationView.setVisibility(View.VISIBLE);
                }
                else {
                    lottieAnimationView.pauseAnimation();
                    lottieAnimationView.setVisibility(View.GONE);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    private ArrayList<PanCard> getDataList(ArrayList<PanCard> arrayList) throws Exception{
        ArrayList<PanCard> list = new ArrayList<>();
        for (int i = 0; i < arrayList.size(); i++) {
            PanCard cuur = arrayList.get(i);
            String id = cuur.getId();
            String name=cuur.getName();
            String number=cuur.getPanNumber();
            String fathersName=cuur.getFathersName();
            String dob=cuur.getDob();
            list.add(new PanCard(id,mainActivity.decryptData(name,id),mainActivity.decryptData(number,id),mainActivity.decryptData(fathersName,id),mainActivity.decryptData(dob,id)));
        }
        return list;
    }


    @Override
    public void onItemClick(int position) {
        mainActivity.openBottomSheet(arrayList.get(position).getId(), "Pan Card", arrayList.get(position));
    }
    @Override
    public void onStart() {
        super.onStart();
        showData();
        mainActivity.collapseBottomSheet();
    }
}
