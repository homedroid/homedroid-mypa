package com.uds.mypa.SensitiveItems.model;

import android.os.Parcel;
import android.os.Parcelable;

public class GasConnection implements Parcelable {
    private String id,number,name,address,userName,password,phone,email;

    public GasConnection(String id, String number, String name, String address, String userName, String password, String phone, String email) {
        this.id = id;
        this.number = number;
        this.name = name;
        this.address = address;
        this.userName = userName;
        this.password = password;
        this.phone = phone;
        this.email = email;
    }

    public GasConnection() {
    }

    protected GasConnection(Parcel in) {
        id = in.readString();
        number = in.readString();
        name = in.readString();
        address = in.readString();
        userName = in.readString();
        password = in.readString();
        phone = in.readString();
        email = in.readString();
    }

    public static final Creator<GasConnection> CREATOR = new Creator<GasConnection>() {
        @Override
        public GasConnection createFromParcel(Parcel in) {
            return new GasConnection(in);
        }

        @Override
        public GasConnection[] newArray(int size) {
            return new GasConnection[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public String getPhone() {
        return phone;
    }

    public String getEmail() {
        return email;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(number);
        dest.writeString(name);
        dest.writeString(address);
        dest.writeString(userName);
        dest.writeString(password);
        dest.writeString(phone);
        dest.writeString(email);
    }
}
