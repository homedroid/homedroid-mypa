package com.uds.mypa.SensitiveItems.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.airbnb.lottie.LottieAnimationView;
import com.facebook.shimmer.ShimmerFrameLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.uds.mypa.Activitys.MainActivity;
import com.uds.mypa.Interfaces.OnItemClickListener;
import com.uds.mypa.R;
import com.uds.mypa.SensitiveItems.adapters.CompanyProfileAdapter;
import com.uds.mypa.SensitiveItems.adapters.PassportAdapter;
import com.uds.mypa.SensitiveItems.model.AmazonBusinessAccount;
import com.uds.mypa.SensitiveItems.model.Aws;
import com.uds.mypa.SensitiveItems.model.CompanyProfile;
import com.uds.mypa.SensitiveItems.model.CurrentAccounts;
import com.uds.mypa.SensitiveItems.model.DomainAccount;
import com.uds.mypa.SensitiveItems.model.EmailAccount;
import com.uds.mypa.SensitiveItems.model.Gst;
import com.uds.mypa.SensitiveItems.model.Legal;
import com.uds.mypa.SensitiveItems.model.Passport;
import com.uds.mypa.SensitiveItems.model.RailwayAccount;
import com.uds.mypa.SensitiveItems.model.WorkplaceByFacebook;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class CompanyProfileFragment extends Fragment implements OnItemClickListener {
    private MainActivity mainActivity;
    private FirebaseAnalytics mFirebaseAnalytics;
    private ArrayList<CompanyProfile> arrayList;
    private FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    private DatabaseReference reference = FirebaseDatabase.getInstance().getReference(user.getUid()).child("Company Profile");
    private ArrayList<CompanyProfile> list;
    private RecyclerView recyclerView;
    private ShimmerFrameLayout shimmerFrameLayout;
    private OnItemClickListener listener;
    private CompanyProfileAdapter adapter;
    private LottieAnimationView lottieAnimationView;

    public CompanyProfileFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_company_profile, container, false);
        mainActivity = (MainActivity) getActivity();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mainActivity);
        arrayList = new ArrayList<>();
        shimmerFrameLayout = view.findViewById(R.id.shimmer);
        listener = this;
        list = new ArrayList<>();
        recyclerView = view.findViewById(R.id.recyclerView);
        lottieAnimationView = view.findViewById(R.id.animation_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        FloatingActionButton fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.setUpFragment(37, null);
            }
        });
        recyclerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mainActivity.toggleBottomSheet();
            }
        });
        shimmerFrameLayout.startShimmerAnimation();

        Toolbar mToolbar = view.findViewById(R.id.toolbar);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mainActivity.goBack();
            }
        });
        return view;
    }

    @Override
    public void onItemClick(int position) {
        mainActivity.openBottomSheet(arrayList.get(position).getId(), "Company Profile", arrayList.get(position));
    }

    @Override
    public void onStart() {
        super.onStart();
        showData();
    }

    private void showData() {
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                list.clear();
                for (DataSnapshot passSnapshot : dataSnapshot.getChildren()) {
                    CompanyProfile companyProfile = passSnapshot.getValue(CompanyProfile.class);
                    list.add(companyProfile);
                }
                try {
                    arrayList = getDataList(list);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                adapter = null;
                try {
                    adapter = new CompanyProfileAdapter(arrayList, getContext(), listener);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                shimmerFrameLayout.stopShimmerAnimation();
                shimmerFrameLayout.setVisibility(View.GONE);
                recyclerView.setAdapter(adapter);
                if (list.isEmpty()) {
                    lottieAnimationView.playAnimation();
                    lottieAnimationView.setVisibility(View.VISIBLE);
                } else {
                    lottieAnimationView.pauseAnimation();
                    lottieAnimationView.setVisibility(View.GONE);
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Toast.makeText(mainActivity, "Something went wrong, Please try again later.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private ArrayList<CompanyProfile> getDataList(ArrayList<CompanyProfile> arrayList) {
        ArrayList<CompanyProfile> list = new ArrayList<>();
        for (int i = 0; i < arrayList.size(); i++) {
            String id = arrayList.get(i).getId();
            Legal legal = getLegalList(arrayList.get(i).getLegal(), id);
            CurrentAccounts currentAccounts = arrayList.get(i).getCurrentAccounts();
            Gst gst = arrayList.get(i).getGst();
            EmailAccount emailAccount = arrayList.get(i).getEmailAccount();
            DomainAccount domainAccount = arrayList.get(i).getDomainAccount();
            RailwayAccount railwayAccount = arrayList.get(i).getRailwayAccount();
            WorkplaceByFacebook workplace = arrayList.get(i).getWorkplaceByFacebook();
            AmazonBusinessAccount amazonBusinessAccount = arrayList.get(i).getAmazonBusinessAccount();
            Aws aws = arrayList.get(i).getAws();
            CompanyProfile companyProfile = new CompanyProfile(id, legal, getCurrentAccountList(currentAccounts, id), getGstList(gst, id), getEmailAccountList(emailAccount, id),
                    getDomainAccountList(domainAccount, id), getRailAccountList(railwayAccount, id), getWorkplaceList(workplace, id),
                    getAmazonBusinessAccountList(amazonBusinessAccount, id),
                    getAwsList(aws, id));
            list.add(companyProfile);
        }
        return list;
    }

    private Legal getLegalList(Legal legal, String id) {
        String venture = null, numDirectors = null, cin = null, incorporationDate = null, officeAddress = null, pan = null, tan = null;
        try {
            venture = legal.getVentureName();
            numDirectors = legal.getNumberOfDirectors();
            cin = legal.getCIN();
            incorporationDate = legal.getIncorporationDate();
            officeAddress = legal.getHeadOfficeAddress();
            pan = legal.getPAN();
            tan = legal.getTAN();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new Legal(venture, numDirectors, cin, incorporationDate, officeAddress, pan, tan);
    }

    private CurrentAccounts getCurrentAccountList(CurrentAccounts currentAccounts, String id) {
        String bankName = null, accountNumber = null, ifsc = null, companyId = null, estatementpass1 = null, estatementpass2 = null, netBankingId = null, netBankingPass = null, mpin = null, vpa = null;
        try {
            bankName = mainActivity.decryptData(currentAccounts.getBankName(), id);
            accountNumber = mainActivity.decryptData(currentAccounts.getAccountNumber(), id);
            ifsc = mainActivity.decryptData(currentAccounts.getIfscCode(), id);
            companyId = mainActivity.decryptData(currentAccounts.getCompanyID(), id);
            estatementpass1 = mainActivity.decryptData(currentAccounts.geteStatementPassword1(), id);
            estatementpass2 = mainActivity.decryptData(currentAccounts.geteStatementPassword2(), id);
            netBankingId = mainActivity.decryptData(currentAccounts.getNetBankingID(), id);
            netBankingPass = mainActivity.decryptData(currentAccounts.getNetBankingPassword(), id);
            mpin = mainActivity.decryptData(currentAccounts.getmPin(), id);
            vpa = mainActivity.decryptData(currentAccounts.getVpa(), id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new CurrentAccounts(bankName, accountNumber, ifsc, companyId, estatementpass1, estatementpass2, netBankingId, netBankingPass, mpin, vpa);
    }

    private Gst getGstList(Gst gst, String id) {
        String gstNumber = null, gstPortalId = null, gstPortalPass = null;
        try {
            gstNumber = mainActivity.decryptData(gst.getGstinNumber(), id);
            gstPortalId = mainActivity.decryptData(gst.getGstPortalID(), id);
            gstPortalPass = mainActivity.decryptData(gst.getGSTPortalPassword(), id);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new Gst(gstNumber, gstPortalId, gstPortalPass);
    }

    private EmailAccount getEmailAccountList(EmailAccount emailAccount, String id) {
        String email = null, password = null;
        try {
            email = mainActivity.decryptData(emailAccount.getEmail(), id);
            password = mainActivity.decryptData(emailAccount.getPassword(), id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new EmailAccount(email, password);
    }

    private DomainAccount getDomainAccountList(DomainAccount domainAccount, String id) {
        String domainId = null, password = null;
        try {
            domainId = mainActivity.decryptData(domainAccount.getId(), id);
            password = mainActivity.decryptData(domainAccount.getPassword(), id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new DomainAccount(domainId, password);
    }

    private RailwayAccount getRailAccountList(RailwayAccount railwayAccount, String id) {
        String railId = null, password = null, name = null, dob = null, phone = null, email = null;
        try {
            railId = mainActivity.decryptData(railwayAccount.getId(), id);
            password = mainActivity.decryptData(railwayAccount.getPassword(), id);
            name = mainActivity.decryptData(railwayAccount.getName(), id);
            dob = mainActivity.decryptData(railwayAccount.getDateOfBirth(), id);
            phone = mainActivity.decryptData(railwayAccount.getPhone(), id);
            email = mainActivity.decryptData(railwayAccount.getEmail(), id);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new RailwayAccount(railId, password, name, dob, phone, email);
    }

    private WorkplaceByFacebook getWorkplaceList(WorkplaceByFacebook workplace, String id) {
        String workplaceId = null, password = null;
        try {
            workplaceId = mainActivity.decryptData(workplace.getId(), id);
            password = mainActivity.decryptData(workplace.getPassword(), id);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new WorkplaceByFacebook(workplaceId, password);
    }

    private AmazonBusinessAccount getAmazonBusinessAccountList(AmazonBusinessAccount amazonBusinessAccount, String id) {
        String accountId = null, password = null;
        try {
            accountId = mainActivity.decryptData(amazonBusinessAccount.getId(), id);
            password = mainActivity.decryptData(amazonBusinessAccount.getPassword(), id);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new AmazonBusinessAccount(accountId, password);
    }

    private Aws getAwsList(Aws aws, String id) {
        String awsId = null, password = null, serverId = null, serverPassword = null;
        try {
            awsId = mainActivity.decryptData(aws.getId(), id);
            password = mainActivity.decryptData(aws.getPassword(), id);
            serverId = mainActivity.decryptData(aws.getId(), id);
            serverPassword = mainActivity.decryptData(aws.getPassword(), id);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return new Aws(awsId, password, serverId, serverPassword);
    }
}
