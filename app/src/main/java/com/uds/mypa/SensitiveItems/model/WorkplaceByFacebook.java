package com.uds.mypa.SensitiveItems.model;

public class WorkplaceByFacebook {
    private String id,password;

    public WorkplaceByFacebook() {
    }

    public WorkplaceByFacebook(String id, String password) {
        this.id = id;
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public String getPassword() {
        return password;
    }
}
