package com.uds.mypa.SensitiveItems.fragment;


import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.uds.mypa.Activitys.MainActivity;
import com.uds.mypa.R;
import com.uds.mypa.SensitiveItems.model.Aadhar;
import com.uds.mypa.utils.Utils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Objects;


public class NewAadharFragment extends Fragment implements DatePickerDialog.OnDateSetListener {
    private TextInputEditText mPhone, mName, mNumber, mAddress, mDateOfBirth;
    private TextInputLayout mNumberLayout, mNameLayout, mPhoneLayout;
    private TextView mTitle;
    private MainActivity mainActivity;
    private FirebaseAnalytics mFirebaseAnalytics;
    private ImageView mDateOfBirthPicker;
    private String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
    private DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(uid);
    private Boolean save = true;

    public NewAadharFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_new_aadhar, container, false);
        myRef.keepSynced(true);
        initialize(view);
        mTitle.setText("Add Aadhar Details");
        mDateOfBirthPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(mainActivity,
                        NewAadharFragment.this, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });
        final FloatingActionButton saveFab, editFab;
        saveFab = view.findViewById(R.id.save_fab);
        editFab = view.findViewById(R.id.edit_fab);
        editFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleViews(true);
                editFab.setVisibility(View.GONE);
            }
        });
        Bundle bundle = getArguments();
        if (bundle != null) {
            Aadhar passport = bundle.getParcelable("aadhar");
            mName.setText(passport.getName());
            mAddress.setText(passport.getAddress());
            mNumber.setText(passport.getAadharNumber());
            mDateOfBirth.setText(passport.getDob());
            mPhone.setText(passport.getPhoneNumber());
            uid = passport.getId();
            save = false;
            if (bundle.getInt("choose") == 0) {
                toggleViews(false);
                editFab.setVisibility(View.VISIBLE);
            } else {
                toggleViews(true);
                editFab.setVisibility(View.GONE);
            }
        }
        saveFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (save)
                    saveData();
                else
                    updateData();
            }
        });
        return view;
    }

    private boolean checkValidation() {
        boolean check = true;
        if (TextUtils.isEmpty(mName.getText().toString())) {
            mNameLayout.setError("Please Enter Name");
            check = false;
        }
        if (TextUtils.isEmpty(mNumber.getText().toString())) {
            mNumberLayout.setError("Please Enter Aadhar Number");
            check = false;
        }
        if (!TextUtils.isEmpty(mPhone.getText().toString()))
            if (!mainActivity.isValidPhone(mPhone.getText().toString())) {
                mPhoneLayout.setError("Invalid Phone Number");
                check = false;
            }

        return check;
    }

    private void initialize(@NonNull View view) {
        mainActivity = (MainActivity) getActivity();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mainActivity);
        mDateOfBirthPicker = view.findViewById(R.id.dob_picker_actions);
        mTitle = view.findViewById(R.id.title_TextView);
        mName = view.findViewById(R.id.name_editText);
        mNumber = view.findViewById(R.id.aadhar_number_editText);
        mAddress = view.findViewById(R.id.address_editText);
        mDateOfBirth = view.findViewById(R.id.date_of_birth_editText);
        mPhone = view.findViewById(R.id.phone_editText);
        mNameLayout = view.findViewById(R.id.name_layout);
        mNumberLayout = view.findViewById(R.id.number_layout);
        mPhoneLayout = view.findViewById(R.id.phone_layout);

    }

    private void saveData() {
        if (checkValidation()) {
            String id = myRef.push().getKey();
            ArrayList<String> list = new ArrayList<>();
            String name = mName.getText().toString().trim();
            String aadharNumber = mNumber.getText().toString().trim();
            String address = mAddress.getText().toString().trim();
            String dob = mDateOfBirth.getText().toString().trim();
            String phone = mPhone.getText().toString().trim();
            list.add(name);
            list.add(aadharNumber);
            list.add(dob);
            list.add(address);
            list.add(phone);
            ArrayList<String> encryptData = null;
            try {
                encryptData = mainActivity.encryptData(list, id);
            } catch (Exception e) {
                e.printStackTrace();
            }
            encryptData.add(0, id);
            Aadhar aadhar = new Aadhar(id, encryptData.get(1), encryptData.get(2), encryptData.get(3), encryptData.get(4), encryptData.get(5));
            myRef.child("Aadhar").child(id).setValue(aadhar);
            mainActivity.showAlertBox(getString(R.string.txt_Success), getString(R.string.txt_data_saved_success), 2);
            mainActivity.goBack();
        }
    }

    private void updateData() {
        if (checkValidation()) {
            ArrayList<String> list = new ArrayList<>();
            String name = mName.getText().toString().trim();
            String aadharNumber = mNumber.getText().toString().trim();
            String address = mAddress.getText().toString().trim();
            String dob = mDateOfBirth.getText().toString().trim();
            String phone = mPhone.getText().toString().trim();
            list.add(name);
            list.add(aadharNumber);
            list.add(dob);
            list.add(address);
            list.add(phone);
            ArrayList<String> encryptData = null;
            try {
                encryptData = mainActivity.encryptData(list, uid);
            } catch (Exception e) {
                e.printStackTrace();
            }
            Objects.requireNonNull(encryptData).add(0, uid);
            Aadhar aadhar = new Aadhar(uid, encryptData.get(1), encryptData.get(2), encryptData.get(3), encryptData.get(4), encryptData.get(5));
            myRef.child("Aadhar").child(uid).setValue(aadhar);
            mainActivity.showAlertBox(getString(R.string.txt_Success), getString(R.string.txt_data_updated_success), 2);
            mainActivity.goBack();
        }
    }

    private void toggleViews(boolean value) {
        mNumber.setEnabled(value);
        mName.setEnabled(value);
        mPhone.setEnabled(value);
        mDateOfBirth.setEnabled(value);
        mAddress.setEnabled(value);
        mDateOfBirthPicker.setEnabled(value);
        mNumber.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        mName.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        mPhone.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        mDateOfBirth.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        mAddress.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        if (value)
            mName.requestFocus();
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String date = Utils.formatDate(dayOfMonth, month, year);
        try {
            if (mainActivity.compareDate(date, null, true)) {
                mDateOfBirth.setText(date);
            } else {
                mDateOfBirth.setText("");
                mainActivity.showAlertBox("Warning!", "Date of Birth Cannot be after today's date", 0);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

}
