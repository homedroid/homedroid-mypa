package com.uds.mypa.SensitiveItems.fragment;


import android.app.DatePickerDialog;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.uds.mypa.Activitys.MainActivity;
import com.uds.mypa.R;
import com.uds.mypa.SensitiveItems.model.PanCard;
import com.uds.mypa.utils.Utils;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * A simple {@link Fragment} subclass.
 */
public class NewPanCardFragment extends Fragment implements DatePickerDialog.OnDateSetListener {
    private TextInputLayout mNameLayout, mNumberLayout;
    private TextInputEditText mDateOfBirth, mName, mNumber, mFathersName;
    private TextView mTitle;
    private MainActivity mainActivity;
    private FirebaseAnalytics mFirebaseAnalytics;
    private ImageView mDateOfBirthPicker;
    private String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
    private DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(uid);
    private Boolean save = true;

    public NewPanCardFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
        mainActivity.collapseBottomSheet();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_pan_card, container, false);
        myRef.keepSynced(true);
        initialize(view);
        mTitle.setText("Add Pan Card");
        mDateOfBirthPicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(mainActivity,
                        NewPanCardFragment.this, mYear, mMonth, mDay);
                datePickerDialog.show();
            }
        });
        final FloatingActionButton saveFab, editFab;
        saveFab = view.findViewById(R.id.save_fab);
        editFab = view.findViewById(R.id.edit_fab);
        editFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toggleViews(true);
                editFab.setVisibility(View.GONE);
            }
        });
        Bundle bundle = getArguments();
        if (bundle != null) {
            PanCard panCard = bundle.getParcelable("pan");
            mName.setText(panCard.getName());
            mFathersName.setText(panCard.getFathersName());
            mNumber.setText(panCard.getPanNumber());
            mDateOfBirth.setText(panCard.getDob());
            uid = panCard.getId();
            save = false;
            if (bundle.getInt("choose") == 0) {
                toggleViews(false);
                editFab.setVisibility(View.VISIBLE);
            } else {
                toggleViews(true);
                editFab.setVisibility(View.GONE);
            }
        }
        saveFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (save)
                    saveData();
                else
                    updateData();
            }
        });
        return view;
    }

    private void toggleViews(boolean b) {
        mDateOfBirth.setEnabled(b);
        mDateOfBirthPicker.setEnabled(b);
        mFathersName.setEnabled(b);
        mName.setEnabled(b);
        mNumber.setEnabled(b);
        mDateOfBirth.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        mFathersName.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        mName.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        mNumber.setTextColor(ContextCompat.getColor(mainActivity, android.R.color.background_dark));
        if (b)
            mName.requestFocus();
    }

    private boolean checkValidation() {
        boolean check = true;
        if (TextUtils.isEmpty(mName.getText().toString())) {
            mNameLayout.setError("Please Enter Name");
            check = false;
        }
        if (TextUtils.isEmpty(mNumber.getText().toString())) {
            mNumberLayout.setError("Please Enter PAN Number");
            check = false;
        }
        return check;
    }


    private void initialize(@NonNull View view) {
        mainActivity = (MainActivity) getActivity();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mainActivity);
        mFathersName = view.findViewById(R.id.fathersName_editText);
        mDateOfBirthPicker = view.findViewById(R.id.dob_picker_actions);
        mDateOfBirth = view.findViewById(R.id.date_of_birth_editText);
        mTitle = view.findViewById(R.id.title_TextView);
        mName = view.findViewById(R.id.name_editText);
        mNumber = view.findViewById(R.id.pan_number_editText);
        mNameLayout=view.findViewById(R.id.name_layout);
        mNumberLayout=view.findViewById(R.id.number_layout);

    }

    private void saveData() {
        if (checkValidation()) {
            String id = myRef.push().getKey();
            ArrayList<String> list = new ArrayList<>();
            String name = mName.getText().toString().trim();
            String panNumber = mNumber.getText().toString().trim();
            String fathersName = mFathersName.getText().toString().trim();
            String dob = mDateOfBirth.getText().toString().trim();
            list.add(name);
            list.add(panNumber);
            list.add(fathersName);
            list.add(dob);
            ArrayList<String> encryptData = null;
            try {
                encryptData = mainActivity.encryptData(list, id);
            } catch (Exception e) {
                e.printStackTrace();
            }
            encryptData.add(0, id);
            PanCard panModel = new PanCard(id, encryptData.get(1), encryptData.get(2), encryptData.get(3), encryptData.get(4));
            myRef.child("Pan Card").child(id).setValue(panModel);
            mainActivity.showAlertBox(getString(R.string.txt_Success), getString(R.string.txt_data_saved_success), 2);
            mainActivity.goBack();
        }
    }

    private void updateData() {
        if (checkValidation()) {
            ArrayList<String> list = new ArrayList<>();
            String name = mName.getText().toString().trim();
            String panNumber = mNumber.getText().toString().trim();
            String fathersName = mFathersName.getText().toString().trim();
            String dob = mDateOfBirth.getText().toString().trim();
            list.add(name);
            list.add(panNumber);
            list.add(fathersName);
            list.add(dob);
            ArrayList<String> encryptData = null;
            try {
                encryptData = mainActivity.encryptData(list, uid);
            } catch (Exception e) {
                e.printStackTrace();
            }
            encryptData.add(0, uid);
            PanCard panModel = new PanCard(uid, encryptData.get(1), encryptData.get(2), encryptData.get(3), encryptData.get(4));

            myRef.child("Pan Card").child(uid).setValue(panModel);
            mainActivity.showAlertBox(getString(R.string.txt_Success), getString(R.string.txt_data_updated_success), 2);
            mainActivity.goBack();
        }
    }


    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String date = Utils.formatDate(dayOfMonth, month, year);
        try {
            if (mainActivity.compareDate(date, null, true)) {
                mDateOfBirth.setText(date);
            } else {
                mDateOfBirth.setText("");
                mainActivity.showAlertBox("Warning!", "Date of Birth Cannot be after today's date", 0);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
