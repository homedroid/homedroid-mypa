package com.uds.mypa.Home.fragment;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.uds.mypa.Home.adapters.SensitiveAdapter;
import com.uds.mypa.Home.models.Items;
import com.uds.mypa.Activitys.MainActivity;
import com.uds.mypa.Interfaces.OnItemClickListener;
import com.uds.mypa.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class SensitiveItemsFragment extends Fragment implements OnItemClickListener {
    private ArrayList<Items> arrayList;
    private MainActivity mainActivity;
    private FirebaseAnalytics mFirebaseAnalytics;


    public SensitiveItemsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onStart() {
        super.onStart();
        mainActivity.collapseBottomSheet();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_sensitive_items, container, false);
        // Inflate the layout for this fragment
        mainActivity = (MainActivity) getActivity();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mainActivity);
        TextView mTitle = view.findViewById(R.id.title_TextView);
        mTitle.setText(getString(R.string.txt_sensitive_items));
        arrayList = new ArrayList<>();
        setUpSensitiveListData();
        RecyclerView recyclerView = view.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(new SensitiveAdapter(arrayList, getContext(), this));
        return view;
    }

    private void setUpSensitiveListData() {
        arrayList.add(new Items("Passport", R.drawable.passport));
        arrayList.add(new Items("Voter ID", R.drawable.voter_id));
        arrayList.add(new Items("PAN Card", R.drawable.pan_card));
        arrayList.add(new Items("Aadhaar Card", R.drawable.aadhar_cards));
        arrayList.add(new Items("Driving License", R.drawable.driving_license));
        arrayList.add(new Items("Banking Card", R.drawable.banking_cards));
        arrayList.add(new Items("Bank Accounts", R.drawable.banking_accounts));
        arrayList.add(new Items("Membership", R.drawable.membership_cards));
        arrayList.add(new Items("Email Accounts", R.drawable.email_accounts));
        arrayList.add(new Items("Social Networking", R.drawable.social_networking));
        arrayList.add(new Items("Online Accounts", R.drawable.online_accounts));
        arrayList.add(new Items("Insurance", R.drawable.insurance));
        arrayList.add(new Items("Electricity", R.drawable.electricity));
        arrayList.add(new Items("Gas Connection", R.drawable.gas_connection));
        arrayList.add(new Items("System Login", R.drawable.system_login));
        arrayList.add(new Items("Remote Login", R.drawable.remote_login));
        arrayList.add(new Items("Wi-Fi Password", R.drawable.wi_fi_password));
        arrayList.add(new Items("Company Profile", R.drawable.company_profile));
        arrayList.add(new Items("Custom", R.drawable.custom));
    }

    @Override
    public void onItemClick(int position) {
        switch (position) {
            //passport
            case 0:
                mainActivity.setUpFragment(2, null);
                break;
            //voter id
            case 1:
                mainActivity.setUpFragment(4, null);
                break;
            //pan card
            case 2:
                mainActivity.setUpFragment(6, null);
                break;
            //aadhar
            case 3:
                mainActivity.setUpFragment(8, null);
                break;
            //driving license
            case 4:
                mainActivity.setUpFragment(10, null);
                break;
                //bank card
            case 5:
                mainActivity.setUpFragment(12, null);
                break;
                //Bank Account
            case 6:
                mainActivity.setUpFragment(14, null);
                break;
            //membership
            case 7:
                mainActivity.setUpFragment(16, null);
                break;
            //email
            case 8:
                mainActivity.setUpFragment(18, null);
                break;
                //social networking
            case 9:
                mainActivity.setUpFragment(20, null);
                break;
            //online accounts
            case 10:
                mainActivity.setUpFragment(22, null);
                break;
            //insurance
            case 11:
                mainActivity.setUpFragment(24, null);
                break;
            //Electricity
            case 12:
                mainActivity.setUpFragment(26, null);
                break;
            //gas connection
            case 13:
                mainActivity.setUpFragment(28, null);
                break;
            case 14:
                mainActivity.setUpFragment(30, null);
                break;
            // remote login
            case 15:
                mainActivity.setUpFragment(32, null);
                break;
            //wifi
            case 16:
                mainActivity.setUpFragment(34, null);
                break;

            case 17:
                mainActivity.setUpFragment(36, null);
                break;
        }
    }
}
