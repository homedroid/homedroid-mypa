package com.uds.mypa.Home.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.uds.mypa.Home.models.Birthday;
import com.uds.mypa.Home.models.Reminders;
import com.uds.mypa.Interfaces.OnItemClickListener;
import com.uds.mypa.databinding.BirthdayListItemBinding;
import com.uds.mypa.databinding.RemindersItemBinding;

import java.util.List;

public class BirthdayAdapter extends RecyclerView.Adapter<BirthdayAdapter.ViewHolder> {

    List<Birthday> list;
    Context context;
    private OnItemClickListener onItemClickListener;
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        BirthdayListItemBinding binding = BirthdayListItemBinding.inflate(inflater, parent, false);
        return new ViewHolder(binding,onItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements  View.OnClickListener {
        private final BirthdayListItemBinding binding;
        private OnItemClickListener onItemClickListener;
        public ViewHolder(@NonNull BirthdayListItemBinding binding, OnItemClickListener onItemClickListener) {
            super(binding.getRoot());
            this.binding=binding;
            this.onItemClickListener=onItemClickListener;
            binding.getRoot().setOnClickListener(this);

        }
        public void bindView(Birthday birthday) {
            binding.title.setText(birthday.getName());
        }



        @Override
        public void onClick(View v) {
            onItemClickListener.onItemClick(getAdapterPosition());
        }
    }
}
