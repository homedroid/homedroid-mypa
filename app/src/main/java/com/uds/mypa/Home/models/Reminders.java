package com.uds.mypa.Home.models;

public class Reminders {
    private String text;
    private int image;

    public Reminders(String text, int image) {
        this.text = text;
        this.image = image;
    }

    public String getText() {
        return text;
    }

    public int getImage() {
        return image;
    }
}
