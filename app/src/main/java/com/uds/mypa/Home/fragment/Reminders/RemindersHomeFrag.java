package com.uds.mypa.Home.fragment.Reminders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.uds.mypa.Home.adapters.RemindersAdapter;
import com.uds.mypa.Home.models.Reminders;
import com.uds.mypa.Interfaces.NavigationHost;
import com.uds.mypa.Interfaces.OnItemClickListener;
import com.uds.mypa.R;
import com.uds.mypa.databinding.FragReminderHomeBinding;
import com.uds.mypa.utils.Images;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class RemindersHomeFrag extends Fragment implements OnItemClickListener {
    private RemindersAdapter adapter;
    private FragReminderHomeBinding binding;

    public RemindersHomeFrag() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragReminderHomeBinding.inflate(inflater, container, false);
        View view=binding.getRoot();
        List<Reminders> list = new ArrayList<>();
        for (int i = 0; i < Images.reminderIcons.length; i++) {
            list.add(new Reminders(Images.remindersTypes[i], Images.reminderIcons[i]));
        }
        adapter = new RemindersAdapter(list, getContext(),this);
        binding.recyclerView.setLayoutManager(new GridLayoutManager(getContext(), 2));
        binding.recyclerView.setAdapter(adapter);
        return view;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(int position) {
        switch (position)
        {
            case 0:
                ((NavigationHost)getActivity()).navigateTo(new BirthdayFrag(),true);
                break;
        }
    }
}
