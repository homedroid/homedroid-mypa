package com.uds.mypa.Home.models;

public class Items {
    private String text;
    private int image;

    public String getText() {
        return text;
    }

    public int getImage() {
        return image;
    }

    public Items(String text, int image) {
        this.text = text;
        this.image = image;
    }
}
