package com.uds.mypa.Home.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.os.Bundle;
import android.view.View;

import com.uds.mypa.Home.adapters.RemindersAdapter;
import com.uds.mypa.Home.fragment.Reminders.BirthdayFrag;
import com.uds.mypa.Home.fragment.Reminders.RemindersHomeFrag;
import com.uds.mypa.Home.models.Reminders;
import com.uds.mypa.Interfaces.NavigationHost;
import com.uds.mypa.Interfaces.OnItemClickListener;
import com.uds.mypa.R;
import com.uds.mypa.databinding.ActivityRemindersBinding;
import com.uds.mypa.utils.Images;

import java.util.ArrayList;
import java.util.List;

public class RemindersActivity extends AppCompatActivity implements NavigationHost {
    private ActivityRemindersBinding binding;
    private RemindersAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityRemindersBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
        if (savedInstanceState == null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .add(R.id.container, new RemindersHomeFrag())
                    .commit();
        }
    }

    public void goBackReminder(View view) {
        finish();
    }

    @Override
    public void navigateTo(Fragment fragment, boolean addToBackstack) {
        FragmentTransaction transaction =
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, fragment);

        if (addToBackstack) {
            transaction.addToBackStack(null);
        }

        transaction.commit();
    }


}
