package com.uds.mypa.Home.bottomSheets;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.uds.mypa.Home.models.Birthday;
import com.uds.mypa.R;
import com.uds.mypa.databinding.BottomsheetBirthdayBinding;


public class AddBirthdayBottomSheet extends BottomSheetDialogFragment implements RadioGroup.OnCheckedChangeListener {
    private BottomsheetBirthdayBinding binding;
    private String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
    private DatabaseReference myRef = FirebaseDatabase.getInstance().getReference(uid);
    private Birthday birthday;
    private String reminderType;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = BottomsheetBirthdayBinding.inflate(inflater, container, false);
        myRef.keepSynced(true);
        binding.btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveReminderDate();
            }
        });
        return binding.getRoot();
    }

    private void saveReminderDate() {
        String id = myRef.push().getKey();
        String name, relation, date;
        name = binding.name.getText().toString();
        relation = binding.relation.getText().toString();
        date = binding.editTextDate.getText().toString();
        birthday = new Birthday(id, name, relation, null, date, null);
        myRef.child("Reminder").child("Birthday").child(id).setValue(birthday);

    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        int checkedRadioButtonId = group.getCheckedRadioButtonId();
        if (checkedRadioButtonId == -1) {
            // No item selected
        } else {
            if (checkedRadioButtonId == R.id.alarm) {
                reminderType = "alarm";
            } else if (checkedRadioButtonId == R.id.notification) {
                reminderType = "notification";
            }
        }
    }
}
