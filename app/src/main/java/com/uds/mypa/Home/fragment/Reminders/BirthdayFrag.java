package com.uds.mypa.Home.fragment.Reminders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.uds.mypa.Home.bottomSheets.AddBirthdayBottomSheet;
import com.uds.mypa.R;
import com.uds.mypa.databinding.FragmentBrithdayBinding;


public class BirthdayFrag extends Fragment {
    private FragmentBrithdayBinding binding;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentBrithdayBinding.inflate(inflater, container, false);
        setHasOptionsMenu(true);
        binding.floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddBirthdayBottomSheet bottomSheet = new AddBirthdayBottomSheet();
                bottomSheet.show(getChildFragmentManager(), "Add Birthday");
            }
        });
        return binding.getRoot();
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        inflater.inflate(R.menu.reminders_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        return true;
    }
}