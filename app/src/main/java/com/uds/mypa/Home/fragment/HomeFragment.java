package com.uds.mypa.Home.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.uds.mypa.Home.activity.RemindersActivity;
import com.uds.mypa.Home.adapters.SensitiveAdapter;
import com.uds.mypa.Home.models.Items;
import com.uds.mypa.Activitys.MainActivity;
import com.uds.mypa.Interfaces.OnItemClickListener;
import com.uds.mypa.R;

import java.util.ArrayList;

public class HomeFragment extends Fragment implements OnItemClickListener {
    private ArrayList<Items> arrayList;
    private MainActivity mainActivity;
    private FirebaseAnalytics mFirebaseAnalytics;
    private TextView mTitle;


    public HomeFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public void onStart() {
        super.onStart();
        mainActivity.collapseBottomSheet();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home_main, container, false);
        mainActivity = (MainActivity) getActivity();
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(mainActivity);
        mTitle = view.findViewById(R.id.title_TextView);
        mTitle.setText(getString(R.string.txt_home));

        arrayList = new ArrayList<>();
        setUpHomeListData();
        RecyclerView recyclerView = view.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(new SensitiveAdapter(arrayList, mainActivity, this));

        return view;
    }


    private void setUpHomeListData() {
        arrayList.add(new Items("Sensitive Items", R.mipmap.sensitive_items));
        arrayList.add(new Items("Reminders", R.mipmap.reminders));
        arrayList.add(new Items("Recharge Dues", R.mipmap.recharge_dues));
        arrayList.add(new Items("Debit-Credit Section", R.mipmap.debit_credit_section));
        arrayList.add(new Items("To Do Lists", R.mipmap.to_do_list));
        arrayList.add(new Items("Business Contacts", R.mipmap.bussiness_contacts));
        arrayList.add(new Items("Sticky Notes", R.mipmap.sticky_notes));
        arrayList.add(new Items("E-Visiting Cards", R.mipmap.evisiting_cards));
        arrayList.add(new Items("Std Codes", R.mipmap.std_codes));
        arrayList.add(new Items("Purchase Lists", R.mipmap.purchase_lists));
        arrayList.add(new Items("Bucket Lists", R.mipmap.bucket_lists));
        arrayList.add(new Items("Custom Notification", R.mipmap.custom_notification));

    }


    @Override
    public void onItemClick(int position) {
        switch (position) {
            case 0:
                mainActivity.setUpFragment(1, null);
                break;
            case 1:
                startActivity(new Intent(getContext(), RemindersActivity.class));
                break;


        }

    }
}
