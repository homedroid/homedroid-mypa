package com.uds.mypa.Home.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.uds.mypa.Home.models.Items;
import com.uds.mypa.Interfaces.OnItemClickListener;
import com.uds.mypa.R;

import java.util.ArrayList;

public class SensitiveAdapter extends RecyclerView.Adapter<SensitiveAdapter.ViewHolder> {
    private ArrayList<Items> arrayList;
    private Context context;
    private  OnItemClickListener onItemClickListner;

    public SensitiveAdapter(ArrayList<Items> arrayList, Context context,OnItemClickListener onItemClickListner) {
        this.arrayList = arrayList;
        this.context = context;
        this.onItemClickListner=onItemClickListner;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.main_list, parent, false);
        return new ViewHolder(view,onItemClickListner);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Items items = arrayList.get(position);
        holder.imageView.setImageResource(items.getImage());
        holder.textView.setText(items.getText());

    }


    @Override
    public int getItemCount() {
        return arrayList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView textView;
        ImageView imageView;
        OnItemClickListener onItemClickListner;

        public ViewHolder(@NonNull View itemView, OnItemClickListener onItemClickListner) {
            super(itemView);
            textView = itemView.findViewById(R.id.title);
            imageView = itemView.findViewById(R.id.image);
            this.onItemClickListner = onItemClickListner;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onItemClickListner.onItemClick(getAdapterPosition());
        }
    }


}
