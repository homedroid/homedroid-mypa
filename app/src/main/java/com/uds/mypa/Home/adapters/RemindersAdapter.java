package com.uds.mypa.Home.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.uds.mypa.Home.activity.RemindersActivity;
import com.uds.mypa.Home.models.Reminders;
import com.uds.mypa.Interfaces.OnItemClickListener;
import com.uds.mypa.R;
import com.uds.mypa.databinding.RemindersItemBinding;

import java.util.List;

public class RemindersAdapter extends RecyclerView.Adapter<RemindersAdapter.ViewHolder> {
    List<Reminders> list;
    Context context;
    private OnItemClickListener onItemClickListener;

    public RemindersAdapter(List<Reminders> list, Context context,OnItemClickListener onItemClickListener) {
        this.list = list;
        this.context = context;
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        RemindersItemBinding binding = RemindersItemBinding.inflate(inflater, parent, false);
        return new ViewHolder(binding,onItemClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindView(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private final RemindersItemBinding binding;
        private OnItemClickListener onItemClickListener;
        public ViewHolder(@NonNull RemindersItemBinding binding, OnItemClickListener onItemClickListener) {
            super(binding.getRoot());
            this.binding = binding;
            this.onItemClickListener=onItemClickListener;
            binding.getRoot().setOnClickListener(this);

        }

        public void bindView(Reminders reminders) {
            binding.text.setText(reminders.getText());
            binding.logo.setImageResource(reminders.getImage());
        }

        @Override
        public void onClick(View v) {
            onItemClickListener.onItemClick(getAdapterPosition());
        }
    }
}
