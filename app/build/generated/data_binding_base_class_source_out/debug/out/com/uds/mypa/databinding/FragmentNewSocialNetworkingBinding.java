// Generated by view binder compiler. Do not edit!
package com.uds.mypa.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.uds.mypa.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class FragmentNewSocialNetworkingBinding implements ViewBinding {
  @NonNull
  private final ConstraintLayout rootView;

  @NonNull
  public final TextInputEditText answerEditText;

  @NonNull
  public final AppBarLayout appBarLayout;

  @NonNull
  public final LinearLayout chooseCompany;

  @NonNull
  public final ImageView companyLogo;

  @NonNull
  public final TextView companyName;

  @NonNull
  public final FloatingActionButton editFab;

  @NonNull
  public final TextInputLayout emailLayout;

  @NonNull
  public final TextInputEditText nameEditText;

  @NonNull
  public final TextInputLayout nameLayout;

  @NonNull
  public final TextInputEditText passwordEditText;

  @NonNull
  public final TextInputLayout passwordLayout;

  @NonNull
  public final TextInputEditText questionEditText;

  @NonNull
  public final TextInputEditText recoveryEmailEditText;

  @NonNull
  public final FloatingActionButton saveFab;

  @NonNull
  public final TextInputEditText userNameEditText;

  @NonNull
  public final TextInputLayout userNameLayout;

  private FragmentNewSocialNetworkingBinding(@NonNull ConstraintLayout rootView,
      @NonNull TextInputEditText answerEditText, @NonNull AppBarLayout appBarLayout,
      @NonNull LinearLayout chooseCompany, @NonNull ImageView companyLogo,
      @NonNull TextView companyName, @NonNull FloatingActionButton editFab,
      @NonNull TextInputLayout emailLayout, @NonNull TextInputEditText nameEditText,
      @NonNull TextInputLayout nameLayout, @NonNull TextInputEditText passwordEditText,
      @NonNull TextInputLayout passwordLayout, @NonNull TextInputEditText questionEditText,
      @NonNull TextInputEditText recoveryEmailEditText, @NonNull FloatingActionButton saveFab,
      @NonNull TextInputEditText userNameEditText, @NonNull TextInputLayout userNameLayout) {
    this.rootView = rootView;
    this.answerEditText = answerEditText;
    this.appBarLayout = appBarLayout;
    this.chooseCompany = chooseCompany;
    this.companyLogo = companyLogo;
    this.companyName = companyName;
    this.editFab = editFab;
    this.emailLayout = emailLayout;
    this.nameEditText = nameEditText;
    this.nameLayout = nameLayout;
    this.passwordEditText = passwordEditText;
    this.passwordLayout = passwordLayout;
    this.questionEditText = questionEditText;
    this.recoveryEmailEditText = recoveryEmailEditText;
    this.saveFab = saveFab;
    this.userNameEditText = userNameEditText;
    this.userNameLayout = userNameLayout;
  }

  @Override
  @NonNull
  public ConstraintLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static FragmentNewSocialNetworkingBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static FragmentNewSocialNetworkingBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.fragment_new_social_networking, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static FragmentNewSocialNetworkingBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    String missingId;
    missingId: {
      TextInputEditText answerEditText = rootView.findViewById(R.id.answer_editText);
      if (answerEditText == null) {
        missingId = "answerEditText";
        break missingId;
      }
      AppBarLayout appBarLayout = rootView.findViewById(R.id.app_bar_layout);
      if (appBarLayout == null) {
        missingId = "appBarLayout";
        break missingId;
      }
      LinearLayout chooseCompany = rootView.findViewById(R.id.chooseCompany);
      if (chooseCompany == null) {
        missingId = "chooseCompany";
        break missingId;
      }
      ImageView companyLogo = rootView.findViewById(R.id.companyLogo);
      if (companyLogo == null) {
        missingId = "companyLogo";
        break missingId;
      }
      TextView companyName = rootView.findViewById(R.id.company_name);
      if (companyName == null) {
        missingId = "companyName";
        break missingId;
      }
      FloatingActionButton editFab = rootView.findViewById(R.id.edit_fab);
      if (editFab == null) {
        missingId = "editFab";
        break missingId;
      }
      TextInputLayout emailLayout = rootView.findViewById(R.id.email_layout);
      if (emailLayout == null) {
        missingId = "emailLayout";
        break missingId;
      }
      TextInputEditText nameEditText = rootView.findViewById(R.id.name_editText);
      if (nameEditText == null) {
        missingId = "nameEditText";
        break missingId;
      }
      TextInputLayout nameLayout = rootView.findViewById(R.id.name_layout);
      if (nameLayout == null) {
        missingId = "nameLayout";
        break missingId;
      }
      TextInputEditText passwordEditText = rootView.findViewById(R.id.password_editText);
      if (passwordEditText == null) {
        missingId = "passwordEditText";
        break missingId;
      }
      TextInputLayout passwordLayout = rootView.findViewById(R.id.password_layout);
      if (passwordLayout == null) {
        missingId = "passwordLayout";
        break missingId;
      }
      TextInputEditText questionEditText = rootView.findViewById(R.id.question_editText);
      if (questionEditText == null) {
        missingId = "questionEditText";
        break missingId;
      }
      TextInputEditText recoveryEmailEditText = rootView.findViewById(R.id.recovery_email_editText);
      if (recoveryEmailEditText == null) {
        missingId = "recoveryEmailEditText";
        break missingId;
      }
      FloatingActionButton saveFab = rootView.findViewById(R.id.save_fab);
      if (saveFab == null) {
        missingId = "saveFab";
        break missingId;
      }
      TextInputEditText userNameEditText = rootView.findViewById(R.id.user_name_editText);
      if (userNameEditText == null) {
        missingId = "userNameEditText";
        break missingId;
      }
      TextInputLayout userNameLayout = rootView.findViewById(R.id.user_name_layout);
      if (userNameLayout == null) {
        missingId = "userNameLayout";
        break missingId;
      }
      return new FragmentNewSocialNetworkingBinding((ConstraintLayout) rootView, answerEditText,
          appBarLayout, chooseCompany, companyLogo, companyName, editFab, emailLayout, nameEditText,
          nameLayout, passwordEditText, passwordLayout, questionEditText, recoveryEmailEditText,
          saveFab, userNameEditText, userNameLayout);
    }
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
