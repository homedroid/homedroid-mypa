// Generated by view binder compiler. Do not edit!
package com.uds.mypa.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.uds.mypa.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class FragmentNewPassportBinding implements ViewBinding {
  @NonNull
  private final ConstraintLayout rootView;

  @NonNull
  public final AppBarLayout appBarLayout;

  @NonNull
  public final FloatingActionButton editFab;

  @NonNull
  public final TextInputEditText expiryDate;

  @NonNull
  public final ImageView expiryDatePickerActions;

  @NonNull
  public final TextInputEditText issueDate;

  @NonNull
  public final ImageView issueDatePickerActions;

  @NonNull
  public final TextInputEditText nameEditText;

  @NonNull
  public final TextInputLayout nameLayout;

  @NonNull
  public final TextInputLayout numberLayout;

  @NonNull
  public final TextInputEditText passportNumberEditText;

  @NonNull
  public final TextInputEditText placeEditText;

  @NonNull
  public final FloatingActionButton saveFab;

  private FragmentNewPassportBinding(@NonNull ConstraintLayout rootView,
      @NonNull AppBarLayout appBarLayout, @NonNull FloatingActionButton editFab,
      @NonNull TextInputEditText expiryDate, @NonNull ImageView expiryDatePickerActions,
      @NonNull TextInputEditText issueDate, @NonNull ImageView issueDatePickerActions,
      @NonNull TextInputEditText nameEditText, @NonNull TextInputLayout nameLayout,
      @NonNull TextInputLayout numberLayout, @NonNull TextInputEditText passportNumberEditText,
      @NonNull TextInputEditText placeEditText, @NonNull FloatingActionButton saveFab) {
    this.rootView = rootView;
    this.appBarLayout = appBarLayout;
    this.editFab = editFab;
    this.expiryDate = expiryDate;
    this.expiryDatePickerActions = expiryDatePickerActions;
    this.issueDate = issueDate;
    this.issueDatePickerActions = issueDatePickerActions;
    this.nameEditText = nameEditText;
    this.nameLayout = nameLayout;
    this.numberLayout = numberLayout;
    this.passportNumberEditText = passportNumberEditText;
    this.placeEditText = placeEditText;
    this.saveFab = saveFab;
  }

  @Override
  @NonNull
  public ConstraintLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static FragmentNewPassportBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static FragmentNewPassportBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.fragment_new_passport, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static FragmentNewPassportBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    String missingId;
    missingId: {
      AppBarLayout appBarLayout = rootView.findViewById(R.id.app_bar_layout);
      if (appBarLayout == null) {
        missingId = "appBarLayout";
        break missingId;
      }
      FloatingActionButton editFab = rootView.findViewById(R.id.edit_fab);
      if (editFab == null) {
        missingId = "editFab";
        break missingId;
      }
      TextInputEditText expiryDate = rootView.findViewById(R.id.expiryDate);
      if (expiryDate == null) {
        missingId = "expiryDate";
        break missingId;
      }
      ImageView expiryDatePickerActions = rootView.findViewById(R.id.expiry_date_picker_actions);
      if (expiryDatePickerActions == null) {
        missingId = "expiryDatePickerActions";
        break missingId;
      }
      TextInputEditText issueDate = rootView.findViewById(R.id.issueDate);
      if (issueDate == null) {
        missingId = "issueDate";
        break missingId;
      }
      ImageView issueDatePickerActions = rootView.findViewById(R.id.issue_date_picker_actions);
      if (issueDatePickerActions == null) {
        missingId = "issueDatePickerActions";
        break missingId;
      }
      TextInputEditText nameEditText = rootView.findViewById(R.id.name_editText);
      if (nameEditText == null) {
        missingId = "nameEditText";
        break missingId;
      }
      TextInputLayout nameLayout = rootView.findViewById(R.id.name_layout);
      if (nameLayout == null) {
        missingId = "nameLayout";
        break missingId;
      }
      TextInputLayout numberLayout = rootView.findViewById(R.id.number_layout);
      if (numberLayout == null) {
        missingId = "numberLayout";
        break missingId;
      }
      TextInputEditText passportNumberEditText = rootView.findViewById(R.id.passport_number_editText);
      if (passportNumberEditText == null) {
        missingId = "passportNumberEditText";
        break missingId;
      }
      TextInputEditText placeEditText = rootView.findViewById(R.id.place_editText);
      if (placeEditText == null) {
        missingId = "placeEditText";
        break missingId;
      }
      FloatingActionButton saveFab = rootView.findViewById(R.id.save_fab);
      if (saveFab == null) {
        missingId = "saveFab";
        break missingId;
      }
      return new FragmentNewPassportBinding((ConstraintLayout) rootView, appBarLayout, editFab,
          expiryDate, expiryDatePickerActions, issueDate, issueDatePickerActions, nameEditText,
          nameLayout, numberLayout, passportNumberEditText, placeEditText, saveFab);
    }
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
