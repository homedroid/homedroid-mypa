// Generated by view binder compiler. Do not edit!
package com.uds.mypa.databinding;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.viewbinding.ViewBinding;
import com.google.android.material.textfield.TextInputLayout;
import com.uds.mypa.R;
import java.lang.NullPointerException;
import java.lang.Override;
import java.lang.String;

public final class ReminderBottomsheetLayoutBinding implements ViewBinding {
  @NonNull
  private final ConstraintLayout rootView;

  @NonNull
  public final Button button;

  @NonNull
  public final TextInputLayout reminderTextLayout;

  @NonNull
  public final TextInputLayout titleTextLayout;

  private ReminderBottomsheetLayoutBinding(@NonNull ConstraintLayout rootView,
      @NonNull Button button, @NonNull TextInputLayout reminderTextLayout,
      @NonNull TextInputLayout titleTextLayout) {
    this.rootView = rootView;
    this.button = button;
    this.reminderTextLayout = reminderTextLayout;
    this.titleTextLayout = titleTextLayout;
  }

  @Override
  @NonNull
  public ConstraintLayout getRoot() {
    return rootView;
  }

  @NonNull
  public static ReminderBottomsheetLayoutBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, null, false);
  }

  @NonNull
  public static ReminderBottomsheetLayoutBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup parent, boolean attachToParent) {
    View root = inflater.inflate(R.layout.reminder_bottomsheet_layout, parent, false);
    if (attachToParent) {
      parent.addView(root);
    }
    return bind(root);
  }

  @NonNull
  public static ReminderBottomsheetLayoutBinding bind(@NonNull View rootView) {
    // The body of this method is generated in a way you would not otherwise write.
    // This is done to optimize the compiled bytecode for size and performance.
    String missingId;
    missingId: {
      Button button = rootView.findViewById(R.id.button);
      if (button == null) {
        missingId = "button";
        break missingId;
      }
      TextInputLayout reminderTextLayout = rootView.findViewById(R.id.reminder_text_layout);
      if (reminderTextLayout == null) {
        missingId = "reminderTextLayout";
        break missingId;
      }
      TextInputLayout titleTextLayout = rootView.findViewById(R.id.title_text_layout);
      if (titleTextLayout == null) {
        missingId = "titleTextLayout";
        break missingId;
      }
      return new ReminderBottomsheetLayoutBinding((ConstraintLayout) rootView, button,
          reminderTextLayout, titleTextLayout);
    }
    throw new NullPointerException("Missing required view with ID: ".concat(missingId));
  }
}
